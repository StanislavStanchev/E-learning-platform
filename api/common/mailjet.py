from mailjet_rest import Client

from api.config import setting_mailjet, settings_email, settings_token

from itsdangerous import URLSafeTimedSerializer

from mailjet_rest import Client

from fastapi import HTTPException

API_KEY = setting_mailjet.api_key

API_SECRET = setting_mailjet.api_secret_key

email_sender = settings_email.email_sender

mailjet = Client(auth=(API_KEY, API_SECRET), version="v3.1")


email_html_template_subscription = """

    <h1>Dear {teacher_name},</h1>

    <p>We're pleased to inform you that a new student, <b>{student_name}</b>, has subscribed to your course, <b>{course_name}</b>.</p>

    <p>Please prepare to provide the necessary guidance and support to help them through the course.</p>

    <p>Best Regards,</p>

    <p>Poodle Management Team</p>

"""


email_html_template_deactivate = """

    <h1>Dear {student_name},</h1>

    <p>We hope this email finds you well. We would like to inform you about an important update regarding the course you have been enrolled in, {course_name}.</p>

    <p>Unfortunately, we regret to inform you that {course_name} is no longer active and has been discontinued. We understand that this may come as a disappointment, and we sincerely apologize for any inconvenience caused.</p>

    <p>The decision to discontinue the course was made after careful consideration and evaluation. We constantly strive to provide the best learning experience for our students, and sometimes adjustments are necessary to ensure the quality and relevance of our course offerings.</p>

    <p>While {course_name} is no longer available, we would like to assure you that our platform continues to offer a wide range of other engaging and valuable courses. We encourage you to explore our course catalog and discover new opportunities for learning and personal growth.</p>

    <p>If you have any questions or require further assistance, please do not hesitate to reach out to our support team at poodle_6@abv.bg. Our team is always ready to assist you and provide guidance on alternative courses that may align with your interests and goals.</p>

    <p>Once again, we apologize for any inconvenience this may cause, and we appreciate your understanding. We remain committed to your learning journey and look forward to supporting you in your pursuit of knowledge.</p>

    <p>Thank you for being a part of our learning community.</p>

    <p>Poodle Management Team</p>

    """

email_html_template_verification = """

    <p>Dear {teacher_name},</p>

    <p>We have received a request to set this email address as your contact email.</p>

    <p>If you made this request, please confirm your email address by clicking the link below:</p>

    <p><a href="http://localhost:8000/api/users/confirm-email/{token}">Confirm Email</a></p>

    <p>If you did not make this request, you can safely ignore this email and your details will remain the same.</p>

    <p>Best Regards,</p>

    <p>Poodle Management Team</p>

    """


def email_students_on_course(
    email_receiver: str,
    course_name: str,
    student_name: str,
    html: str,
    teacher_name=None,
):
    if teacher_name == None:
        html_content = html.format(student_name=student_name, course_name=course_name)

    else:
        html_content = html.format(
            student_name=student_name,
            course_name=course_name,
            teacher_name=teacher_name,
        )

    data = {
        "Messages": [
            {
                "From": {"Email": f"{email_sender}", "Name": "Poodle"},
                "To": [{"Email": f"{email_receiver}", "Name": f"{student_name}"}],
                "Subject": f"Subscription to {course_name}",
                "TextPart": "Greetings from Mailjet!",
                "HTMLPart": html_content,
            }
        ]
    }

    
    response = mailjet.send.create(data=data)


    if response.status_code != 200:
        raise HTTPException(status_code = response.status_code, detail=f"Failed to send email.")



s = URLSafeTimedSerializer(settings_token.secret_key)


def verify_email(email: str, teacher_name):
    token = s.dumps(email, salt="email-confirm")

    html_content = email_html_template_verification.format(
        teacher_name=teacher_name, token=token
    )

    data = {
        "Messages": [
            {
                "From": {"Email": f"{email_sender}", "Name": "Poodle"},
                "To": [{"Email": email, "Name": teacher_name}],
                "Subject": "Confirm Email",
                "HTMLPart": html_content,
            }
        ]
    }

    
    response = mailjet.send.create(data=data)


    if response.status_code != 200:
        raise HTTPException(status_code = response.status_code, detail=f"Failed to send email.")

    
