from fastapi import APIRouter
from api.routers.tags import tags_router
from api.routers.users import users_router
from api.routers.admins import admins_router
from api.routers.guests import guests_router
from api.routers.courses import courses_router
from api.routers.sections import section_router
from api.routers.students import students_router


api_router = APIRouter(prefix="/api")


api_router.include_router(courses_router)
api_router.include_router(section_router)
api_router.include_router(users_router)
api_router.include_router(tags_router)
api_router.include_router(students_router)
api_router.include_router(admins_router)
api_router.include_router(guests_router)
