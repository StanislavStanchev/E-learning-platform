import mariadb
from api.common import mailjet
from pydantic import conint, constr
from fastapi_pagination import Page, paginate
from api.services import courses_service, users_service
from fastapi import APIRouter, Depends, Query, Path
from api.common.responses import NotFound, Conflict, Forbidden, NoContent, Created
from api.data.models import (
    Course,
    CourseResponseModel,
    SortBySectionAttribute,
    SortType,
    CourseWithSectionsResponseModel,
    Section,
    Tag,
    HomePageInput,
    UserOut,
    CourseEditModel,
    CourseStatus,
)

courses_router = APIRouter(prefix="/courses", tags=["Courses"])


@courses_router.post("", status_code=201)
def create_course(
    course: Course, current_user: UserOut = Depends(users_service.get_current_user)
):
    """Teachers can create new courses where each course should has unique title."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can create courses.",
    )
    if is_forbidden:
        return is_forbidden

    try:
        return courses_service.create_course(course=course, current_user=current_user)

    except mariadb.IntegrityError:
        return Conflict(content="Title already in use!")


@courses_router.get("", status_code=200, response_model=Page[CourseResponseModel])
def get_all_courses(
    search: constr(strip_whitespace=True, min_length=1, max_length=30) | None = None,
    tag: constr(strip_whitespace=True, min_length=2, max_length=30) | None = None,
    sort: SortType | None = None,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Logged-in user can view a list of courses and the course(active/inactive) status."""

    return paginate(
        courses_service.all(
            search=search, tag=tag, sort=sort, current_user=current_user
        )
    )


@courses_router.get(
    "/{id}", status_code=200, response_model=CourseWithSectionsResponseModel
)
def get_course(
    id: int = Path(ge=1),
    sort: SortType | None = None,
    sort_by: SortBySectionAttribute | None = None,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Users can access course along with its sections depending on their role, subscription and the course(active/inactive) status."""

    course_with_sections = courses_service.get_by_id(
        id, sort=sort, sort_by=sort_by, current_user=current_user
    )

    if course_with_sections == False:
        return Forbidden(
            content="Only users with subscription / confirmed subscription have access to Premium courses."
        )
    elif course_with_sections == None:
        return NotFound(content=f"Course with Id No. {id} not found.")

    return course_with_sections


@courses_router.patch("/{id}", status_code=201)
def update_course(
    info_to_update: CourseEditModel,
    id: int,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Teachers can update the main course information."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can edit courses.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.update_course(
        course_id=id, current_user=current_user, updated_course_data=info_to_update
    )

    if result == "Course not found.":
        return NotFound(content="Course not found!")
    elif result == "You are not the teacher of this course.":
        return Forbidden(content="You are not the teacher of this course.")
    elif result == None:
        return Conflict(content="Cannot update the same information.")

    return result


@courses_router.put("/{id}/sections", status_code=201)
def create_section_and_assign_to_course_handler(
    id: int,
    section: Section,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """The endpoint allows Teachers to create and assign section to their own courses with one action."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can add section to course!.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.create_section_and_assign_to_course(
        course_id=id, section=section, current_user=current_user
    )

    if not result:
        return Forbidden(
            f"Teacher with id:{current_user.id} cannot add section to this course!"
        )
    return result


@courses_router.put("/{id}/tags", status_code=201)
def create_tag_and_assign_to_course_handler(
    id: int, tag: Tag, current_user: UserOut = Depends(users_service.get_current_user)
):
    """The endpoint allows Teachers to create and assign tag to specific course with one action."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can add section to course!.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.create_tag_and_assign_to_course(
        course_id=id, tag=tag, current_user=current_user
    )

    if not result:
        return Forbidden(
            f"Teacher with id:{current_user.id} cannot add tag to this course!"
        )
    return result


@courses_router.delete("/{id}", status_code=204)
def unsubscribe_from_course(
    id: int, current_user: UserOut = Depends(users_service.get_current_user)
):
    """Students are able to unsubscribe from premium courses."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="student, admin",
        message="Only Students can unsubscribe from courses.",
    )

    if is_forbidden:
        return is_forbidden

    unsubscribed_successfully = courses_service.unsubscribe_from_course(
        id=id, current_user=current_user
    )

    if unsubscribed_successfully:
        return NoContent()
    elif not unsubscribed_successfully:
        return NotFound(
            content=f"Such Subscription for Course with ID No.{id} does not exist."
        )


@courses_router.put("/{id}", status_code=201)
def subscribe_to_course(
    id: int = Path(ge=1),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Students can subscribe to public courses or sent subscription requests for premium courses. Constraint for premium courses: 5 at a time)."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="student, admin",
        message="Only Students can subscribe to courses.",
    )

    if is_forbidden:
        return is_forbidden

    subscribed_successfully = courses_service.request_subscription(id, current_user)

    if subscribed_successfully == (True, True):
        return Created(content="You have successfully subscribed to the public course.")
    elif subscribed_successfully == (True, False):
        (
            teacher_email,
            course_title,
            s_name,
            teacher_name,
        ) = courses_service.get_teacher_email_by_course_id(id, current_user.id)
        mailjet.email_students_on_course(
            teacher_email,
            course_title,
            s_name,
            mailjet.email_html_template_subscription,
            teacher_name,
        )

        return Created(
            content="Your request for subscription to the course was received."
        )
    elif subscribed_successfully == "Course does not exist.":
        return NotFound(content=f"Course with ID No. {id} does not exist.")
    elif subscribed_successfully == "Request already exist.":
        return Conflict(
            f"You already requested / have access to Course with ID No.{id}."
        )
    elif "Maximum" in subscribed_successfully:
        return Conflict(
            content="You have reached the maximum number of premium course subscriptions."
        )


@courses_router.patch("/{id}/sections", status_code=201)
def update_section_information(
    id: int,
    section: Section,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Teachers are able to update section information."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Students can subscribe to courses.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.patch_section(
        course_id=id, section=section, current_user=current_user
    )

    if result == "Only owner can edit sections.":
        return Forbidden(result)
    elif result == "Course not found.":
        return NotFound(result)
    elif result == "Section not found.":
        return NotFound(result)
    return result


@courses_router.put("/{id}/rates", status_code=201)
def add_rate_information(
    id: int,
    rate: conint(ge=1, le=10),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Students are able to rate a specific Course within the range from 1 (Min) to 10 (Max)."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="student",
        message="Only Students can rate courses.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.add_rate_to_course(
        course_id=id, rate=rate, current_user=current_user
    )

    if result == "Course does not exist!":
        return NotFound("Course does not exist!")
    elif result == "You can only give 1 rate per course.":
        return Conflict("You can only give 1 rate per course.")
    elif result == "User does not have access to the course.":
        return Conflict("User does not have access to the course.")
    return result


@courses_router.put("/{id}/home-pages", status_code=201)
def set_home_page_to_course(
    id: int,
    url_address: HomePageInput,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Teachers can set and update a Course Home Page."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only creator can add home picture.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.add_put_home_page_to_course(
        course_id=id, url_address=url_address, current_user=current_user
    )

    if result == f"There is no course with id:{id}!":
        return NotFound(f"There is no course with id:{id}!")
    elif result == f"You dont have permission for this course!":
        return Forbidden(result)
    return result


@courses_router.get("/{id}/progress")
def get_course_progress(
    id: conint(ge=1), current_user: UserOut = Depends(users_service.get_current_user)
):
    """Students can track their progress in a specific course."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="student",
        message="Only Students can see course progress.",
    )
    if is_forbidden:
        return is_forbidden

    return courses_service.course_progress(id, current_user)


@courses_router.get("/{id}/rates", status_code=200)
def view_course_rates(
    id: int, current_user: UserOut = Depends(users_service.get_current_user)
):
    """Admins can view report on Students that rated specific Course."""

    is_forbidden = users_service.validate_role(
        current_user, requested_role="admin", message="Only admins have access."
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.view_students_that_rated_course(course_id=id)

    if not result:
        return NotFound(f"Course with id:{id} does not have rates!")
    return result


@courses_router.put("/{id}/status", status_code=201)
def update_course_active_status(
    id: conint(ge=1) = Path(),
    course_status: CourseStatus = Query(),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Teachers can change the status (deactivate or activate) their own courses only when there are no students subscribed.
    A submitted but not approved request for subscription is not considered as student subscription.
    """

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers could activate / deactivate courses.",
    )
    if is_forbidden:
        return is_forbidden

    updated_course = courses_service.update_course_active_status(
        id=id, course_status=course_status, current_user=current_user
    )

    if updated_course == "Not found":
        return NotFound(content=f"Course with ID No. {id} does not exist.")
    elif updated_course == "Not teacher":
        return Conflict(
            content=f"Only author of the course has the option to change its status."
        )
    elif updated_course:
        if course_status == "Inactive":
            return Created(content=f'Course status successfully changed to "Inactive".')
        else:
            return Created(content=f'Course status successfully changed to "Active".')
    else:
        return Conflict(
            content=f"You cannot change the Course status due to active (subscribed) students."
        )


@courses_router.get("/students/report", status_code=200)
def select_courses_by_teacher_or_student(
    teacher_id: int | None = None,
    student_id: int | None = None,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Admins can search for spefic Course, filtered by Teacher and / or Student."""

    is_forbidden = users_service.validate_role(
        current_user, requested_role="admin", message="Only admins have access."
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.select_courses_by_teacher_or_student_handler(
        teacher_id=teacher_id, student_id=student_id
    )

    if (
        result
        == f"Teacher with id:{teacher_id} doest not have course with student with id{student_id}."
    ):
        return NotFound(result)
    elif result == f"Teacher with id:{teacher_id} doest not have courses.":
        return NotFound(result)
    elif result == f"Student with id:{student_id} doest not have courses.":
        return NotFound(result)
    else:
        return result


@courses_router.delete("/{course_id}/sections/{section_id}", status_code=204)
def remove_section_from_course(
    course_id: int,
    section_id: int,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """The endpoint allows Teachers to remove specific section from their course."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can add section to course!.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.remove_section_from_course(
        course_id=course_id, section_id=section_id, current_user=current_user
    )

    if not result:
        return Forbidden(
            f"Teacher with id:{current_user.id} cannot delete section from this course!"
        )
    return result


@courses_router.delete("/{course_id}/tags/{tag_id}", status_code=204)
def delete_tag_from_course(
    course_id: int,
    tag_id: int,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """The endpoint allows Teachers to remove specific section from their own course."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can add section to course!.",
    )
    if is_forbidden:
        return is_forbidden

    result = courses_service.remove_tag_from_course(
        course_id=course_id, tag_id=tag_id, current_user=current_user
    )
    
    if result == "Permission denied.":
        return Forbidden(result)
    elif result == "Course not found.":
        return NotFound(result)
    elif result == "Tag not found":
        return NotFound(result)
    else:
        return result
