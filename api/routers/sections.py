from fastapi_pagination import Page, paginate
from api.common.responses import NotFound, Forbidden
from fastapi import APIRouter, Path, Depends, Query
from api.services import sections_service, users_service
from api.data.models import Section, SortType, SortBySectionAttribute, UserOut

section_router = APIRouter(prefix="/sections", tags=["Sections"])


@section_router.get("", status_code=200, response_model=Page[Section])
def get_sections(
    sort: SortType | None = None,
    sort_by: SortBySectionAttribute | None = None,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Returns a list of all sections (if admin/teacher), else: only to public or premium active courses where user has subscription."""

    return paginate(
        sections_service.all_sections(
            sort=sort, sort_by=sort_by, current_user=current_user
        )
    )


@section_router.get("/{id}", status_code=200, response_model=Section)
def get_section(
    id: int = Path(ge=1),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Returns a Section to which user has access. Can be accessed by all users (students/teachers/admins)."""

    section = sections_service.get_by_id(id=id, current_user=current_user)

    if section == "premium":
        return Forbidden(
            content="Only subscribed users have access to sections from Premium courses. Please subscribe."
        )
    elif not section:
        return NotFound(content=f"Section with Id No. {id} not found.")
    sections_service.update_section_is_view(id, current_user)
    return section
