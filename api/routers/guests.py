from pydantic import conint, constr
from fastapi_pagination import Page, paginate
from api.common.responses import NotFound
from api.services import users_service
from fastapi import APIRouter, Depends
from api.services import courses_service, tags_service
from api.data.models import SortType, CourseResponseModel, SortByTagAttribute, Tag
from api.services import users_service, courses_service
from api.common.responses import NotFound
from fastapi import APIRouter, status, Depends
from fastapi.security import OAuth2PasswordRequestForm
from api.data.models import User, Token


guests_router = APIRouter(prefix="/guests", tags=["Guests"])


@guests_router.post("/signup", status_code=status.HTTP_201_CREATED)
def create_user(user: User):
    
    return users_service.create(user)


@guests_router.post("/login", status_code=status.HTTP_201_CREATED, response_model=Token)
def login(user: OAuth2PasswordRequestForm = Depends()):
    real_password = users_service.get_password(user.username)
    if not real_password:
        return NotFound(content=f"Invalid credentials!")

    is_valid = users_service.compare_passwords(user.password, real_password)
    if not is_valid:
        return NotFound(content=f"Invalid credentials!")

    access_token = users_service.create_access_token(data={"email": user.username})
    return {"access_token": access_token, "token_type": "bearer"}


@guests_router.get(
    "/courses", status_code=200, response_model=Page[CourseResponseModel]
)
def get_public_courses_for_quest_users(
    rating: conint(ge=1, le=10) | None = None,
    tag: constr(strip_whitespace=True, min_length=2, max_length=30) | None = None,
    sort: SortType | None = None,
):
    """Anonymous users are able to view the title and description of available public courses"""

    return paginate(
        courses_service.get_public_courses_for_guests(rating=rating, tag=tag, sort=sort)
    )


@guests_router.get("/tags", status_code=200, response_model=Page[Tag])
def get_public_tags(
    sort: SortType | None = None, sort_by: SortByTagAttribute | None = None
):
    """Returns Tags of all public (only) courses. Premium courses are not shown."""

    return paginate(tags_service.get_public_tags(sort=sort, sort_by=sort_by))
