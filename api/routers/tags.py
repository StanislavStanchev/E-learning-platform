from fastapi import APIRouter, Path, Depends
from fastapi_pagination import Page, paginate
from api.common.responses import NotFound, Conflict
from api.services import tags_service, users_service
from api.data.models import (
    Tag,
    SortByTagAttribute,
    SortType,
    TagWithCoursesResponseModel,
    UserOut,
)

tags_router = APIRouter(prefix="/tags", tags=["Tags"])


@tags_router.get("", status_code=200, response_model=Page[Tag])
def get_all_tags(
    sort: SortType | None = None,
    sort_by: SortByTagAttribute | None = None,
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Returns Tags of all courses (including Premium)."""

    return paginate(tags_service.all(sort=sort, sort_by=sort_by))


@tags_router.get("/{id}", status_code=200, response_model=TagWithCoursesResponseModel)
def get_tag(
    id: int = Path(ge=1),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Returns a Tag along with the related courses. Tag can be accessed only by logged-in user."""

    tag_with_courses = tags_service.get_by_id(id=id, current_user=current_user)

    if tag_with_courses == "not assigned":
        return Conflict(content="The tag exists, but no courses are assigned to it.")
    elif not tag_with_courses:
        return NotFound(content=f"Tag with Id No.{id} not found.")

    return tag_with_courses
