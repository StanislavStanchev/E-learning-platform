from pydantic import constr
from fastapi_pagination import Page, paginate
from fastapi import APIRouter, Depends, Query
from api.services import students_service, users_service
from api.common.responses import Created, NotFound, NoContent
from api.data.models import StudentWithCourse, SortType, SortStudentsBy, UserOut

students_router = APIRouter(prefix="/students", tags=["Students"])


@students_router.get("", response_model=Page[StudentWithCourse])
def teacher_report_for_subscribed_students(
    current_user: UserOut = Depends(users_service.get_current_user),
    search: constr(strip_whitespace=True, min_length=1, max_length=30) | None = None,
    sort: SortType | None = None,
    sort_by: SortStudentsBy | None = None,
):
    """Each Teacher can access a report with the subscribed students in their courses."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher",
        message="Only Teachers can access the report to subscribed students.",
    )

    if is_forbidden:
        return is_forbidden

    return paginate(
        students_service.teacher_report_for_subscribed_students(
            current_user, search, sort, sort_by
        )
    )


@students_router.put("/subscriptions", status_code=201)
def approve_student_subscription_to_course(
    student_id: int = Query(ge=1),
    course_id: int = Query(ge=1),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Teachers / Admins can approve course subscription requests submitted by students."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="teacher, admin",
        message="Only Teachers / Admins can approve course subscriptions.",
    )
    if is_forbidden:
        return is_forbidden

    subscribtion_approved = students_service.subscription_approval(
        student_id, course_id, current_user
    )

    if subscribtion_approved:
        return Created(
            content=f"Subscription approved. Student No. {student_id} granted access to course {course_id}."
        )
    else:
        return NotFound(
            content=f"Not found request by Student No. {student_id} to Course No. {course_id}."
        )


@students_router.delete("", status_code=204)
def remove_student_from_course(
    student_id: int = Query(ge=1),
    course_id: int = Query(ge=1),
    current_user: UserOut = Depends(users_service.get_current_user),
):
    """Admins could remove students from courses."""

    is_forbidden = users_service.validate_role(
        current_user,
        requested_role="admin",
        message="Only Admins can remove Students from Course.",
    )
    if is_forbidden:
        return is_forbidden

    student_removed = students_service.remove_student_from_course(
        student_id, course_id, current_user
    )

    if student_removed:
        return NoContent()
    else:
        return NotFound(
            content=f"Not found Course No. {course_id} with Student No. {student_id}."
        )
