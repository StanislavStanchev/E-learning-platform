from api.services import users_service
from api.common.responses import NotFound
from fastapi import APIRouter, status, Depends
from fastapi.security import OAuth2PasswordRequestForm
from api.data.models import User, Token, UserOut, UpdatePassword
from itsdangerous import SignatureExpired
from api.common.mailjet import s

users_router = APIRouter(prefix="/users", tags=["Users"])


@users_router.get("")
def get_user_info(user_auth: UserOut = Depends(users_service.get_current_user)):
    return users_service.view_info(user_auth)


@users_router.put("", status_code=201)
def update_user_info(
    user_update: User, user_auth: UserOut = Depends(users_service.get_current_user)
):
    return users_service.update_info(user_update, user_auth)


@users_router.patch("/password", status_code=201)
def update_user_password(
    update_password: UpdatePassword,
    user_auth: UserOut = Depends(users_service.get_current_user),
):
    return users_service.update_password(update_password.password, user_auth)


@users_router.get("/confirm-email/{token}")
def confirm_email(token: str):
    try:
        email = s.loads(
            token, salt="email-confirm", max_age=3600
        )  # Token is valid for 1 hour

    except SignatureExpired:
        return {"message": "The token is expired!"}

    users_service.user_is_verified(email)

    return {"message": "Email address confirmed!"}
