from api.common import mailjet
from api.config import settings_token
from pydantic import ValidationError
from api.common.responses import Forbidden
from datetime import datetime, timedelta
from passlib.context import CryptContext
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordBearer
from fastapi import status, HTTPException, Depends
from jose import ExpiredSignatureError, JWTError, jwt
from api.data.models import User, TokenData, UserOut, ViewStudent, ViewTeacher
from api.data.database import read_query, update_query, _get_connection
import mariadb

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/guests/login")

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def create(user: User):
    """Creates User, where the email should be unique."""

    hashed_password = pwd_context.hash(user.password)
    try:
        with _get_connection() as conn:
            cursor = conn.cursor()
            conn.autocommit = False
            cursor.execute(
                "INSERT INTO users(role,email, f_name, l_name, password) VALUES(?,?,?,?,?)",
                (
                    0 if user.role == "student" else 1,
                    user.username,
                    user.first_name,
                    user.last_name,
                    hashed_password,
                ),
            )
            id = cursor.lastrowid

            if user.role == "student":
                cursor.execute(
                    "INSERT INTO user_verification_access(is_verified,has_access, users_id) VALUES(?,?, ?)",
                    (1, 1, id),
                )
            elif user.role == "teacher":
                cursor.execute(
                    "INSERT INTO contact_info(phone_number, linkedin_account, users_id) VALUES(?,?,?)",
                    (user.phone_number, user.linkedin_account, id),
                )

                mailjet.verify_email(
                    user.username, (user.first_name + " " + user.last_name)
                )
                cursor.execute(
                    "INSERT INTO user_verification_access(is_verified,users_id) VALUES(?,?)",
                    (0, id),
                )
                message = """We have sent you an email to confirm your account.
                Please check your inbox and follow the instructions in the email to activate your account."""
                conn.commit()
                return JSONResponse(content=message)
            conn.commit()
            return (
                f"You have successfully created a user with username: {user.username}."
            )
    except mariadb.IntegrityError as e:
        if "Duplicate entry" in str(e) and "email_UNIQUE" in str(e):
            return JSONResponse(
                status_code=400, content="This email is already registered."
            )
        elif "Duplicate entry" in str(e) and "phone_number_UNIQUE" in str(e):
            return JSONResponse(
                status_code=400, content="This phone number is already registered."
            )
        elif "Duplicate entry" in str(e) and "linkedin_account_UNIQUE" in str(e):
            return JSONResponse(
                status_code=400, content="This linkedin_account is already registered."
            )


def get_password(username: str, get_data_func=None) -> str | None:
    """'Returns User password based on its email."""
    if get_data_func == None:
        get_data_func = read_query
    try:
        data = get_data_func("SELECT password FROM users WHERE email = ? ", (username,))
        return data[0][0]
    except:
        return False


def compare_passwords(provided_password, real_password) -> bool:
    """Compare login password with the stored password in the DB."""
    return pwd_context.verify(provided_password, real_password)


def create_access_token(data: dict):
    """Creates JWT which could be stored by the Web client and the passed with the headers of each request."""
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(
        minutes=settings_token.access_token_expire_minites
    )
    to_encode.update({"exp": expire})

    encoded_jwt = jwt.encode(
        to_encode, settings_token.secret_key, settings_token.algorithm
    )
    return encoded_jwt


# def get_current_role(email: str, get_data_func=None) -> str | None:
#     """Returns User's current role based on the provided email."""
#     if get_data_func is None:
#         get_data_func = read_query

#     data = get_data_func("SELECT role FROM users WHERE email = ?", (email,))
#     return "student" if data[0][0] == 0 else ("teacher" if data[0][0] == 1 else "admin")


def get_by_email(email: str, get_data_func=None) -> UserOut | None:
    """Returns User, found by Email."""

    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        """SELECT u.id,u.email,u.role, uva.is_verified, uva.has_access
                         FROM users u
                         LEFT JOIN user_verification_access uva on u.id = uva.users_id
                         WHERE u.email = ?""",
        (email,),
    )
    if not data:
        return None
    if data[0][3] == 0:
        raise HTTPException(status_code=401, detail="User is not verified")
    if data[0][4] == 0:
        raise HTTPException(status_code=401, detail="User is deactivated")
    return UserOut.from_query_result(data[0][0], data[0][1], data[0][2])


def credentials_exception(
    headers,
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail=f"Could not validate credentials",
):
    """Creates Unauthorized exception."""
    credentials_exception = HTTPException(
        status_code=status_code, detail=detail, headers=headers
    )
    return credentials_exception


def verify_access_token(token: str):
    """Verifies the JWT provided in the header of the request."""
    authenticate_value = 'Bearer scope="undefined"'
    try:
        payload = jwt.decode(token, settings_token.secret_key, settings_token.algorithm)
        email: str = payload.get("email")
        authenticate_value = f'Bearer scope="{email}"'

        if not email:
            raise credentials_exception(
                headers={"WWW-Authenticate": f"{authenticate_value}"}
            )

        token_data = TokenData(email=email)
    except ExpiredSignatureError:
        raise HTTPException(
            status_code=403,
            detail="Token has been expired. Please log-in to access the resource.",
        )
    except (JWTError, ValidationError):
        raise credentials_exception(
            headers={"WWW-Authenticate": f"{authenticate_value}"}
        )

    return token_data


def get_current_user(token: str = Depends(oauth2_scheme)) -> UserOut | None:
    """Returns the information about the current (logged-in) user."""
    token: TokenData = verify_access_token(token)

    return get_by_email(token.email)


def view_info(user_auth: UserOut, get_data_func=None):
    """Returns data about the User."""
    if get_data_func is None:
        get_data_func = read_query
    if user_auth.role == "student":
        data = get_data_func(
            "SELECT role, email, f_name, l_name FROM users WHERE email = ?",
            (user_auth.username,),
        )
        return ViewStudent.from_query_result(
            data[0][0], data[0][1], data[0][2], data[0][3]
        )
    if user_auth.role == "teacher":
        data = get_data_func(
            """SELECT users.role, users.email, users.f_name, users.l_name, contact_info.phone_number, contact_info.linkedin_account
                               FROM users LEFT JOIN contact_info ON users.id = contact_info.users_id WHERE users.email = ? """,
            (user_auth.username,),
        )
        return ViewTeacher.from_query_result(
            data[0][0], data[0][1], data[0][2], data[0][3], data[0][4], data[0][5]
        )


def update_info(update_user: User, user_auth: UserOut, get_data_func=None):
    """Updates the Info of specific User."""

    if get_data_func is None:
        get_data_func = update_query

    conn = _get_connection()
    cursor = conn.cursor()
    try:
        if user_auth.role == "student":
            cursor.execute(
                """UPDATE users SET f_name = CASE WHEN ? IS NOT NULL THEN ? ELSE f_name END, 
                            l_name = CASE WHEN ? IS NOT NULL THEN ? ELSE l_name END WHERE id = ?""",
                (
                    update_user.first_name,
                    update_user.first_name,
                    update_user.last_name,
                    update_user.last_name,
                    user_auth.id,
                ),
            )

        elif user_auth.role == "teacher":
            cursor.execute(
                """UPDATE users SET f_name = CASE WHEN ? IS NOT NULL THEN ? ELSE f_name END, 
                            l_name = CASE WHEN ? IS NOT NULL THEN ? ELSE l_name END WHERE id = ?""",
                (
                    update_user.first_name,
                    update_user.first_name,
                    update_user.last_name,
                    update_user.last_name,
                    user_auth.id,
                ),
            )

            cursor.execute(
                """UPDATE contact_info SET phone_number = CASE WHEN ? IS NOT NULL THEN ? ELSE phone_number END, 
                          linkedin_account = CASE WHEN ? IS NOT NULL THEN ? ELSE linkedin_account END WHERE users_id = ?""",
                (
                    update_user.phone_number,
                    update_user.phone_number,
                    update_user.linkedin_account,
                    update_user.linkedin_account,
                    user_auth.id,
                ),
            )

        conn.commit()
        return JSONResponse(content="User info updated.")

    except Exception as e:
        conn.rollback()
        raise e


def validate_role(user_auth: UserOut, requested_role: str, message: str):
    """Validates if the expected User role equals the required role(s)."""
    if user_auth.role not in requested_role:
        return Forbidden(content=message)


def update_password(new_password, user_auth: UserOut, get_data_func=None):
    """Updates User password."""
    if get_data_func is None:
        get_data_func = update_query
    hashed_password = pwd_context.hash(new_password)
    get_data_func(
        "UPDATE users SET password = ? WHERE email = ?",
        (hashed_password, user_auth.username),
    )
    return JSONResponse(
        content=f"Password of user {user_auth.username} updated successfully"
    )


def validate_teacher_id_for_course_id(teacher_id: int, course_id: int):
    """Validates if the spefici Teacher is the author of the Course."""
    if teacher_id != course_id:
        return Forbidden("Do not have permission to edit this course!")

def user_is_verified(email: str, update_data_func = None):
    if update_data_func == None:
        update_data_func = update_query
    update_data_func('''
        UPDATE user_verification_access
        JOIN users
        ON user_verification_access.users_id = users.id
        SET user_verification_access.is_verified = 1
        WHERE users.email = %s
    ''', (email,))
