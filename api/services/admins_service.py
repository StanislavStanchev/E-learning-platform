import api.services.courses_service
from mariadb import IntegrityError
from api.data.database import read_query, update_query, _get_connection
from api.data.models import UserOut, AdminReportResponseModel, CourseStatus, UserStatus
from api.common.responses import NotFound, Conflict
from fastapi.responses import JSONResponse
from api.common.mailjet import mailjet


def admin_report_for_courses(
    current_user: UserOut,
    search: str | None = None,
    sort: str | None = None,
    get_data_func=None,
):
    """Responds with a list of all Courses available along with their rating and enrolled students. Can be accessed by Admins only."""
    if get_data_func is None:
        get_data_func = read_query

    if current_user.role == "admin":
        sql = """SELECT c.id, c.title, c.is_public, r.avg_rating, uhc.enrolled_students
                FROM courses AS c 
                LEFT JOIN (SELECT course_id, AVG(rate) as avg_rating
                    FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
                LEFT JOIN (SELECT course_id, COUNT(user_id) AS enrolled_students
                    FROM users_has_courses AS uhc
                    WHERE uhc.has_access = 1
                    GROUP BY course_id) AS uhc ON c.id = uhc.course_id"""

        where_clauses = []

        if search:
            sql += " WHERE c.title LIKE ?"
            where_clauses.append(f"%{search}%")

        sql += " GROUP BY c.id"

        data = get_data_func(sql, (*where_clauses,))

        courses = list(AdminReportResponseModel.from_query_result(*row) for row in data)

        if sort and (sort == "asc" or sort == "desc"):
            courses = api.courses_service.sort_categories(
                data, reverse=sort == "desc"
            )

        return courses


def update_course_active_status(
    id: int, course_status: CourseStatus, current_user: UserOut, update_data_func=None
) -> bool:
    """Admins can deactivate/activate courses. The enrolled students will receive a notification that the course is no longer active.
    Can be accessed only by Admin users, else: returns None."""
    if update_data_func is None:
        update_data_func = update_query

    if current_user.role == "admin":
        try:
            sql = """INSERT INTO course_status (course_id, is_active) VALUES (?, ?)
                    ON DUPLICATE KEY UPDATE is_active = IF(course_id = ?, VALUES(is_active), is_active)"""

            affected_rows = update_data_func(
                sql, (id, 1 if course_status == "Active" else 0, id)
            )

            if affected_rows and course_status == "Inactive":
                _send_email_to_enrolled_students_re_deactivated_course(
                    id=id, current_user=current_user
                )
                return True
            else:
                return False

        except IntegrityError as e:
            if "Cannot add or update a child row" in str(e):
                return "Not found"


def _send_email_to_enrolled_students_re_deactivated_course(
    id: int, current_user: UserOut, get_data_func=None
):
    """Sends email notification to enrolled students that the course is no longer active.
    Can be accessed only by Admin users, else: returns None."""
    if get_data_func is None:
        get_data_func = read_query

    if current_user.role == "admin":
        sql = """SELECT u.email, c. title, CONCAT(u.f_name, ' ', u.l_name) AS name
                FROM users AS u
                LEFT JOIN users_has_courses AS uhc ON u.id = uhc.user_id
                LEFT JOIN courses AS c ON c.id = uhc.course_id
                WHERE uhc.has_access = 1 AND uhc.course_id = ?"""

        data = get_data_func(sql, (id,))

        for row in data:
            mailjet.email_students_on_course(
                email_receiver=row[0],
                course_name=row[1],
                student_name=row[2],
                html=mailjet.email_html_template_deactivate,
            )


def change_user_access(
    id: int, user_status: UserStatus, update_data_func=None, get_data_func=None
):
    if update_data_func == None:
        update_data_func = update_query
    if get_data_func == None:
        get_data_func = read_query

    data = get_data_func(
        """SELECT u.role, uva.has_access
                            FROM users u
                            JOIN user_verification_access uva ON u.id = uva.users_id
                            WHERE u.id = ?""",
        (id,),
    )
    if not data:
        return NotFound(content=f"There is no user with id {id}!")
    role = "teacher" if data[0][0] == 1 else "student"
    has_access = data[0][1]

    if user_status == UserStatus.Active:
        if has_access == 1:
            return Conflict(content=f"{role.capitalize()}  is already active!")

        affected_rows = update_data_func(
            """
            UPDATE user_verification_access
            SET has_access = 1
            WHERE users_id = ?  and has_access = 0
        """,
            (id,),
        )
        return JSONResponse(content=f"{role.capitalize()} is now active!")

    elif user_status == UserStatus.Inactive:
        if role != "student":
            return Conflict(content="Only students can be deactivated!")
        if has_access == 0:
            return Conflict(content=f"{role.capitalize()}  is already inactive!")
        affected_rows = update_data_func(
            """
            UPDATE user_verification_access uva
            JOIN users u ON uva.users_id = u.id
            SET uva.has_access = 0
            WHERE uva.users_id = ? AND uva.has_access = 1 AND u.role = '0'
        """,
            (id,),
        )
        return JSONResponse(content=f"{role.capitalize()} is now inactive!")
