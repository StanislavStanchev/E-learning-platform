from api.data.database import read_query
from api.data.models import (Tag,
                        TagWithCoursesResponseModel,
                        SortByTagAttribute,
                        SortType,
                        CourseResponseModel,
                        UserOut)


def all(sort: SortType | None = None,
        sort_by: SortByTagAttribute | None =  None,
        get_data_func = None) -> list[Tag]:
    '''Returns Tags of all courses.'''
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('''SELECT t.id, t.area FROM tags AS t
        LEFT JOIN courses_has_tags AS cht ON cht.tag_id = t.id
        LEFT JOIN courses AS c ON cht.course_id = c.id
        LEFT JOIN course_status AS cs ON c.id = cs.course_id
        WHERE c.is_public in (1,0) AND (cs.is_active = 1 OR cs.is_active IS NULL)
        GROUP BY t.id''', ())

    tags = list(Tag.from_query_result(*row) for row in data)
    
    if sort or sort_by:
        tags = sort_tags(tags, attribute=sort_by, reverse=sort == 'desc')

    return tags


def get_public_tags(sort: SortType | None = None,
                    sort_by: SortByTagAttribute | None = None,
                    get_data_func = None) -> list[Tag]:
    '''Returns Tags of all public (only) courses. Premium courses are not shown.'''
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func('''SELECT t.id, t.area FROM tags AS t
    LEFT JOIN courses_has_tags AS cht ON cht.tag_id = t.id
    LEFT JOIN courses AS c ON cht.course_id = c.id
    LEFT JOIN course_status AS cs ON c.id = cs.course_id
    WHERE c.is_public = 1  AND (cs.is_active = 1 OR cs.is_active IS NULL)
    GROUP BY t.id''', ())

    tags = list(Tag.from_query_result(*row) for row in data)
    
    if sort or sort_by:
        tags = sort_tags(tags, attribute=sort_by, reverse=sort == 'desc')

    return tags


def get_by_id(id: int, current_user: UserOut, get_data_func = None):
    '''Returns a Tag along with the related courses. Tag can be accessed only by logged-in user.'''
    if get_data_func is None:
        get_data_func = read_query
    
    if current_user:
        data = get_data_func(
                '''SELECT
                CASE
                    WHEN c.is_public IN (1,0)
                    THEN CONCAT_WS('|^', t.id, t.area, c.id, c.title, c.description, c.objectives,
                                    c.is_public, CONCAT(u.f_name, ' ', u.l_name), r.avg_rating)
                    WHEN t.id IS NULL THEN '[]'
                    ELSE CONCAT_WS('|^', NULL)
                END AS tag_with_courses
                FROM tags AS t
                LEFT JOIN courses_has_tags AS cht ON t.id = cht.tag_id
                LEFT JOIN courses AS c ON cht.course_id = c.id
                LEFT JOIN users AS u ON c.teacher_id = u.id
                LEFT JOIN (SELECT course_id, AVG(rate) as avg_rating
                    FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
                LEFT JOIN course_status AS cs ON c.id = cs.course_id
                WHERE t.id = ? AND (cs.is_active = 1 OR cs.is_active IS NULL)''', (id,))

    if data == []:
        return None  # Not Found
    elif not data[0][0]:
        return 'not assigned'
    else:
        courses = []
        for row in data:
            course_info_from_db = row[0].split('|^')[2:]
            courses.append(CourseResponseModel.from_query_result(*course_info_from_db))
        
        tag_id, tag_area = data[0][0].split('|^')[0:2]

        return (TagWithCoursesResponseModel.from_query_result(
                            tag=Tag.from_query_result(id=int(tag_id), area=tag_area),
                            courses=courses))
    

def sort_tags(lst: list[Tag], *, attribute = 'id', reverse=False) -> list[Tag]:
    '''Sorts list of Tags.'''
    if attribute == 'area':
        def sort_by(tag: Tag): return tag.area
    else:
        def sort_by(tag: Tag): return tag.id
    return sorted(lst, key=sort_by,
        reverse=reverse)