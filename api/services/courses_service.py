import api.services.sections_service
from api.data.database import read_query, update_query, insert_query, _get_connection
from api.data.models import (
    Course,
    CourseResponseModel,
    SortBySectionAttribute,
    SortType,
    CourseWithSectionsResponseModel,
    Section,
    Tag,
    UserOut,
    HomePageInput,
    CourseEditModel,
    UserRatedCourse,
    CourseTeacherResponseModel,
    CourseStatus,
)


def sort_courses(lst: list[CourseResponseModel], reverse=False):
    return sorted(lst, key=lambda c: c.title, reverse=reverse)


def create_course(
    course: Course,
    current_user: UserOut,
    insert_data_func=None,
):
    """Only Teachers can create course, return Forbidden if role of current_user is not Teacher, return course if success"""
    if insert_data_func is None:
        insert_data_func = insert_query
    status_value = 0 if course.is_public == "Premium" else 1

    insert_data_func(
        "INSERT INTO courses(title,description,objectives,teacher_id,is_public) VALUES (?,?,?,?,?)",
        (
            course.title,
            course.description,
            course.objectives,
            current_user.id,
            status_value,
        ),
    )
    return course


def all(
    current_user: UserOut,
    search: str | None = None,
    tag: str | None = None,
    sort: str | None = None,
    get_data_func=None,
):
    """Responds with a list of all Courses available (depending on the role, subscription and course active status)."""
    if get_data_func is None:
        get_data_func = read_query

    sql = """SELECT c.id, c.title, c.description, c.objectives, c.is_public,
                CONCAT(u.f_name, ' ', u.l_name) as teacher, r.avg_rating
                FROM courses AS c 
                LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id
                LEFT JOIN users AS u ON u.id = c.teacher_id
                LEFT JOIN courses_has_tags AS cht ON cht.course_id = c.id
                LEFT JOIN tags AS t ON cht.tag_id = t.id
                LEFT JOIN (SELECT course_id, AVG(rate) as avg_rating
                    FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
                LEFT JOIN course_status AS cs ON c.id = cs.course_id"""

    params = ()

    if search and tag:
        sql += " WHERE c.title LIKE ? AND t.area LIKE ?"
        params = [f"%{search}%", f"%{tag}%"]
    elif tag:
        sql += " WHERE t.area LIKE ?"
        params = f"%{tag}%"
    elif search:
        sql += " WHERE c.title LIKE ?"
        params = f"%{search}%"

    if (search or tag) and current_user.role == "student":
        sql += " AND (cs.is_active = 1 OR cs.is_active IS NULL)"
    elif current_user.role == "student":
        sql += " WHERE (cs.is_active = 1 OR cs.is_active IS NULL)"

    sql += " GROUP BY c.id"

    data = get_data_func(sql, (params,))

    courses = list(CourseResponseModel.from_query_result(*row) for row in data)

    if sort and (sort == "asc" or sort == "desc"):
        courses = sort_courses(data, reverse=sort == "desc")

    return courses


def get_by_title(title: str, get_data_func=None) -> CourseResponseModel:
    """Returns Course, searched by Title."""
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        """SELECT c.id, c.title, c.description, c.objectives, c.is_public, CONCAT(u.f_name, ' ', u.l_name) as teacher, r.avg_rating
                FROM courses AS c 
                LEFT JOIN users AS u ON c.teacher_id = u.id
                LEFT JOIN (SELECT course_id, AVG(rate) as avg_rating
                    FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
                LEFT JOIN course_status AS cs ON c.id = cs.course_id
                WHERE (cs.is_active = 1 OR cs.is_active IS NULL) AND c.title = ?""",
        (title,),
    )

    return next((CourseResponseModel.from_query_result(*row) for row in data), None)


def get_by_id(
    id: int,
    current_user: UserOut,
    sort: SortType | None = None,
    sort_by: SortBySectionAttribute | None = None,
    get_data_func=None,
) -> CourseWithSectionsResponseModel:
    """Returns actove course and its' sections if public and/or user has access else: None.
    Returns 'NO' when course does not exist of False when student does not have access.
    """
    if get_data_func is None:
        get_data_func = read_query

    if (
        current_user.role == "student"
    ):  # Show Course which is active and (public or premium but user has subscription).
        sql = """SELECT CASE
        WHEN NOT EXISTS (SELECT 1 FROM courses WHERE id = ?) OR cs.is_active = 0 THEN 'NO'
        WHEN EXISTS (SELECT 1 FROM users_has_courses AS uhc JOIN courses AS c
                    WHERE (course_id = ? AND user_id = ? AND has_access = 1))
        THEN CONCAT_WS('|^', c.id, c.title, c.description, c.objectives,
                c.is_public, CONCAT(u.f_name, ' ', u.l_name), r.avg_rating, '~^|',
                s.id, s.title, s.content, s.description, s.ext_link)
        END
        FROM courses AS c 
        LEFT JOIN users AS u ON c.teacher_id = u.id
        LEFT JOIN (SELECT * FROM sections WHERE id IN (SELECT section_id FROM courses_has_sections WHERE course_id = ?)) AS s ON 1=1
        LEFT JOIN course_status AS cs ON c.id = cs.course_id
        LEFT JOIN users_has_courses AS uhc ON c.id = ?
        LEFT JOIN (SELECT course_id, AVG(rate) AS avg_rating FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
        GROUP BY s.id"""

        params = (id, id, current_user.id, id, id)

    else:  # Show Course to every Teacher or Admin
        sql = """SELECT CASE
                    WHEN NOT EXISTS (SELECT 1 FROM courses WHERE id = ?) THEN 'NO'
                    ELSE CONCAT_WS('|^', c.id, c.title, c.description, c.objectives, c.is_public, CONCAT(u.f_name, ' ', u.l_name),
                        r.avg_rating, '~^|', s.id, s.title, s.content, s.description, s.ext_link)
                END
            FROM courses AS c 
            LEFT JOIN users AS u ON c.teacher_id = u.id
            LEFT JOIN (SELECT * FROM sections WHERE id IN (SELECT section_id FROM courses_has_sections WHERE course_id = ?)) AS s ON 1=1
            LEFT JOIN course_status AS cs ON c.id = cs.course_id
            LEFT JOIN users_has_courses AS uhc ON c.id = ?
            LEFT JOIN (SELECT course_id, AVG(rate) AS avg_rating FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
            GROUP BY s.id"""
        params = (id, id, id)

    data = get_data_func(sql, params)

    if data[0][0] == "NO":
        return None
    elif not data[0][0]:
        return False
    else:
        course_data = data[0][0].split("|^")
        course_data = course_data[: course_data.index("~^|")]
        course_result = CourseResponseModel.from_query_result(*course_data)

        sections_result = []
        for row in data:
            if row[0] is not None:
                row = row[0].split("|^")
                if row[row.index("~^|") + 1 :] != []:
                    sections_result.append(
                        Section.from_query_result(*(row[row.index("~^|") + 1 :]))
                    )
                else:
                    break
            elif row[0] == None:
                break

        if sort:
            sections_result = api.services.sections_service.sort_sections(
                sections_result, attribute=sort_by, reverse=sort == "desc"
            )

        return CourseWithSectionsResponseModel.from_query_result(
            course=course_result, sections=sections_result
        )


def update_course(
    course_id: int,
    current_user: UserOut,
    updated_course_data: CourseEditModel = None,
    update_data_func=None,
    read_data_fun=None,
):
    """Only Teachers can edit course, return Forbidden if role of current_user is not Teacher, return Json response if success"""
    if update_data_func is None:
        update_data_func = update_query
    if read_data_fun is None:
        read_data_fun = read_query

    query_teacher = read_data_fun(
        "SELECT teacher_id FROM courses WHERE id = ?", ((course_id,))
    )

    if query_teacher == []:
        return "Course not found."
    elif query_teacher[0][0] != current_user.id:
        return "You are not the teacher of this course."
    elif updated_course_data is None:
        return None
    query = "UPDATE courses SET "

    if updated_course_data.is_public == "Public":
        updated_course_data.is_public = 1
    elif updated_course_data.is_public == "Premium":
        updated_course_data.is_public = 0

    params = []

    if updated_course_data.title:
        query += "title=?, "
        params.append(updated_course_data.title)

    if updated_course_data.description:
        query += "description=?, "
        params.append(updated_course_data.description)

    if updated_course_data.objectives:
        query += "objectives=?, "
        params.append(updated_course_data.objectives)

    if updated_course_data.is_public is not None:
        query += "is_public=?, "
        params.append(updated_course_data.is_public)

    query = query.rstrip(", ")
    query += " WHERE id=? AND  teacher_id=?"
    params.extend([course_id, current_user.id])

    result = update_data_func(query, params)

    if not result:
        return None
    return {"updated": "OK"}


def create_section_and_assign_to_course(
    course_id, section: Section, current_user: UserOut, insert_data_func=None
):
    """Only Teachers can create section, return Forbidden if role of current_user is not Teacher, return course_id and section if success"""
    if insert_data_func is None:
        insert_data_func = insert_query
    with _get_connection() as conn:
        cursor = conn.cursor()
        conn.autocommit = False

        cursor.execute(
            """INSERT INTO sections (title, content, description, ext_link)
                            SELECT ?, ?, ?, ? WHERE EXISTS (SELECT id,teacher_id FROM courses WHERE id = ? AND teacher_id = ?)""",
            (
                section.title,
                section.content,
                section.description,
                section.ext_link,
                course_id,
                current_user.id,
            ),
        )
        section_id = cursor.lastrowid

        if not section_id:
            return None

        cursor.execute(
            "INSERT INTO courses_has_sections(course_id, section_id) VALUES (?, ?)",
            (int(course_id), int(section_id)),
        )
        section.id = section_id
        conn.commit()

        return {"course_id": course_id}, section


def unsubscribe_from_course(
    id: int, current_user: UserOut, update_data_func=None
) -> bool | str:
    """User can unsubscribe from Course if the Course exist and the user has active subscription."""
    if update_data_func is None:
        update_data_func = update_query

    affected_rows = update_data_func(
        """UPDATE users_has_courses
        LEFT JOIN courses AS c ON users_has_courses.course_id = c.id
        SET has_access = 0
        WHERE users_has_courses.course_id = ? AND users_has_courses.user_id = ?""",
        (id, current_user.id),
    )

    return True if affected_rows > 0 else False


def create_tag_and_assign_to_course(
    course_id, tag: Tag, current_user: UserOut, insert_data_func=None
):
    """Only Teachers can create tag, return Forbidden if role of current_user is not Teacher, return course_id and tag if success"""
    if insert_data_func is None:
        insert_data_func = insert_query

    with _get_connection() as conn:
        cursor = conn.cursor()
        conn.autocommit = False

        cursor.execute(
            "INSERT INTO tags (area) SELECT ? FROM courses WHERE id = ? and teacher_id = ?",
            (tag.area, course_id, current_user.id),
        )
        tag_id = cursor.lastrowid

        if not tag_id:
            return None

        cursor.execute(
            "INSERT INTO courses_has_tags(course_id, tag_id) VALUES (?, ?)",
            (int(course_id), int(tag_id)),
        )

        tag.id = tag_id
        conn.commit()

        return {"course_id": course_id}, tag


def remove_section_from_course(
    course_id, section_id, current_user: UserOut, update_data_func=None
):
    """Deletes Section from Course. The Section is no longer available in the Database."""
    if update_data_func is None:
        update_data_func = update_query

    result = update_data_func(
        """DELETE chs FROM courses_has_sections AS chs
                            JOIN courses AS c ON c.id = chs.course_id
                            WHERE (course_id = ? AND section_id = ? AND c.teacher_id = ?)""",
        (course_id, section_id, current_user.id),
    )
    return result


def remove_tag_from_course(
    course_id, tag_id, current_user: UserOut, update_data_func=None, read_data_func=None
):
    """Deletes the connection between a Tag and a specific Course. The same Tag can be reused again."""
    if update_data_func is None:
        update_data_func = update_query
    if read_data_func is None:
        read_data_func = read_query

    query = """
    SELECT CASE
        WHEN c.teacher_id = ? THEN 'OK'
        ELSE 'NO'
    END AS teacher_permission,
    CASE
        WHEN chc.course_id = ? THEN 'OK'
        ELSE 'NO'
    END AS course_permission,
    CASE
        WHEN chc.tag_id = ? THEN 'OK'
        ELSE 'NO'
    END AS section_permission
    FROM courses_has_tags AS chc
    LEFT JOIN courses AS c ON chc.course_id = c.id
    LIMIT 1"""

    params = (current_user.id, course_id, tag_id)
    result = read_data_func(query, params)

    if result[0][0] == "NO":
        return "Permission denied."
    elif result[0][1] == "NO":
        return "Course not found."
    elif result[0][2] == "NO":
        return "Tag not found"

    del_tag = update_data_func(
        "DELETE FROM courses_has_tags WHERE course_id = ? AND tag_id = ?",
        (course_id, tag_id),
    )

    return {"course_id": course_id, "tag_id": tag_id}


def patch_section(
    course_id,
    section: Section,
    current_user: UserOut,
    update_data_func=None,
    read_data_func=None,
):
    """Updates Section information."""
    if update_data_func is None:
        update_data_func = update_query
    if read_data_func is None:
        read_data_func = read_query

    query = """
    SELECT CASE
        WHEN c.teacher_id = ? THEN 'OK'
        ELSE 'NO'
    END AS teacher_permission,
    CASE
        WHEN chc.course_id = ? THEN 'OK'
        ELSE 'NO'
    END AS course_permission,
    CASE
        WHEN chc.section_id = ? THEN 'OK'
        ELSE 'NO'
    END AS section_permission
    FROM courses_has_sections AS chc
    LEFT JOIN courses AS c ON chc.course_id = c.id
    LIMIT 1"""

    params = (current_user.id, course_id, section.id)
    result = read_data_func(query, params)

    if result[0][0] == "NO":
        return "Only owner can edit sections."
    elif result[0][1] == "NO":
        return "Course dont have sections."
    elif result[0][2] == "NO":
        return "Section not found."

    update_data_func(
        """UPDATE sections
                    SET title=?, content=?,
                    description=CASE WHEN ? THEN ? ELSE description END,
                    ext_link=CASE WHEN ? THEN ? ELSE ext_link END
                    WHERE id=?""",
        (
            section.title,
            section.content,
            section.description is not None,
            section.description,
            section.ext_link is not None,
            section.ext_link,
            section.id,
        ),
    )

    return {"course_id": course_id, "section": section}


def add_put_home_page_to_course(
    course_id, url_address: HomePageInput, current_user: UserOut
):
    """Teachers can set and update a Course Home Page."""

    with _get_connection() as conn:
        cursor = conn.cursor()
        conn.autocommit = False

        query = """
        SELECT c.id, CASE WHEN c.teacher_id = ? THEN teacher_id ELSE 'NO' END AS teacher_id,hp.id
        FROM courses as c
        LEFT JOIN home_pages as hp on c.id = hp.course_id
        WHERE c.id = ?
        """
        params = (current_user.id, course_id)
        cursor.execute(query, params)
        course_exist = cursor.fetchone()

        if course_exist is None:
            return f"There is no course with id:{course_id}!"
        elif course_exist[1] == "NO":
            return f"You dont have permission for this course!"

        elif course_exist[2] is None:
            cursor.execute(
                "INSERT INTO home_pages (link, course_id) VALUES (?, ?)",
                (url_address.url_address, course_id),
            )
            home_page_id = cursor.lastrowid
            return {"course_id": course_id, "home_page_id": home_page_id}
        elif course_exist[2]:
            cursor.execute(
                "UPDATE home_pages SET link = ? WHERE course_id = ?",
                (url_address.url_address, course_id),
            )
        conn.commit()
        return {"course_id": course_id, "home_page_id": course_exist[2]}


def request_subscription(
    id: int, current_user: UserOut, update_data_func=None, get_data_func=None
) -> bool | str:
    """User can directly subscribe to Public courses or to request subscription to Premium courses."""
    if update_data_func is None:
        update_data_func = update_query

    if get_data_func is None:
        get_data_func = read_query

    request_to_course_exist = get_data_func(
        """SELECT 
                    EXISTS(SELECT 1 FROM courses WHERE id = ?) AS course_exists,
                    EXISTS(SELECT 1 FROM users_has_courses WHERE course_id = ? AND user_id = ?) AS request_exists,
                    EXISTS(SELECT 1 FROM courses WHERE is_public=1 AND id= ?) AS is_public""",
        (id, id, current_user.id, id),
    )

    course_exists = request_to_course_exist[0][0]
    request_exists = request_to_course_exist[0][1]
    subscription_flag_to_public_course = request_to_course_exist[0][2]

    if not course_exists:
        return "Course does not exist."
    elif request_exists:
        return "Request already exist."
    else:
        # Using DB update_query with Insert is Done intentionally since update_query returns rowcount.
        sql = """INSERT INTO users_has_courses (user_id, course_id, has_access) 
                SELECT ?, ?, 
                    CASE
                        WHEN c.is_public = 1 THEN 1
                        WHEN private_courses_count < 5 THEN 0
                        ELSE -1
                    END AS has_access
                FROM courses AS c
                LEFT JOIN (
                    SELECT COUNT(*) AS private_courses_count
                    FROM users_has_courses AS uhc
                    INNER JOIN courses AS c ON uhc.course_id = c.id
                    WHERE uhc.user_id = ? AND c.is_public = 0 AND uhc.has_access = 1
                ) AS subquery ON 1=1
                LEFT JOIN course_status AS cs ON c.id = cs.course_id
                WHERE c.id = ? AND (cs.is_active = 1 OR cs.is_active IS NULL)
                HAVING has_access <> -1"""

        params = (current_user.id, id, current_user.id, id)

        affected_rows = update_data_func(sql, params)

        if affected_rows == 0:
            return "Maximum number of premium courses subscribed."
        elif affected_rows == 1:
            return (True, subscription_flag_to_public_course == True)


def add_rate_to_course(
    course_id, rate, current_user: UserOut, get_data_func=None, insert_data_func=None
):
    """Student can rate a Course from 1 to 10."""
    if get_data_func is None:
        get_data_func = read_query
    if insert_data_func is None:
        insert_data_func = insert_query
    query = """
    SELECT
        CASE
            WHEN c.id IS NULL THEN 'Course does not exist!'
            WHEN r.user_id IS NOT NULL THEN 'You can only give 1 rate per course.'
            WHEN uhc.course_id IS NULL THEN 'User does not have access to the course.'
            WHEN uhc.has_access = 1 THEN 'OK'
            ELSE 'Permission denied'
        END AS result
    FROM
        courses AS c
        LEFT JOIN rates AS r ON c.id = r.course_id AND r.user_id = ?
        LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id AND uhc.user_id = ?
        LEFT JOIN course_status AS cs ON c.id = cs.course_id
    WHERE
        c.id = ? AND (cs.is_active = 1 OR cs.is_active IS NULL)
    """

    result = get_data_func(query, (current_user.id, current_user.id, course_id))

    if not result:
        return "Course does not exist!"

    if result[0][0] != "OK":
        return result[0][0]
    create_rate = insert_data_func(
        "INSERT INTO rates(rate, user_id,course_id) VALUES (?, ?,?)",
        (int(rate), int(current_user.id), int(course_id)),
    )

    return {"rate": rate, "user_id": current_user.id, "course_id": course_id}


def view_students_that_rated_course(course_id, get_data_func=None):
    """Provides list of students that rated specific Course."""
    if get_data_func is None:
        get_data_func = read_query

    query = """
        SELECT r.rate, u.id, u.f_name, u.l_name
        FROM users AS u
        LEFT JOIN rates AS r ON u.id = r.user_id
        WHERE r.course_id = ? 
    """
    params = (course_id,)
    result = get_data_func(query, params)

    if result:
        user_rated_courses = []
        for row in result:
            rate, user_id, first_name, last_name = row
            user_rated_course = UserRatedCourse.from_query_result(
                rate, user_id, first_name, last_name
            )
            user_rated_courses.append(user_rated_course)

        return user_rated_courses
    else:
        return None


def select_courses_by_teacher_or_student_handler(
    teacher_id: int | None = None, student_id: int | None = None, get_data_func=None
):
    """Provides a list of courses, filtered by teacher and / or student."""
    if get_data_func is None:
        get_data_func = read_query

    if teacher_id and student_id:
        query = """
        SELECT uhc.user_id, c.id, c.title, c.description, c.teacher_id
        FROM courses AS c
        LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id
        WHERE c.teacher_id = ? AND uhc.user_id = ? AND uhc.has_access = 1 
    """
        params = (teacher_id, student_id)
        result = get_data_func(query, params)

        if not result:
            return f"Teacher with id:{teacher_id} does not have a course with student with id {student_id}."

    elif teacher_id:
        query = """
        SELECT c.id, c.title, c.description, c.teacher_id
        FROM courses AS c
        WHERE c.teacher_id = ?
    """
        params = (teacher_id,)
        result = get_data_func(query, params)
        if not result:
            return f"Teacher with id:{teacher_id} doest not have courses."
    elif student_id:
        query = """
        SELECT uhc.course_id, c.title, c.description, c.teacher_id
        FROM users_has_courses AS uhc
        LEFT JOIN courses AS c ON c.id = uhc.course_id
        Where uhc.user_id = ? AND uhc.has_access =1 
    """
        params = (student_id,)
        result = get_data_func(query, params)
        if not result:
            return f"Student with id:{student_id} doest not have courses."
    course_response = []
    for row in result:
        course_id, course_title, course_description, teacher_id_from_q = row

        current_course_with_stud = CourseTeacherResponseModel.from_query_result(
            course_id=course_id,
            course_title=course_title,
            course_description=course_description,
            teacher_id=teacher_id_from_q,
        )
        course_response.append(current_course_with_stud)
    return course_response


def course_progress(id: int, current_user: UserOut, get_data_func=None):
    if get_data_func == None:
        get_data_func = read_query
    result = get_data_func(
        """
                            SELECT 
                            CASE
                                WHEN course_exists = 0 OR cs.is_active = 0 THEN 'Course does not exist'
                                WHEN viewed_courses IS NULL THEN 'Student is not subscribed'
                                WHEN sections_in_course = 0 THEN 'Course has no sections'
                                ELSE CONCAT('Course progress is ', CAST((COALESCE(viewed_courses,0) / sections_in_course * 100) AS INT), ' %')
                            END as progress_status
                            FROM (
                              SELECT
                                    (
                                        SELECT COUNT(*) 
                                        FROM users_has_courses uhc
                                        JOIN courses_has_sections chs ON uhc.course_id = chs.course_id
                                        JOIN users_has_sections uhs ON chs.section_id = uhs.sections_id AND uhc.user_id = uhs.users_id
                                        WHERE uhc.user_id = ? AND uhc.course_id = ? AND uhc.has_access = 1
                                    ) as viewed_courses,
                                    (
                                        SELECT COUNT(*) 
                                        FROM courses_has_sections 
                                        WHERE course_id = ?
                                    ) as sections_in_course,
                                    (
                                        SELECT CASE WHEN EXISTS(
                                            SELECT 1 
                                            FROM courses 
                                            WHERE id = ?
                                        ) THEN 1 ELSE 0 END
                                    ) as course_exists
                                )counts
                                LEFT JOIN course_status AS cs ON cs.course_id = ?""",
        (current_user.id, id, id, id, id),
    )
    return {"result": result[0][0]}


def update_course_active_status(
    id: int,
    course_status: CourseStatus,
    current_user: UserOut,
    update_data_func=None,
    get_data_func=None,
) -> bool | str:
    """Teachers can change the status (deactivate or activate) their own courses only when there are no students subscribed.
    A submitted but not approved request for subscription is not considered as student subscription.
    """
    if update_data_func is None:
        update_data_func = update_query

    if get_data_func is None:
        get_data_func = read_query

    if current_user.role == "teacher":
        course_update_conditions = get_data_func(
            """SELECT
                                        EXISTS(SELECT 1 FROM courses WHERE id = ?) AS course_exists,
                                        EXISTS(SELECT 1 FROM courses WHERE id = ? AND teacher_id = ?) AS is_teacher""",
            (id, id, current_user.id),
        )
        if not course_update_conditions[0][0]:
            return "Not found"
        elif not course_update_conditions[0][1]:
            return "Not teacher"

        sql = """INSERT INTO course_status (course_id, is_active)
                SELECT ?, ?
                FROM courses AS c
                WHERE c.teacher_id = ? AND c.id = ? AND NOT EXISTS (
                SELECT 1
                FROM users_has_courses AS uhc
                WHERE uhc.course_id = ? AND uhc.has_access = 1)
                ON DUPLICATE KEY UPDATE is_active = IF(
                    course_status.course_id = VALUES(course_status.course_id), VALUES(is_active), is_active)"""

        affected_rows = update_data_func(
            sql, (id, 1 if course_status == "Active" else 0, current_user.id, id, id)
        )

        return True if affected_rows else False


def get_teacher_email_by_course_id(id: int, student_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = read_query

    data = get_data_func(
        """
        SELECT u.email,  c.title, CONCAT(student.f_name, ' ', student.l_name) as teacher_name, CONCAT(u.f_name, ' ', u.l_name) as teacher_name    
        FROM courses c 
        JOIN users u ON c.teacher_id = u.id
        JOIN users_has_courses uhc ON c.id = uhc.course_id
        JOIN users student ON uhc.user_id = student.id
        WHERE c.id = ? AND uhc.user_id = ?""",
        (id, student_id),
    )

    return data[0][0], data[0][1], data[0][2], data[0][3]


def get_public_courses_for_guests(
    rating: int | None = None,
    tag: str | None = None,
    sort: str | None = None,
    get_data_func=None,
):
    """Responds with a list of all Public Courses available."""
    if get_data_func is None:
        get_data_func = read_query

    where_clauses = []

    sql = """SELECT c.id, c.title, c.description, c.objectives, c.is_public,
            CONCAT(u.f_name, ' ', u.l_name) as teacher, r.avg_rating
            FROM courses AS c 
            LEFT JOIN users_has_courses AS uhc ON c.id = uhc.course_id
            LEFT JOIN users AS u ON u.id = c.teacher_id
            LEFT JOIN courses_has_tags AS cht ON cht.course_id = c.id
            LEFT JOIN tags AS t ON cht.tag_id = t.id
            LEFT JOIN (SELECT course_id, AVG(rate) as avg_rating
                    FROM rates GROUP BY course_id) AS r ON c.id = r.course_id
            LEFT JOIN course_status AS cs ON c.id = cs.course_id
            WHERE c.is_public = 1 AND (cs.is_active = 1 OR cs.is_active IS NULL)"""

    if rating and tag:
        sql += " AND avg_rating >= ? AND t.area LIKE ?"
        where_clauses.extend(rating, f"%{tag}%")
    elif tag:
        sql += " AND t.area LIKE ?"
        where_clauses.append(tag)
    elif rating:
        sql += " AND avg_rating >= ?"
        where_clauses.append(rating)

    sql += " GROUP BY c.id"

    data = get_data_func(sql, (*where_clauses,))

    courses = list(CourseResponseModel.from_query_result(*row) for row in data)

    if sort and (sort == "asc" or sort == "desc"):
        courses = sort_courses(data, reverse=sort == "desc")

    return courses
