-- MariaDB dump 10.19  Distrib 10.11.2-MariaDB, for Win64 (AMD64)
--
-- Host: poodle.comamhmtcrkx.eu-north-1.rds.amazonaws.com    Database: poodle
-- ------------------------------------------------------
-- Server version	10.6.10-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact_info`
--

DROP TABLE IF EXISTS `contact_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_info` (
  `phone_number` int(15) NOT NULL,
  `linkedin_account` varchar(45) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`users_id`),
  UNIQUE KEY `phone_number_UNIQUE` (`phone_number`),
  UNIQUE KEY `linkedin_account_UNIQUE` (`linkedin_account`),
  KEY `fk_contact_info_users1_idx` (`users_id`),
  CONSTRAINT `fk_contact_info_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_info`
--

LOCK TABLES `contact_info` WRITE;
/*!40000 ALTER TABLE `contact_info` DISABLE KEYS */;
INSERT INTO `contact_info` VALUES
(882746621,'linkedin.com/petyodenev2413',212),
(882746627,'linkedin.com/username-334',213),
(882746629,'linkedin.com/username-123',214),
(8827466,'linkedin.com/usernadsdweame-12334',238);
/*!40000 ALTER TABLE `contact_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_status`
--

DROP TABLE IF EXISTS `course_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`is_active`,`course_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `course_id_UNIQUE` (`course_id`),
  KEY `fk_course_status_courses1_idx` (`course_id`),
  CONSTRAINT `fk_course_status_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_status`
--

LOCK TABLES `course_status` WRITE;
/*!40000 ALTER TABLE `course_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `course_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `objectives` varchar(2000) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  KEY `fk_courses_users1_idx` (`teacher_id`),
  CONSTRAINT `fk_courses_users1` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES
(92,'Python Bootcamp','Learn Python like a Professional Start from the basics and go all the way to creating your own applications and games.','Command Line',212,1),
(93,'Django & Python - Beginer','Welcome to the beginner course on How to Build a REST API from scratch, using Django, Django REST Framework, Python, Vagrant, VirtualBox, Atom, and ModHeaders. You\'ll also learn how to deploy your dev server to AWS!','How to create the most important part of any user application\r\nHow to confidently use some of the most in-demand full stack technologies today\r\nHow to create a local development server from scratch\r\nHow to create a brand new Django project with sqlite database\r\nHow to build your own browsable, self documenting REST API\r\nHandle user registration, login, and status updates in your app with your very own REST API',212,0),
(94,'Python Data Structures & Algorithms','This course makes learning to code fun and makes hard concepts easy to understand.\r\n\r\nHow did I do this?  By using animations!\r\n\r\nAnimating the Data Structures & Algorithms makes everything more visually engaging and allows students to learn more material - in less time - with higher retention (a pretty good combination).','Data Structures\r\n\r\nLists\r\n\r\nLinked Lists\r\n\r\nDoubly Linked Lists\r\n\r\nStacks & Queues\r\n\r\nBinary Trees\r\n\r\nHash Tables\r\n\r\nGraphs\r\n\r\n\r\n\r\nAlgorithms\r\n\r\nSorting\r\n\r\nBubble Sort\r\n\r\nSelection Sort\r\n\r\nInsertion Sort\r\n\r\nMerge Sort\r\n\r\nQuick Sort\r\n\r\nSearching\r\n\r\nBreadth First Search\r\n\r\nDepth First Search',213,1),
(95,'Ultimate Excel Programmer','Teach Excel to Do Your Work FOR YOU. . .\r\n\r\nMicrosoft Office is everywhere, installed on over 750 million computers, but most users only know how to set up a basic table or maybe even do a few formulas here and there.\r\n\r\nIn my course, I teach you how to take Excel by the horns and make it do whatever you want, whenever you want. It can go through loads of information and create a printable report for you. You can make custom forms so that you can access, analyze, edit, or add new information quickly to your data tables/ worksheets.','Automate and Customize data entry forms\r\nChoose the right Loop for each task\r\nMaster the CELLS and RANGE objects in multiple scenarios\r\nCreate multiple Variable styles to match your need\r\nCustomize your VBA Editor and Understand all the Toolbars and options\r\nDebug and Troubleshoot code like a boss!\r\nRecord, Modify or Write Macros from scratch\r\nMake Custom Formulas/Functions on the fly\r\nBreeze through IF THEN statements and conquer all the Logical Operators\r\nBatch out inter-active MessageBoxes, InputBoxes and give users CHOICES!\r\nGenerate Basic Reports that can be printed\r\nAdd filters to report menus to narrow the records\r\nTake control of forms, Buttons, Drop-down menus, Checkboxes and option buttons AND so much more. . .\r\nTrigger code from a number of different methods - from Clicking on a cell - to De-Selecting a worksheet.\r\nSet up Special Commands when a workbook: Opens or closes, is selected, any cell or certain cells are selected, right before printing, etc. . .\r\nManipulate Userforms for data entry, report generation, editing tables/databases - ALL within your control. Restrict the flow of data OR make the Userform(s) responsive, calculating, INTUITIVE.\r\nStreamline your work and the work of others.\r\nPut Excel ON AUTOPILOT. . .',213,0),
(96,'JavaScript From Zero to Expert','JavaScript is the most popular programming language in the world. It powers the entire modern web. It provides millions of high-paying jobs all over the world.\r\n\r\nThat\'s why you want to learn JavaScript too. And you came to the right place!\r\n\r\n\r\n\r\nWhy is this the right JavaScript course for you?\r\n\r\nThis is the most complete and in-depth JavaScript course on Poodle(and maybe the entire internet!). It\'s an all-in-one package that will take you from the very fundamentals of JavaScript, all the way to building modern and complex applications.','Build 5 beautiful real-world projects for your portfolio! In these projects, you will learn how to plan and architect your applications using flowcharts and common JavaScript patterns\r\n\r\nMaster the JavaScript fundamentals: variables, if/else, operators, boolean logic, functions, arrays, objects, loops, strings, and more\r\n\r\nLearn modern JavaScript (ES6+) from the beginning: arrow functions, destructuring, spread operator, default arguments, optional chaining, and more\r\n\r\nHow JavaScript works behind the scenes: engines, the call stack, hoisting, scoping, the \'this\' keyword, reference values, and more.\r\n\r\nDeep dive into functions: arrow functions, first-class and higher-order functions, bind, and closures.\r\n\r\nDeep dive into object-oriented programming: prototypal inheritance, constructor functions (ES5), classes (ES6), encapsulation, abstraction, inheritance, and polymorphism. [This is like a small standalone course]\r\n\r\nDeep dive into asynchronous JavaScript: the event loop, promises, async/await, and error handling. You will use these to access data from third-party APIs with AJAX calls. [This is like a small standalone course]\r\n\r\nLearn modern tools that are used by professional web developers: NPM, Parcel (module bundler), Babel, and ES6 modules\r\n\r\nCheck out the course curriculum for an even more detailed overview of the content :)',214,1),
(97,'Real-World Websites HTML and CSS','Amazing, right? What if you knew exactly how to design and build a website like that, completely from scratch? How amazing would that be?\r\n\r\nWell, I\'m here to teach you HTML, CSS, and web design, all by building the stunning website that you just saw, step-by-step.\r\n\r\nSo, after finishing this course, you will know exactly how to build a beautiful, professional, and ready-to-launch website just like Omnifood, by following a 7-step process. And it will even look great on any computer, tablet, and smartphone.','Become a modern and confident HTML and CSS developer, no prior knowledge needed!\r\nDesign and build a stunning real-world project for your portfolio from scratch\r\nModern, semantic and accessible HTML5\r\nModern CSS (previous CSS3), including flexbox and CSS Grid for layout\r\nImportant CSS concepts such as the box model, positioning schemes, inheritance, solving selector conflicts, etc.\r\nA web design framework with easy-to-use rules and guidelines to design eye-catching websites\r\nHow to plan, sketch, design, build, test, and optimize a professional website\r\nHow to make websites work on every possible mobile device (responsive design)\r\nHow to use common components and layout patterns for professional website design and development\r\nDeveloper skills such as reading documentation, debugging, and using professional tools\r\nHow to find and use free design assets such as images, fonts, and icons\r\nPractice your skills with 10+ challenges (solutions included)',214,0);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_has_sections`
--

DROP TABLE IF EXISTS `courses_has_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_has_sections` (
  `course_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`,`section_id`),
  KEY `fk_courses_has_sections_sections1_idx` (`section_id`),
  KEY `fk_courses_has_sections_courses1_idx` (`course_id`),
  CONSTRAINT `fk_courses_has_sections_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_sections_sections1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_has_sections`
--

LOCK TABLES `courses_has_sections` WRITE;
/*!40000 ALTER TABLE `courses_has_sections` DISABLE KEYS */;
INSERT INTO `courses_has_sections` VALUES
(92,166),
(92,167),
(92,168),
(92,169),
(92,170),
(92,171),
(92,172),
(93,173),
(93,174),
(93,175),
(93,176),
(93,177),
(93,178),
(93,179),
(94,180),
(94,181),
(94,182),
(94,183),
(94,184),
(94,186),
(94,187),
(94,188),
(95,189),
(95,190),
(95,191),
(95,192),
(95,193),
(95,194),
(95,195),
(96,196),
(96,197),
(96,198),
(96,199),
(96,200),
(96,201),
(96,202),
(97,203),
(97,204),
(97,205),
(97,206),
(97,207),
(97,208),
(97,209);
/*!40000 ALTER TABLE `courses_has_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_has_tags`
--

DROP TABLE IF EXISTS `courses_has_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_has_tags` (
  `course_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`,`tag_id`),
  KEY `fk_courses_has_tags_tags1_idx` (`tag_id`),
  KEY `fk_courses_has_tags_courses1_idx` (`course_id`),
  CONSTRAINT `fk_courses_has_tags_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_tags_tags1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_has_tags`
--

LOCK TABLES `courses_has_tags` WRITE;
/*!40000 ALTER TABLE `courses_has_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_pages`
--

DROP TABLE IF EXISTS `home_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(3000) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_home_pages_courses1_idx` (`course_id`),
  CONSTRAINT `fk_home_pages_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_pages`
--

LOCK TABLES `home_pages` WRITE;
/*!40000 ALTER TABLE `home_pages` DISABLE KEYS */;
INSERT INTO `home_pages` VALUES
(1,'https://d31ezp3r8jwmks.cloudfront.net/tp1h80m384u2i6gipzcecj472h6t',92);
/*!40000 ALTER TABLE `home_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rates` (
  `rate` tinyint(10) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`course_id`,`rate`),
  KEY `fk_courses_has_users_users1_idx` (`user_id`),
  KEY `fk_courses_has_users_courses1_idx` (`course_id`),
  CONSTRAINT `fk_courses_has_users_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rates`
--

LOCK TABLES `rates` WRITE;
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'Your custom description could be added here.',
  `ext_link` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'Link to external resourse could be added here.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES
(166,'Python Setup','https://youtube.com/embed/Kn1HF3oD19c','Setting up a development environment: This section typically covers the installation and configuration of integrated development environments (IDEs) or code editors commonly used with Python, such as PyCharm, Visual Studio Code, Sublime Text, or Atom.',NULL),
(167,'Python Comparison Operators','https://youtube.com/embed/mrryXQnlYN8','In Python, comparison operators are used to compare two values and determine the relationship between them.\r\nThese operators return a Boolean value, either True or False, based on the outcome of the comparison.',NULL),
(168,'Loops','https://youtube.com/embed/5AOfDuV6X30','This section covers the for loop, which iterates over sequences, and the while loop, which repeatedly executes code based on a condition. These looping statements enable automation, data processing, and dynamic control flow in Python programs.',NULL),
(169,'Statements','https://youtube.com/embed/497MClrekMY','Statements are individual instructions that make up a program. They include assignment statements, print statements, conditional statements (if, elif, else), looping statements (for, while), function definitions, and return statements. They control program flow, manipulate data, and perform actions in Python.',NULL),
(170,'Methods and Functions','https://youtube.com/embed/-Bkupx9gX0o','Python methods and functions are blocks of reusable code that perform specific tasks. Methods are functions that belong to objects, while functions are independent. They allow code modularity, accept arguments, perform actions, and can return values. They are crucial for organizing and reusing code in Python programs.',NULL),
(171,'Object Oriented Programming','https://youtube.com/embed/pnWINBJ3-yA','Object-oriented programming (OOP) is a programming paradigm that organizes code around objects. Objects are instances of classes that encapsulate data and behavior. OOP emphasizes concepts like inheritance, polymorphism, and encapsulation, enabling code reusability, modularity, and easier maintenance in large-scale applications.',NULL),
(172,'Modules and Packages','https://youtube.com/embed/TIt2EOuC-Bo','Modules in Python are individual files containing Python code that can be imported and used in other programs. Packages are directories that contain multiple modules, allowing for organized and modular code structure. They promote code reuse, modularity, and help manage large codebases efficiently in Python projects.',NULL),
(173,'Setting up Django','https://youtube.com/embed/MLeVezRSjvU','To set up Django, install it using pip, create a new project using \"django-admin startproject projectname\", configure the database, run migrations with \"python manage.py migrate\", and start the development server with \"python manage.py runserver\".',NULL),
(174,'Creating Django App','https://youtube.com/embed/fdXLIX3BfAc','To create a Django app, navigate to your project\'s root directory, then run \"python manage.py startapp appname\". This creates a new directory with the specified app name, containing necessary files. Next, add the app to the project\'s settings, define models, views, and URLs to build the app\'s functionality.',NULL),
(175,'Setup Database','https://youtube.com/embed/09ghrChRtuw','To set up the database in Django, configure the database settings in your project\'s settings.py file. Specify the database engine (e.g., SQLite, MySQL, PostgreSQL), database name, user credentials, and host. Then, run \"python manage.py makemigrations\" to create migration files, and \"python manage.py migrate\" to apply those migrations and set up the database tables.',NULL),
(176,'Setup Django Admin','https://youtube.com/embed/iLhcV7t3zug','To set up Django admin, first create a superuser with \"python manage.py createsuperuser\". Then, register models in the admin.py file of your app. Customize the admin interface by overriding templates or using decorators. Access the admin interface at \"/admin\" and log in with the superuser credentials to manage your app\'s data.',NULL),
(177,'Create Login API','https://youtube.com/embed/ARy_ozfWj-I','To create a login API in Django, define a URL endpoint, view, and serializer. Use Django\'s built-in authentication framework or implement a custom authentication logic. Validate user credentials, generate and return a token or session. Protect the API with authentication decorators or middleware for secure access.',NULL),
(178,'Create Profile Feed API','https://youtube.com/embed/g3Ko8OpPTGY?list=PLj-GICAf5y3HF-dPuidVE6Cyas5IGp_mZ','To create a profile feed API in Django, define URL endpoints for creating, retrieving, updating, and deleting feed items. Implement views, serializers, and authentication mechanisms. Connect the feed items to user profiles. Secure the API with authentication and authorization. Test the API using Django\'s testing framework.',NULL),
(179,'Deploying API on AWS server','https://youtube.com/embed/uiPSnrE6uWE','To deploy an API on AWS, create an EC2 instance, install required dependencies, configure security groups, and open necessary ports. Set up a web server (e.g., Apache or Nginx), install and configure Django or Flask. Finally, use Route 53 or Load Balancer to route traffic to the API.',NULL),
(180,'Big O','https://youtube.com/embed/A03oI0znAoc','Big O notation is used to describe the time and space complexity of an algorithm. It represents the worst-case scenario for the growth rate of an algorithm as the input size increases. It helps analyze and compare algorithms based on their efficiency and scalability, allowing for better algorithmic choices in programming.',NULL),
(181,'Linked List','https://youtube.com/embed/qp8u-frRAnU','A linked list is a data structure composed of nodes, where each node contains a value and a reference to the next node. It allows for efficient insertion and deletion operations, but accessing elements requires traversing the list. Linked lists are dynamic and can grow or shrink as needed, making them suitable for certain scenarios.',NULL),
(182,'Doubly Linked List','https://youtube.com/embed/89Gma338jHU','A doubly linked list is a type of linked list where each node contains a value, a reference to the next node, and a reference to the previous node. This bidirectional linkage allows for efficient traversal in both directions. It supports operations like insertion, deletion, and traversal in both forward and backward directions, providing more flexibility than a singly linked list.',NULL),
(183,'Stacks & Queues','https://youtube.com/embed/9gnEcGePgHw','Stacks and queues are essential data structures. A stack follows the Last-In, First-Out (LIFO) principle, with elements added and removed from the top. Queues, on the other hand, follow the First-In, First-Out (FIFO) principle, with elements added at the rear and removed from the front.',NULL),
(184,'Trees','https://youtube.com/embed/4r_XR9fUPhQ','Trees are hierarchical data structures in Python. Each tree node can have child nodes, forming a branching structure. Common types include binary trees, AVL trees, and red-black trees. Trees are useful for representing hierarchical relationships, organizing data, and solving various problems like searching and sorting.',NULL),
(186,'Graphs','https://youtube.com/embed/j0IYCyBdzfA','Graphs are a data structure consisting of a set of vertices (nodes) connected by edges. They are used to represent relationships between objects. Graphs can be directed or undirected, and they can have weighted or unweighted edges. They are widely used in various fields, including network analysis, social networks, routing algorithms, and data modeling. Graph algorithms can solve problems like path finding, connectivity analysis, and graph traversal.',NULL),
(187,'Recursion','https://youtube.com/embed/9bsK03SlmNM','Recursion is a programming concept where a function calls itself to solve a problem by breaking it down into smaller, simpler instances of the same problem. It involves defining a base case that terminates the recursion and a recursive case that calls the function again with a modified input. Recursion is useful for solving problems that exhibit a recursive structure, such as tree traversal, factorial calculation, and solving recursive mathematical formulas.',NULL),
(188,'Recursive Binary Search Tree','https://youtube.com/embed/8eDVIvTE-iE','A recursive binary search tree is a binary tree data structure that follows the binary search property and utilizes recursion for operations. In a binary search tree, each node has a key, and the keys in the left subtree are less than the node\'s key, while keys in the right subtree are greater. Recursive functions are used to perform operations like insertion, deletion, and searching by recursively traversing the tree based on the comparison of keys.',NULL),
(189,'Range Object','https://youtube.com/embed/h7-R_9ConD8','In Excel, the Range object represents a cell, a range of cells, or a collection of ranges. It allows manipulation and access to cell data, formatting, and other properties. Range objects can be used to perform various operations, such as reading and writing values, applying formulas, and formatting cells within an Excel worksheet.',NULL),
(190,'Range Properties','https://youtube.com/embed/KpqKmH3a-os','The Range object in Excel has various properties that allow access and manipulation of cell range data. Common properties include Value (to get or set the value of a cell or range), Formula (to get or set the formula of a cell or range), Row (to get the row number), Column (to get the column number), and Font (to access and modify the font properties of cells).',NULL),
(191,'Cells Object','https://youtube.com/embed/HeueGSi1Lao','The Cells object in Excel represents a collection of all the cells in a worksheet. It allows for easy navigation and manipulation of cell data. The Cells object can be used to read and write values, apply formatting, perform calculations, and perform various operations on individual cells or ranges of cells within an Excel worksheet.',NULL),
(192,'Variables','https://youtube.com/embed/DpOoE4U4iGg','In Excel, variables are used to store and manipulate data within formulas, macros, and VBA (Visual Basic for Applications) code. Unlike traditional programming languages, Excel does not require explicit variable declaration. Variables in Excel are dynamically assigned based on the data being used and can be named references to cells, ranges, or formulas. They allow for efficient data manipulation and computation within the spreadsheet environment.',NULL),
(193,'Toolbar & Menus','https://youtube.com/embed/3rXvaX2bs1Q','In Excel, the toolbar and menus offer commands for tasks like formatting, data manipulation, and more. The toolbar displays icons for frequently used commands, while menus categorize commands under headings like File, Edit, View, and Insert. They provide convenient access to Excel\'s features and functionality.',NULL),
(194,'Loops','https://youtube.com/embed/iIjhQsqIDuY','In Excel, loops can be implemented using VBA (Visual Basic for Applications) macros. Common loop structures include the For loop, Do-While loop, and Do-Until loop. These loops allow repetitive execution of code, iterating over cells, rows, or columns, and performing actions based on certain conditions. They are useful for automating tasks and processing data in Excel.',NULL),
(195,'Events','https://youtube.com/embed/VYsEEtzEonU','In Excel, events are actions or occurrences that can trigger specific actions or code execution. Examples of events in Excel include opening or closing a workbook, clicking a cell, changing the value of a cell, or saving a workbook. By using VBA (Visual Basic for Applications) macros, you can associate code with specific events to perform actions or respond to user interactions in Excel.',NULL),
(196,'JavaScript Fundamentals','https://youtube.com/embed/vEROU2XtPR8','JavaScript is a widely used programming language for web development. It runs in the browser and allows interactive elements, dynamic content, and client-side functionality. JavaScript provides features like variables, data types, operators, control flow, functions, and object-oriented programming. It is essential for creating modern web applications and enhancing user experiences.',NULL),
(197,'Data Structures','https://youtube.com/embed/poGEVboh9Rw?list=PLC3y8-rFHvwg6nsAOfC5Is18KB2DrVOJy','JavaScript provides built-in data structures such as arrays and objects. Additionally, it offers other data structures like maps, sets, and linked lists through the built-in JavaScript objects and methods. These data structures enable efficient storage, manipulation, and organization of data in JavaScript applications.',NULL),
(198,'Document Object Model','https://youtube.com/embed/y17RuWkWdn8','The Document Object Model (DOM) in JavaScript represents the structure of an HTML or XML document as a tree of objects. It allows JavaScript to interact with and manipulate the elements, attributes, and content of a web page dynamically. DOM methods and properties provide functionality for accessing, modifying, and updating the document\'s structure, styles, and behavior.',NULL),
(199,'Object-Oriented Programming','https://youtube.com/embed/vDJpGenyHaA','JavaScript supports object-oriented programming (OOP) principles. It allows the creation of objects with properties and methods using classes or constructor functions. In JavaScript, objects can be instantiated, inherited, and polymorphism can be achieved. OOP in JavaScript provides encapsulation, abstraction, inheritance, and polymorphism, enabling modular and reusable code structures.',NULL),
(200,'Arrays','https://youtube.com/embed/7W4pQQ20nJg','Arrays in JavaScript are data structures that store multiple values in a single variable. They are created using square brackets [] and can hold any type of data, including numbers, strings, objects, or even other arrays. JavaScript arrays are dynamic, meaning their size can change dynamically by adding or removing elements. Arrays have various built-in methods for manipulating and accessing elements, such as push, pop, shift, and splice. They are commonly used for storing and manipulating collections of related data in JavaScript.',NULL),
(201,'Dates And Timers','https://youtube.com/embed/zwBMp1U6FII','In JavaScript, the Date object is used to work with dates and times. It provides methods for creating, manipulating, and formatting dates and times. The Date object allows getting and setting the current date and time, as well as performing operations like adding or subtracting time intervals. JavaScript also offers timers, such as setTimeout and setInterval, for executing code at specified intervals or after a delay. These features enable time-based functionality and scheduling in JavaScript applications.',NULL),
(202,'External Libraries','https://youtube.com/embed/uoB0oAX3cvM','Popular JavaScript libraries like jQuery, React, Angular, lodash, D3.js, Axios, Moment.js, Chart.js, and Express.js provide additional functionality and utilities for web development, including DOM manipulation, user interface building, data visualization, HTTP requests, date handling, and server-side applications.',NULL),
(203,'HTML Fundamentals','https://youtube.com/embed/qz0aGYrrlhU','HTML (Hypertext Markup Language) is the standard markup language for creating web pages. It defines the structure and content of a webpage using elements and tags. HTML elements represent different parts of a webpage, such as headings, paragraphs, images, links, tables, forms, and more. It forms the backbone of web development.',NULL),
(204,'CSS Fundamentals','https://youtube.com/embed/1Qu5kydw4FA?list=PLuE4JRVl95pTI3jIYZKdZrczm6DgfBSTU','CSS (Cascading Style Sheets) is a style sheet language used to describe the visual appearance and formatting of HTML elements on a web page. It enables the separation of presentation and structure. CSS defines styles like colors, fonts, layouts, and animations, allowing designers to customize the look and feel of web pages.',NULL),
(205,'Flexbox','https://youtube.com/embed/phWxA89Dy94','Flexbox is a CSS layout module that provides a flexible and efficient way to create responsive and dynamic layouts. It allows easy positioning, alignment, and distribution of elements within a container. With flexible properties like flex-direction, justify-content, and align-items, Flexbox simplifies the creation of complex and adaptive layouts.',NULL),
(206,'Grid Fundamentals','https://youtube.com/embed/8QSqwbSztnA','CSS Grid is a powerful layout system that allows you to create complex grid-based layouts with ease. It divides a web page into rows and columns, enabling precise control over the placement and sizing of elements. With properties like grid-template-columns, grid-template-rows, and grid-gap, CSS Grid provides a versatile and responsive layout solution for modern web design.',NULL),
(207,'Web Design Rules','https://youtube.com/embed/GJN7TemsZtY','Web design rules ensure visually appealing and user-friendly websites. Key rules include simplicity, consistency, responsive design, clear navigation, and readability. They emphasize clean and intuitive layouts, consistent branding, mobile-friendly designs, easy navigation, and legible content to enhance the user experience and drive engagement on the website.',NULL),
(208,'Responsive Web Design','https://youtube.com/embed/zF6VSky4SIc','Responsive web design is an approach to building websites that ensures optimal viewing and interaction across different devices and screen sizes. It uses flexible layouts, fluid images, and media queries to dynamically adjust the content and design elements based on the user\'s device. This helps provide a seamless and consistent experience for users, regardless of the device they are using to access the website.',NULL),
(209,'Layout Patterns','https://youtube.com/embed/tv-_1er1mWI','Layout patterns in web design, such as grid, hero/header, sidebar, card, full-screen, magazine, and one-page layouts, provide structured and visually appealing arrangements for organizing website content. These patterns enhance the user experience by facilitating content presentation, navigation, and readability, resulting in more engaging and effective websites.',NULL);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_verification_access`
--

DROP TABLE IF EXISTS `user_verification_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_verification_access` (
  `is_verified` tinyint(1) NOT NULL,
  `has_access` tinyint(1) NOT NULL DEFAULT 0,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`users_id`),
  KEY `fk_user_verification_access_users1_idx` (`users_id`),
  CONSTRAINT `fk_user_verification_access_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_verification_access`
--

LOCK TABLES `user_verification_access` WRITE;
/*!40000 ALTER TABLE `user_verification_access` DISABLE KEYS */;
INSERT INTO `user_verification_access` VALUES
(1,1,212),
(1,1,213),
(1,1,214),
(1,1,218),
(1,1,238);
/*!40000 ALTER TABLE `user_verification_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinyint(2) NOT NULL DEFAULT 0,
  `email` varchar(100) NOT NULL,
  `f_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(212,1,'petyo_denev2@abv.bg','Petyo','Denev','$2b$12$.3OStBNHL77to3SYCOl22.7762M9orZ2z61auqyNRdiT5njNFsf.6'),
(213,1,'ivandimitrov@gmail.com','Ivan','Dimitrov','$2b$12$qMPnRguqZFt.pvNv0fMmXeo2aGKX/9GVc3L.64u.zGyn3BvTQvfXi'),
(214,1,'stanislavstanchev@gmail.com','Stanislav','Stanchev','$2b$12$nhigLXjUCmnP1i6BJUK98OINA2FuXU/Aa6rEzaPsabeNBIdf1e/hC'),
(218,0,'zdravko_denev72@abv.bg','Zdravko','Denev','$2b$12$8M7dSqMOFJD4bXj4bG/Lq.rplHTBmahwHidLPTFFLrJ/k1WkH6xfu'),
(238,1,'teacher_with_no_courses@abv.bg','Neli','Filipova','$2b$12$d32P1.d4WUEyyJ175WbGpeTWHV95RvCIfdV1f2uaYfsacokqUvPuu');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_has_courses`
--

DROP TABLE IF EXISTS `users_has_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_has_courses` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `has_access` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`,`course_id`,`has_access`),
  KEY `fk_users_has_courses_courses1_idx` (`course_id`),
  KEY `fk_users_has_courses_users_idx` (`user_id`),
  CONSTRAINT `fk_users_has_courses_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_courses_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_has_courses`
--

LOCK TABLES `users_has_courses` WRITE;
/*!40000 ALTER TABLE `users_has_courses` DISABLE KEYS */;
INSERT INTO `users_has_courses` VALUES
(218,92,1),
(218,93,0);
/*!40000 ALTER TABLE `users_has_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_has_sections`
--

DROP TABLE IF EXISTS `users_has_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_has_sections` (
  `users_id` int(11) NOT NULL,
  `sections_id` int(11) NOT NULL,
  `is_viewed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`users_id`,`sections_id`,`is_viewed`),
  KEY `fk_users_has_sections_sections1_idx` (`sections_id`),
  KEY `fk_users_has_sections_users1_idx` (`users_id`),
  CONSTRAINT `fk_users_has_sections_sections1` FOREIGN KEY (`sections_id`) REFERENCES `sections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_sections_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_has_sections`
--

LOCK TABLES `users_has_sections` WRITE;
/*!40000 ALTER TABLE `users_has_sections` DISABLE KEYS */;
INSERT INTO `users_has_sections` VALUES
(212,173,1),
(212,174,1),
(214,198,1),
(218,167,1),
(238,172,1);
/*!40000 ALTER TABLE `users_has_sections` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-13 18:41:25
