from api.common.responses import NotFound, Conflict, Forbidden, NoContent, Created
from api.data.models import CourseResponseModel, Section, CourseStatus
import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.routers import courses

class Courses_Should(TestCase):

    def test_createCourse_returnForbidden_when_studentTryToAccess(self):
        mock_course = td.fake_course()
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden,"Only Teachers can create courses.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.create_course(mock_course,mock_student)

        self.assertEqual(expected, result)
        mock_validation_role.assert_called_once()
    
    def test_createCourse_returnCourse_when_validArgs(self):
        mock_course = td.fake_course()
        mock_student = td.fake_user()
        expected = mock_course

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch('api.services.courses_service.create_course', return_value = mock_course) as mock_course_resposne:
                result = courses.create_course(mock_course,mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(expected.title, result.title)
        self.assertEqual(expected.description, result.description)
        self.assertEqual(expected.objectives, result.objectives)
        self.assertEqual(expected.is_public, result.is_public)
        
        mock_course_resposne.assert_called_once()
        mock_validation_role.assert_called_once()
        
    def test_createCourse_returnsConflic_when_titleAlreadyInUse(self):
        mock_course = td.fake_course()
        mock_student = td.fake_user()
        mock_response = td.fake_response(Conflict, "Title already in use!", status_code=409)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch('api.services.courses_service.create_course', return_value = mock_response) as mock_course_resposne:
                result = courses.create_course(mock_course,mock_student)

        self.assertEqual(expected, result)
        self.assertIsInstance(result,Conflict)
        self.assertEqual(409, result.status_code)
        self.assertEqual("Title already in use!", result.content)
        mock_course_resposne.assert_called_once()
        mock_validation_role.assert_called_once()

    def test_getCourse_returnsForbidden_when_notSubscribedUser(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden,"Only users with subscription / confirmed subscription have access to Premium courses.", status_code=403)
        expected = mock_response

        with patch('api.services.courses_service.get_by_id', return_value = mock_response) as mock_validation_role:
            result = courses.get_course(mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_getCourse_returnsNotFound_when_courseDoesNotExist(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(NotFound,"Course with Id No. {id} not found.", status_code=404)
        expected = mock_response

        with patch('api.services.courses_service.get_by_id', return_value = mock_response) as mock_validation_role:
            result = courses.get_course(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 404)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()

    def test_getCourse_returnsCourse_when_courseExist(self):
        mock_student = td.fake_user()
        mock_course = td.fake_course_response_model()
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2',content='Great content',
                                        description='Great description',ext_link='www.facebook.com')
   
        expected = td.fake_course_with_sections_response(course=mock_course, sections=[mock_section1,mock_section2])

        with patch('api.services.courses_service.get_by_id', return_value = expected) as mock_response:
            result = courses.get_course(mock_student)

        self.assertEqual(expected.course.id, result.course.id)
        self.assertEqual(expected.course.title, result.course.title)
        self.assertEqual(expected.course.description, result.course.description)
        self.assertEqual(expected.course.objectives, result.course.objectives)
        self.assertEqual(expected.course.teacher, result.course.teacher)
        self.assertEqual(expected.course.rating, result.course.rating)
        self.assertIsInstance(result.course, CourseResponseModel)

        for expected_section, result_section in zip(expected.sections, result.sections):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)  
            self.assertEqual(expected_section.content, result_section.content)  
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)   
            self.assertIsInstance(result_section, Section)

        mock_response.assert_called_once()

    def test_getCourse_returnsCourseWithSectionsInDesc_when_sortParamIsPassed(self):
        mock_student = td.fake_user()
        mock_course = td.fake_course_response_model()
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2',content='Great content',
                                        description='Great description',ext_link='www.facebook.com')
   
        expected = td.fake_course_with_sections_response(course=mock_course, sections=[mock_section2, mock_section1])

        with patch('api.services.courses_service.get_by_id', return_value = expected) as mock_response:
            result = courses.get_course(mock_student, sort='desc')

        self.assertEqual(expected.course.id, result.course.id)
        self.assertEqual(expected.course.title, result.course.title)
        self.assertEqual(expected.course.description, result.course.description)
        self.assertEqual(expected.course.objectives, result.course.objectives)
        self.assertEqual(expected.course.teacher, result.course.teacher)
        self.assertEqual(expected.course.rating, result.course.rating)
        self.assertIsInstance(result.course, CourseResponseModel)

        for expected_section, result_section in zip(expected.sections, result.sections):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)  
            self.assertEqual(expected_section.content, result_section.content)  
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)   
            self.assertIsInstance(result_section, Section)

        mock_response.assert_called_once()

    def test_getCourse_returnsCourseWithSectionsByTitle_when_sortByParamIsPassed(self):
        mock_student = td.fake_user()
        mock_course = td.fake_course_response_model()
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2',content='Great content',
                                        description='Great description',ext_link='www.facebook.com')
   
        expected = td.fake_course_with_sections_response(course=mock_course, sections=[mock_section2, mock_section1])

        with patch('api.services.courses_service.get_by_id', return_value = expected) as mock_response:
            result = courses.get_course(mock_student, sort_by='title')

        self.assertEqual(expected.course.id, result.course.id)
        self.assertEqual(expected.course.title, result.course.title)
        self.assertEqual(expected.course.description, result.course.description)
        self.assertEqual(expected.course.objectives, result.course.objectives)
        self.assertEqual(expected.course.teacher, result.course.teacher)
        self.assertEqual(expected.course.rating, result.course.rating)
        self.assertIsInstance(result.course, CourseResponseModel)

        for expected_section, result_section in zip(expected.sections, result.sections):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)  
            self.assertEqual(expected_section.content, result_section.content)  
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)   
            self.assertIsInstance(result_section, Section)

        mock_response.assert_called_once()

    def test_updateCourse_returnForbidden_when_studentTryToAccess(self):
        mock_course = td.fake_course()
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden,"Only Teachers can edit courses.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.update_course(mock_course,mock_student)

        self.assertEqual(expected, result)
        mock_validation_role.assert_called_once()

    def test_updateCourse_returnNotFound_when_courseDoesNotExist(self):
        mock_course = td.fake_course()
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(NotFound,"Course not found!", status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch('api.services.courses_service.update_course', return_value = mock_response):
            result = courses.update_course(mock_course,mock_user)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 404)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()

    def test_updateCourse_returnForbidden_when_teacherIsNotAuthorOfTheCourses(self):
        mock_course = td.fake_course()
        mock_user = td.fake_user(role='tacher')
        mock_response = td.fake_response(Forbidden,"You are not the teacher of this course.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch('api.services.courses_service.update_course', return_value = mock_response):
            result = courses.update_course(mock_course,mock_user)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_updateCourse_returnConflict_when_teacherIsNotAuthorOfTheCourses(self):
        mock_course = td.fake_course()
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Conflict,"Cannot update the same information.", status_code=409)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch('api.services.courses_service.update_course', return_value = mock_response):
            result = courses.update_course(mock_course,mock_user)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 409)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()

    def test_createSectionAndAssignToCourseHandler_returnForbidden_when_notTeacher(self):
        mock_section = td.fake_section()
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden,'Only Teachers can add section to course!.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.create_section_and_assign_to_course_handler(1, mock_section,mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_createSectionAndAssignToCourseHandler_returnForbidden_when_teacherIsNotTheAuthor(self):
        mock_section = td.fake_section()
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Forbidden,'Teacher with id:1 cannot add section to this course!', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch ('api.services.courses_service.create_section_and_assign_to_course', return_value = mock_response):
            result = courses.create_section_and_assign_to_course_handler(1, mock_section,mock_user)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_createSectionAndAssignToCourseHandler_returnCourseIdAndCreatedSection_when_validArgs(self):
        mock_section = td.fake_section()
        mock_user = td.fake_user(role='teacher')
        expected = {'course_id':1}, mock_section

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch ('api.services.courses_service.create_section_and_assign_to_course', return_value = ({'course_id':1}, mock_section)):
            result = courses.create_section_and_assign_to_course_handler(1, mock_section,mock_user)

        self.assertEqual(expected, result)
        mock_validation_role.assert_called_once()

    def test_unsubscribeFromCourse_returnForbidden_when_notStudent(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden, 'Only Students can unsubscribe from courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.unsubscribe_from_course(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_unsubscribeFromCourse_returnNotFound_when_courseDoesNotExist(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(NotFound, 'Such Subscription for Course with ID No.1 does not exist.', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.unsubscribe_from_course(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 404)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()

    def test_unsubscribeFromCourse_returnContent_when_validArgs(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(NoContent, content='', status_code=204)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role,\
            patch('api.services.courses_service.unsubscribe_from_course', return_value = True) as mock_result:
            result = courses.unsubscribe_from_course(1, mock_student)

        self.assertEqual(result.status_code, 204)
        self.assertIsInstance(result, NoContent)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_subscribeToCourse_returnForbidden_when_notStudent(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden, 'Only Students can subscribe to courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.unsubscribe_from_course(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_subscribeToCourse_returnForbidden_when_notStudent(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden, 'Only Students can subscribe to courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.subscribe_to_course(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_subscribeToCourse_returnCreated_when_studentsSuccessfullySubscribedToPublicCourse(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Created, 'You have successfully subscribed to the public course.', status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = None) as mock_validation_role, \
            patch('api.services.courses_service.request_subscription', return_value = (True,True)) as mock_subscription:
                result = courses.subscribe_to_course(id=1, current_user=mock_student)

        self.assertEqual(result.status_code, 201)
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_subscription.assert_called_once()

    def test_subscribeToCourse_returnCreatedAndCallsEmailFunc_when_studentsSuccessfullyRequestsSubscriptionPremiumCourse(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Created, 'Your request for subscription to the course was received.', status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
            patch('api.services.courses_service.request_subscription', return_value=(True, False)) as mock_subscription, \
            patch('api.services.courses_service.get_teacher_email_by_course_id', return_value=('teacher@gmail.com', 'Course Title', 'Student Name', 'Teacher Name')) as mock_teacher_email, \
            patch('api.common.mailjet.email_students_on_course', return_value=['student@gmail.com', 'Title', 'Ivan Dimitrov', 'Petyo Denev']) as mock_email_sent, \
            patch('api.common.mailjet.email_html_template_subscription', return_value=True):
                result = courses.subscribe_to_course(id=1, current_user=mock_student)


        self.assertEqual(result.status_code, 201)
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_subscription.assert_called_once()
        mock_teacher_email.assert_called_once()
        mock_email_sent.assert_called_once()

    def test_subscribeToCourse_returnNotFound_when_courseNotFound(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(NotFound, 'Course with ID No. {id} does not exist.', status_code=404)
        expected = mock_response

        
        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
            patch('api.services.courses_service.request_subscription', return_value='Course does not exist.') as mock_subscription:
            result = courses.subscribe_to_course(id=1, current_user=mock_student)

        self.assertEqual(result.status_code, 404)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_subscription.assert_called_once()

    def test_subscribeToCourse_returnConflict_when_courseStudentAlreadyHasAccess(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Conflict, 'You already requested / have access to Course with ID No.1.', status_code=409)
        expected = mock_response

        
        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
            patch('api.services.courses_service.request_subscription', return_value='Request already exist.') as mock_subscription:
            result = courses.subscribe_to_course(id=1, current_user=mock_student)

        self.assertEqual(result.status_code, 409)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()
        mock_subscription.assert_called_once()
        
    def test_subscribeToCourse_returnConflict_when_studentReacherdMaxNumberOfPremiumCourses(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Conflict, 'You have reached the maximum number of premium course subscriptions.', status_code=409)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
            patch('api.services.courses_service.request_subscription', return_value='Maximum') as mock_subscription:
            result = courses.subscribe_to_course(id=1, current_user=mock_student)

        self.assertEqual(result.status_code, 409)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()
        mock_subscription.assert_called_once()

    def test_updateSectionInformation_returnForbidden_when_notStudent(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Forbidden, 'Only Students can subscribe to courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.update_section_information(1, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_updateSectionInformation_returnForbidden_when_notAuthorOfTheSecion(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Forbidden, content='Only Students can subscribe to courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role, \
            patch('api.services.courses_service.patch_section', return_value = 'Only owner can edit sections.') as mock_result:
                result = courses.update_section_information(1, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateSectionInformation_returnNotFound_when_courseDoesNotExist(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(NotFound, content='Course not found.', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role, \
            patch('api.services.courses_service.patch_section', return_value = 'Course not found.') as mock_result:
                result = courses.update_section_information(1, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateSectionInformation_returnCourseIDAndSection_when_validArgs(self):
        mock_user = td.fake_user(role='teacher')
        mock_section = td.fake_section()
        expected = {"course_id": 1, "section": mock_section}

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role, \
            patch('api.services.courses_service.patch_section', return_value = expected) as mock_result:
                result = courses.update_section_information(1, mock_user)

        self.assertEqual(result, expected)
        self.assertEqual(list(result.values())[0], 1)
        self.assertEqual(list(result.values())[1], mock_section)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_addRateInformation_returnForbidden_when_notStudent(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Forbidden, 'Only Students can rate courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = courses.add_rate_information(1,10, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_addRateInformation_returnNotFound_when_courseDoesNotExist(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(exact_response=NotFound, content='Course does not exist!', status_code=404)
        expected = 404

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.courses_service.add_rate_to_course', return_value = 'Course does not exist!') as mock_result:
            result = courses.add_rate_information(1,10, mock_user)

        self.assertEqual(expected, result.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_addRateInformation_returnConflict_when_duplicateEntryOfRate(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(exact_response=Conflict, content='You can only give 1 rate per course.', status_code=409)
        expected = 409

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.courses_service.add_rate_to_course', return_value = mock_response) as mock_result:
            result = courses.add_rate_information(1,10, mock_user)

        self.assertEqual(expected, result.status_code)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_addRateInformation_returnConflict_when_userHasNoAccess(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(exact_response=Conflict, content='User does not have access to the course.', status_code=409)
        expected = 409

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.courses_service.add_rate_to_course', return_value = mock_response) as mock_result:
            result = courses.add_rate_information(1,10, mock_user)

        self.assertEqual(result.status_code, expected)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_addRateInformation_returnRateInfo_when_validArgs(self):
        mock_user = td.fake_user()
        expected = {"rate": 10, "user_id": 1, "course_id": 1}

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.courses_service.add_rate_to_course', return_value = expected) as mock_result:
            result = courses.add_rate_information(1,10, mock_user)

        self.assertEqual(expected, result)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_setHomePageToCourse_returnNotFound_when_courseDoesNotExist(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(NotFound, 'There is no course with id:1!', status_code=404)
        expected = NotFound('There is no course with id:1!')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.add_put_home_page_to_course', return_value=mock_response) as mock_result:
            result = courses.set_home_page_to_course(1, {'url_address': 'https://example.com'}, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, 'There is no course with id:1!')
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_setHomePageToCourse_returnForbidden_when_userDoesNotHavePermission(self):
        mock_user = td.fake_user(role='student')
        mock_response = td.fake_response(Forbidden, 'You dont have permission for this course!', status_code=403)
        expected = Forbidden('You dont have permission for this course!')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.add_put_home_page_to_course', return_value=mock_response) as mock_result:
            result = courses.set_home_page_to_course(1, {'url_address': 'https://example.com'}, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, 'You dont have permission for this course!')
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_setHomePageToCourse_returnHomePageInfo_when_validArgs(self):
        mock_user = td.fake_user(role='teacher')
        expected = {'url_address': 'https://example.com'}

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.add_put_home_page_to_course', return_value=expected) as mock_result:
            result = courses.set_home_page_to_course(1, {'url_address': 'https://example.com'}, mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()


    def test_setHomePageToCourse_returnNotFound_when_courseDoesNotExist(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(NotFound, 'There is no course with id:1!', status_code=404)
        expected = NotFound('There is no course with id:1!')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.add_put_home_page_to_course', return_value=mock_response) as mock_result:
            result = courses.set_home_page_to_course(1, {'url_address': 'https://example.com'}, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_setHomePageToCourse_returnForbidden_when_userDoesNotHavePermission(self):
        mock_user = td.fake_user(role='student')
        mock_response = td.fake_response(Forbidden, 'You dont have permission for this course!', status_code=403)
        expected = Forbidden('You dont have permission for this course!')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.add_put_home_page_to_course', return_value=mock_response) as mock_result:
            result = courses.set_home_page_to_course(1, {'url_address': 'https://example.com'}, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_setHomePageToCourse_returnHomePageInfo_when_validArgs(self):
        mock_user = td.fake_user(role='teacher')
        expected = {'url_address': 'https://example.com'}

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.add_put_home_page_to_course', return_value=expected) as mock_result:
            result = courses.set_home_page_to_course(1, {'url_address': 'https://example.com'}, mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_getCourseProgress_returnForbidden_when_userIsNotStudent(self):
        mock_user = td.fake_user(role='teacher')
        expected =  td.fake_response(exact_response=Forbidden,content='Only Students can see course progress.', status_code=403)

        with patch('api.services.users_service.validate_role', return_value=expected) as mock_validation_role:
            result = courses.get_course_progress(1, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, expected.content)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()


    def test_getCourseProgress_returnCourseProgress_when_validArgs(self):
        mock_user = td.fake_user(role='student')
        mock_response = [('Course progress is 50 %',)]
        expected = {'result': 'Course progress is 50 %'}

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.course_progress', return_value=expected) as mock_result:
            result = courses.get_course_progress(1, mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_viewCourseRates_returnForbidden_when_userIsNotAdmin(self):
            mock_user = td.fake_user(role='teacher')
            mock_response =  td.fake_response(exact_response=Forbidden,content='Only admins have access.', status_code=403)
            expected = mock_response

            with patch('api.services.users_service.validate_role', return_value=mock_response) as mock_validation_role:
                result = courses.view_course_rates(1, mock_user)

            self.assertEqual(result.status_code, expected.status_code)
            self.assertEqual(result.content, expected.content)
            self.assertIsInstance(result, Forbidden)
            mock_validation_role.assert_called_once()

    def test_viewCourseRates_returnNotFound_when_courseHasNoRates(self):
        mock_user = td.fake_user(role='admin')
        mock_response =  td.fake_response(exact_response=Forbidden,content='Course with id:1 does not have rates!', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.view_students_that_rated_course', return_value=[]) as mock_result:
            result = courses.view_course_rates(1, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_viewCourseRates_returnCourseRates_when_validArgs(self):
        mock_user = td.fake_user(role='admin')
        mock_response = [('John Doe', 5), ('Jane Smith', 4), ('Michael Johnson', 3)]
        expected = [('John Doe', 5), ('Jane Smith', 4), ('Michael Johnson', 3)]

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.view_students_that_rated_course', return_value=mock_response) as mock_result:
            result = courses.view_course_rates(1, mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateCourseActiveStatus_returnForbidden_when_userIsNotTeacher(self):
        mock_user = td.fake_user(role='student')
        mock_response =  td.fake_response(exact_response=Forbidden,content='Only Teachers could activate / deactivate courses.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=mock_response) as mock_validation_role:
            result = courses.update_course_active_status(1, CourseStatus.Inactive, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, expected.content)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_updateCourseActiveStatus_returnNotFound_when_courseDoesNotExist(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = "Not found"
        expected = NotFound('Course with ID No. 1 does not exist.')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.update_course_active_status', return_value=mock_response) as mock_result:
            result = courses.update_course_active_status(1, CourseStatus.Inactive, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateCourseActiveStatus_returnConflict_when_userIsNotTeacher(self):
        mock_user = td.fake_user(role='admin')
        mock_response = "Not teacher"
        expected = Conflict('Only author of the course has the option to change its status.')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.update_course_active_status', return_value=mock_response) as mock_result:
            result = courses.update_course_active_status(1, CourseStatus.Inactive, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateCourseActiveStatus_returnCreatedInactive_when_courseStatusIsInactive(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = True
        expected = Created(content='Course status successfully changed to "Inactive".')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.update_course_active_status', return_value=mock_response) as mock_result:
            result = courses.update_course_active_status(1, CourseStatus.Inactive, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateCourseActiveStatus_returnCreatedActive_when_courseStatusIsActive(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = True
        expected = Created(content='Course status successfully changed to "Active".')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.update_course_active_status', return_value=mock_response) as mock_result:
            result = courses.update_course_active_status(1, CourseStatus.Active, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_updateCourseActiveStatus_returnConflict_when_activeStudentsExist(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = False
        expected = Conflict(content='You cannot change the Course status due to active (subscribed) students.')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.update_course_active_status', return_value=mock_response) as mock_result:
            result = courses.update_course_active_status(1, CourseStatus.Inactive, mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Conflict)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_selectCoursesByTeacherOrStudent_returnForbidden_when_userIsNotAdmin(self):
        mock_user = td.fake_user(role='teacher')
        mock_response =  td.fake_response(exact_response=Forbidden,content='Only admins have access.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=mock_response) as mock_validation_role:
            result = courses.select_courses_by_teacher_or_student(teacher_id=1, student_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, expected.content)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_selectCoursesByTeacherOrStudent_returnNotFound_when_teacherIdAndStudentIdDoNotExist(self):
        mock_user = td.fake_user(role='admin')
        mock_response =  td.fake_response(exact_response=NotFound,content="Teacher with id:1 does not have course with student with id No. 2.", status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.select_courses_by_teacher_or_student_handler', return_value=mock_response) as mock_result:
            result = courses.select_courses_by_teacher_or_student(teacher_id=1, student_id=2, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, expected.content)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_selectCoursesByTeacherOrStudent_returnNotFound_when_teacherIdDoesNotExist(self):
        mock_user = td.fake_user(role='admin')
        mock_response =  td.fake_response(exact_response=NotFound,content="Teacher with id:1 does not have courses.", status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.select_courses_by_teacher_or_student_handler', return_value=mock_response) as mock_result:
            result = courses.select_courses_by_teacher_or_student(teacher_id=1, student_id=None, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, expected.content)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_selectCoursesByTeacherOrStudent_returnNotFound_when_studentIdDoesNotExist(self):
        mock_user = td.fake_user(role='admin')
        mock_response =  td.fake_response(exact_response=NotFound,content="Student with id:1 does not have courses.", status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.select_courses_by_teacher_or_student_handler', return_value=mock_response) as mock_result:
            result = courses.select_courses_by_teacher_or_student(teacher_id=None, student_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertEqual(result.content, expected.content)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()


    def test_selectCoursesByTeacherOrStudent_returnResult_when_validTeacherAndStudentIds(self):
        mock_user = td.fake_user(role='admin')
        mock_response = {"course_id": 1, "course_name": "Mathematics", "teacher_id": 1, "student_id": 1}
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.select_courses_by_teacher_or_student_handler', return_value=mock_response) as mock_result:
            result = courses.select_courses_by_teacher_or_student(teacher_id=1, student_id=1, current_user=mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_removeSectionFromCourse_returnForbidden_when_userIsNotTeacher(self):
        mock_user = td.fake_user(role='student')
        mock_response =  td.fake_response(exact_response=Forbidden,content='Only Teachers can add section to course!.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role:
            result = courses.remove_section_from_course(course_id=1, section_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_removeSectionFromCourse_returnForbidden_when_teacherCannotDeleteSection(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = False
        expected = Forbidden(f"Teacher with id:{mock_user.id} cannot delete section from this course!")

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.remove_section_from_course', return_value=mock_response) as mock_result:
            result = courses.remove_section_from_course(course_id=1, section_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_removeSectionFromCourse_returnResult_when_validArgs(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = True
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.remove_section_from_course', return_value=mock_response) as mock_result:
            result = courses.remove_section_from_course(course_id=1, section_id=1, current_user=mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_deleteTagFromCourse_returnForbidden_when_userIsNotTeacher(self):
        mock_user = td.fake_user(role='student')
        expected = Forbidden('Only Teachers can add section to course!.')

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role:
            result = courses.delete_tag_from_course(course_id=1, tag_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_deleteTagFromCourse_returnForbidden_when_permissionDenied(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = "Permission denied."
        expected = Forbidden(mock_response)

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.remove_tag_from_course', return_value=mock_response) as mock_result:
            result = courses.delete_tag_from_course(course_id=1, tag_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_deleteTagFromCourse_returnNotFound_when_courseNotFound(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = "Course not found."
        expected = NotFound(mock_response)

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.remove_tag_from_course', return_value=mock_response) as mock_result:
            result = courses.delete_tag_from_course(course_id=1, tag_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_deleteTagFromCourse_returnNotFound_when_tagNotFound(self):
        mock_user = td.fake_user(role='teacher')
        mock_response =  td.fake_response(exact_response=NotFound,content="Tag not found.", status_code=404)
        expected = mock_response
       
        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.remove_tag_from_course', return_value=mock_response) as mock_result:
            result = courses.delete_tag_from_course(course_id=1, tag_id=1, current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()

    def test_deleteTagFromCourse_returnResult_when_validArgs(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = None
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=False) as mock_validation_role, \
                patch('api.services.courses_service.remove_tag_from_course', return_value=mock_response) as mock_result:
            result = courses.delete_tag_from_course(course_id=1, tag_id=1, current_user=mock_user)

        self.assertEqual(result, expected)
        mock_validation_role.assert_called_once()
        mock_result.assert_called_once()
