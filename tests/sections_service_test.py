from api.data.models import Section
import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.services import sections_service

class SectionsService_Should(TestCase):

    def test_createSection_returnSection_when_validArgs(self):
        insert_data_func = lambda q, p: 1
        mock_section = td.fake_section()
        expected = mock_section

        result = sections_service.create_section(mock_section, insert_data_func=insert_data_func)

        self.assertEqual(expected, result)

    def test_getIdTitle_returnData_when_sectionExist(self):
        get_data_func = lambda q, p: [(1,)]
        excected =  [(1,)]

        result = sections_service.get_id_title('Title', get_data_func=get_data_func)

        self.assertEqual(excected, result)

    def test_getIdTitle_returnNone_when_noSuchID(self):
        get_data_func = lambda q, p: []

        result = sections_service.get_id_title('Title', get_data_func=get_data_func)

        self.assertIsNone(result)

    def test_getByTitle_returnSection_when_sectionExist(self):
        get_data_func = lambda q, p: [(1,'Section 1', 'Section content','Great description!', 'www.google.com/?search=section1')]
        mock_section = td.fake_section()
        expected = mock_section

        with patch('api.services.sections_service.Section.from_query_result', return_value = mock_section) as mock_section_result:
            result = sections_service.get_by_title('Section 1', get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_section_result.assert_called_once()

    def test_getByTitle_returnNone_when_sectionDoesNotExist(self):
        get_data_func = lambda q, p: []

        with patch('api.services.sections_service.Section.from_query_result', return_value = []) as mock_section_result:
            result = sections_service.get_by_title('Section 1', get_data_func=get_data_func)

        self.assertIsNone(result)
        mock_section_result.assert_not_called()

    def test_getById_returnNone_when_sectionNotFound(self):
        get_data_func = lambda q, p: []
        mock_user = td.fake_user()

        result = sections_service.get_by_id(1, current_user=mock_user, get_data_func=get_data_func)

        self.assertIsNone(result)

    def test_getById_returnPremium_when_userIsNotSubscribed(self):
        get_data_func = lambda q, p: [(None,)]
        mock_user = td.fake_user()
        expected = 'premium'

        result = sections_service.get_by_id(1, current_user=mock_user, get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_getById_returnSection_when_userHasAccess(self):
        get_data_func = lambda q, p: [('1|^Section 1|^Section content|^Great description!|^www.google.com/?search=section1')]
        mock_user = td.fake_user()
        mock_section = td.fake_section()
        expected = mock_section
        
        with patch('api.services.sections_service.Section.from_query_result', return_value = mock_section) as mock_section_result:
            result = sections_service.get_by_id(1, current_user=mock_user, get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_sortSections_returnSortedListOfSectionByIdInAsc_when_noParamsPassed(self):
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        expected = [mock_section1, mock_section2, mock_section3]

        result = sections_service.sort_sections([mock_section2, mock_section1, mock_section3])

        self.assertEqual(expected,result)
        
    def test_sortSections_returnSortedListOfSectionByIdInDesc_when_descParamIsPassed(self):
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        expected = [mock_section3, mock_section2, mock_section1]

        result = sections_service.sort_sections([mock_section2, mock_section1, mock_section3], reverse=True)

        self.assertEqual(expected,result)

    def test_sortSections_returnSortedListOfSectionByTitleAsc_when_titleParamIsPassed(self):
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        expected = [mock_section1, mock_section2, mock_section3]

        result = sections_service.sort_sections([mock_section2, mock_section1, mock_section3], attribute='title')

        self.assertEqual(expected,result)

    def test_sortSections_returnSortedListOfSectionByTitleDesc_when_titleParamIsPassed(self):
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        expected = [mock_section3, mock_section2, mock_section1]

        result = sections_service.sort_sections([mock_section2, mock_section1, mock_section3], attribute='title', reverse=True)

        self.assertEqual(expected,result)

    def test_allSections_returnListOfSectionsForTeacher_when_validArgs(self):
        mock_user = td.fake_user(role='teacher')
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        get_data_func = lambda q, p: [(1,'Section 1', 'Section content','Great description!', 'www.google.com/?search=section1'),
                                      (2, 'Section 2', 'Section content 2','Great description 2!', 'www.facebook.com'),
                                      (3, 'Section 3', 'Section content 3','Great description 3!', 'www.facebook.com/hey')]
        expected = [mock_section1, mock_section2, mock_section3]

        with patch('api.services.sections_service.Section.from_query_result', side_effect = expected) as mock_section_result:
            result = sections_service.all_sections(current_user=mock_user, get_data_func=get_data_func)

        for expected_section, result_section in zip(expected, result):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)
            self.assertEqual(expected_section.content, result_section.content)
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)
            self.assertIsInstance(result_section, Section)

        mock_section_result.assert_called()

    def test_allSections_returnListOfSectionsForStudent_when_validArgs(self):
        mock_user = td.fake_user()
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        get_data_func = lambda q, p: [(1,'Section 1', 'Section content','Great description!', 'www.google.com/?search=section1'),
                                      (2, 'Section 2', 'Section content 2','Great description 2!', 'www.facebook.com'),
                                      (3, 'Section 3', 'Section content 3','Great description 3!', 'www.facebook.com/hey')]
        expected = [mock_section1, mock_section2, mock_section3]

        with patch('api.services.sections_service.Section.from_query_result', side_effect = expected) as mock_section_result:
            result = sections_service.all_sections(current_user=mock_user, get_data_func=get_data_func)

        for expected_section, result_section in zip(expected, result):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)
            self.assertEqual(expected_section.content, result_section.content)
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)
            self.assertIsInstance(result_section, Section)

        mock_section_result.assert_called()

    def test_allSections_returnListOfSectionsForStudentInDesc_when_sortParamIsPassed(self):
        mock_user = td.fake_user()
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2', content='Section content 2', description='Great description 2!', ext_link='www.facebook.com')
        mock_section3 = td.fake_section(id=3, title='Section 3', content='Section content 3', description='Great description 3!', ext_link='www.facebook.com/hey')
        get_data_func = lambda q, p: [(1,'Section 1', 'Section content','Great description!', 'www.google.com/?search=section1'),
                                      (2, 'Section 2', 'Section content 2','Great description 2!', 'www.facebook.com'),
                                      (3, 'Section 3', 'Section content 3','Great description 3!', 'www.facebook.com/hey')]
        list_of_sections = [mock_section1, mock_section2, mock_section3]
        expected = [mock_section3, mock_section2, mock_section1]

        with patch('api.services.sections_service.Section.from_query_result', return_value = list_of_sections) as mock_section_result, \
            patch('api.services.sections_service.sort_sections', return_value = expected) as mock_sorted_sections:
                result = sections_service.all_sections(current_user=mock_user, get_data_func=get_data_func, sort='desc')

        for expected_section, result_section in zip(expected, result):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)
            self.assertEqual(expected_section.content, result_section.content)
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)
            self.assertIsInstance(result_section, Section)

        mock_section_result.assert_called()
        mock_sorted_sections.assert_called_once()

    def test_updateSection_updatesSectionAsViewedAndReturnsNone_when_validArgs(self):
        mock_user = td.fake_user()
        get_data_func = lambda q,p: 1

        result = sections_service.update_section_is_view(1, mock_user,get_data_func=get_data_func)

        self.assertIsNone(result)