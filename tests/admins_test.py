import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.routers import admins
from api.common.responses import NotFound, Forbidden, Created


class Admins_Should(TestCase):
    
    def test_getSection_returnForbidden_whenSectionIsPremium(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(Forbidden,"Only Admins could access the report for courses.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=mock_response) as mock_result:
            result = admins.get_admin_report_for_courses(current_user=mock_user)

        self.assertEqual(expected.status_code, result.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_result.assert_called_once()

    def test_updateCourseActiveStatus_returnForbidden_when_notAdmin(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden, "Only Admins could activate / deactivate courses.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = admins.update_course_active_status(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()
        
    def test_updateCourseActiveStatus_returnNotFound_when_courseNotFound(self):
        mock_user = td.fake_user(role='admin')
        mock_response = td.fake_response(NotFound, "Course with ID No. 1 does not exist.", status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.admins_service.update_course_active_status', return_value = 'Not found') as mock_update_course:
            result = admins.update_course_active_status(1, mock_user)

        self.assertEqual(expected.status_code, result.status_code)
        self.assertEqual(expected.content, result.body.decode())
        self.assertIsInstance(result, NotFound)
        mock_validation_role.assert_called_once()
        mock_update_course.assert_called_once()

    def test_updateCourseActiveStatus_changesStatusToInactiveAndSendsEmail_when_successValidArgs(self):
        mock_user = td.fake_user(role='admin')
        mock_response = td.fake_response(Created,
                        'Course status successfully changed to "Inactive". Email notifications are sent to each enrolled students.', status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.admins_service.update_course_active_status', return_value = True) as mock_update_course:
            result = admins.update_course_active_status(1, course_status='Inactive', current_user=mock_user)

        self.assertEqual(201, result.status_code)
        self.assertEqual(expected.content, result.body.decode())
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_update_course.assert_called_once()

    def test_updateCourseActiveStatus_changesStatusToActive_when_validArgs(self):
        mock_user = td.fake_user(role='admin')
        mock_response = td.fake_response(Created,
                        'Course status successfully changed to "Active".', status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.admins_service.update_course_active_status', return_value = True) as mock_update_course:
            result = admins.update_course_active_status(1, course_status='Active', current_user=mock_user)

        self.assertEqual(201, result.status_code)
        self.assertEqual(expected.content, result.body.decode())
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_update_course.assert_called_once()

    def test_updateCourseActiveStatus_changesStatusToInactive_when_successValidArgsAndHasDuplicateDbEntry(self):
        mock_user = td.fake_user(role='admin')
        mock_response = td.fake_response(Created,
                        'Course status successfully changed to "Inactive".', status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.admins_service.update_course_active_status', return_value = False) as mock_update_course:
            result = admins.update_course_active_status(1, course_status='Inactive', current_user=mock_user)

        self.assertEqual(201, result.status_code)
        self.assertEqual(expected.content, result.body.decode())
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_update_course.assert_called_once()

    def test_updateCourseActiveStatus_changesStatusToActive_when_successValidArgsAndHasDuplicateDbEntry(self):
        mock_user = td.fake_user(role='admin')
        mock_response = td.fake_response(Created,
                        'Course status successfully changed to "Active".', status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.admins_service.update_course_active_status', return_value = False) as mock_update_course:
            result = admins.update_course_active_status(1, course_status='Active', current_user=mock_user)

        self.assertEqual(201, result.status_code)
        self.assertEqual(expected.content, result.body.decode())
        self.assertIsInstance(result, Created)
        mock_validation_role.assert_called_once()
        mock_update_course.assert_called_once()
        
    def test_approveAccess_returnForbidden_when_notAdmin(self):
        mock_student = td.fake_user()
        mock_response = td.fake_response(Forbidden, "Only Admins could activate / deactivate users.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value = mock_response) as mock_validation_role:
            result = admins.update_course_active_status(1, mock_student)

        self.assertEqual(expected, result)
        self.assertEqual(result.status_code, 403)
        self.assertIsInstance(result, Forbidden)
        mock_validation_role.assert_called_once()

    def test_approveAccess_callsChangeUserAccess_when_validArgs(self):
        mock_user = td.fake_user(role='admin')
       

        with patch('api.services.users_service.validate_role', return_value = False) as mock_validation_role,\
            patch('api.services.admins_service.change_user_access') as mock_change_user_access_call:
            result = admins.approve_access(1, user_status='Active', current_user=mock_user)

        mock_validation_role.assert_called_once()
        mock_change_user_access_call.assert_called_once()