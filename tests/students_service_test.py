import unittest
import test_database as td
from unittest.mock import patch
from api.services import students_service

class StudentsService_Should(unittest.TestCase):

    def test_sortStudents_returnsSortedStudentsAsc_when_noAttributes(self):
        mock_student1 = td.fake_student_with_course()
        mock_student2 = td.fake_student_with_course(id=2, username='petyo@gmail.com', name='Petyo Denev', course='HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='stnai@gmail.com', name='Stanislav Stanchev', course='Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student1, mock_student2, mock_student3]
        result = students_service.sort_students(mock_list_of_students)
        self.assertEqual(expected,result)

    def test_sortStudents_returnsSortedStudentsDesc_when_reverseIsTrue(self):
        mock_student1 = td.fake_student_with_course()
        mock_student2 = td.fake_student_with_course(id=2, username='petyo@gmail.com', name='Petyo Denev', course='HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='stnai@gmail.com', name='Stanislav Stanchev', course='Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student3, mock_student2, mock_student1]
        result = students_service.sort_students(mock_list_of_students, reverse=True)
        self.assertEqual(expected,result)

    def test_sortStudents_SortsByUsernameAsc_when_attributeIsPassed(self):
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student1 ,mock_student2, mock_student3]
        result = students_service.sort_students(mock_list_of_students, attribute='username')
        self.assertEqual(expected,result)

    def test_sortStudents_SortsByUsernameDesc_when_attributeIsPassedAndReverseIsTrue(self):
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student3, mock_student2, mock_student1]
        result = students_service.sort_students(mock_list_of_students,attribute='username', reverse=True)
        self.assertEqual(expected,result)

    def test_sortStudents_SortsByCourseAsc_when_attributeIsPassed(self):
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student1 ,mock_student2, mock_student3]
        result = students_service.sort_students(mock_list_of_students, attribute='course')
        self.assertEqual(expected,result)

    def test_sortStudents_SortsByCourseDesc_when_attributeIsPassedAndReverseIsTrue(self):
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student3, mock_student2, mock_student1]
        result = students_service.sort_students(mock_list_of_students, attribute='course', reverse=True)
        self.assertEqual(expected,result)


    def test_sortStudents_SortsByNameAsc_when_attributeIsPassed(self):
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student1 ,mock_student2, mock_student3]
        result = students_service.sort_students(mock_list_of_students, attribute='name')
        self.assertEqual(expected,result)

    def test_sortStudents_SortsByNameDesc_when_attributeIsPassedAndReverseIsTrue(self):
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        mock_list_of_students = [mock_student2, mock_student1, mock_student3]
        expected = [mock_student3, mock_student2, mock_student1]
        result = students_service.sort_students(mock_list_of_students, attribute='name', reverse=True)
        self.assertEqual(expected,result)

    def test_teacherReportForSubscribedStudents_returnsListOfStudentsWithCourse_when_validParams(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        get_data_func = lambda q, params: [(1,'az@gmail.com', 'Ivan Dimitrov','A FastAPI'), (2,'beti@gmail.com', 'Petyo Denev','B HTML and CSS'), (3,'vedi@gmail.com','Stanislav Stanchev','C Emailjet')]
        expected = [mock_student1, mock_student2, mock_student3]

        with patch('api.services.students_service.StudentWithCourse.from_query_result', side_effect=expected) as mock_from_query_result:
            result = students_service.teacher_report_for_subscribed_students(current_user=mock_teacher, get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_from_query_result.assert_any_call(1, 'az@gmail.com', 'Ivan Dimitrov', 'A FastAPI')
        mock_from_query_result.assert_any_call(2, 'beti@gmail.com', 'Petyo Denev', 'B HTML and CSS')
        mock_from_query_result.assert_any_call(3, 'vedi@gmail.com', 'Stanislav Stanchev', 'C Emailjet')

    def test_teacherReportForSubscribedStudents_returnsListOfStudentsWithCourseDesc_when_validAndSortDesc(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        get_data_func = lambda q, params: [(1,'az@gmail.com', 'Ivan Dimitrov','A FastAPI'), (2,'beti@gmail.com', 'Petyo Denev','B HTML and CSS'), (3,'vedi@gmail.com','Stanislav Stanchev','C Emailjet')]
        expected = [mock_student3,mock_student2, mock_student1]

        with patch('api.services.students_service.StudentWithCourse.from_query_result', side_effect=expected) as mock_from_query_result:
            result = students_service.teacher_report_for_subscribed_students(current_user=mock_teacher, sort='desc', get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_from_query_result.assert_any_call(1, 'az@gmail.com', 'Ivan Dimitrov', 'A FastAPI')
        mock_from_query_result.assert_any_call(2, 'beti@gmail.com', 'Petyo Denev', 'B HTML and CSS')
        mock_from_query_result.assert_any_call(3, 'vedi@gmail.com', 'Stanislav Stanchev', 'C Emailjet')

    def test_teacherReportForSubscribedStudents_returnsListOfStudents_when_searchedThroughByName(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        get_data_func = lambda q, params: [(2,'beti@gmail.com', 'Petyo Denev','B HTML and CSS'),]
        expected = [mock_student2]

        with patch('api.services.students_service.StudentWithCourse.from_query_result', side_effect=expected) as mock_from_query_result:
            result = students_service.teacher_report_for_subscribed_students(current_user=mock_teacher, search='Pety', get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_from_query_result.assert_any_call(2, 'beti@gmail.com', 'Petyo Denev', 'B HTML and CSS')
        
    def test_teacherReportForSubscribedStudents_returnsListOfStudentSortedByName_when_validArgs(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        get_data_func = lambda q, params: [(2,'beti@gmail.com', 'Petyo Denev','B HTML and CSS'), (3,'vedi@gmail.com','Stanislav Stanchev','C Emailjet'), (1,'az@gmail.com', 'Ivan Dimitrov','A FastAPI')]
        expected = [mock_student1, mock_student2, mock_student3]

        with patch('api.services.students_service.StudentWithCourse.from_query_result', side_effect=expected) as mock_from_query_result:
            result = students_service.teacher_report_for_subscribed_students(current_user=mock_teacher, sort_by='name', get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_from_query_result.assert_any_call(1, 'az@gmail.com', 'Ivan Dimitrov', 'A FastAPI')
        mock_from_query_result.assert_any_call(2, 'beti@gmail.com', 'Petyo Denev', 'B HTML and CSS')
        mock_from_query_result.assert_any_call(3, 'vedi@gmail.com', 'Stanislav Stanchev', 'C Emailjet')
      
    def test_teacherReportForSubscribedStudents_returnsListOfStudentSortedByUsername_when_validArgs(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        get_data_func = lambda q, params: [(2,'beti@gmail.com', 'Petyo Denev','B HTML and CSS'), (3,'vedi@gmail.com','Stanislav Stanchev','C Emailjet'), (1,'az@gmail.com', 'Ivan Dimitrov','A FastAPI')]
        expected = [mock_student1, mock_student2, mock_student3]

        with patch('api.services.students_service.StudentWithCourse.from_query_result', side_effect=expected) as mock_from_query_result:
            result = students_service.teacher_report_for_subscribed_students(current_user=mock_teacher, sort_by='username', get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_from_query_result.assert_any_call(1, 'az@gmail.com', 'Ivan Dimitrov', 'A FastAPI')
        mock_from_query_result.assert_any_call(2, 'beti@gmail.com', 'Petyo Denev', 'B HTML and CSS')
        mock_from_query_result.assert_any_call(3, 'vedi@gmail.com', 'Stanislav Stanchev', 'C Emailjet')
    
    def test_teacherReportForSubscribedStudents_returnsListOfStudentSortedByCourse_when_validArgs(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_student1 = td.fake_student_with_course(id=1, username='az@gmail.com', name='Ivan Dimitrov', course='A FastAPI')
        mock_student2 = td.fake_student_with_course(id=2, username='beti@gmail.com', name='Petyo Denev', course='B HTML and CSS')
        mock_student3 = td.fake_student_with_course(id=3, username='vedi@gmail.com', name='Stanislav Stanchev', course='C Emailjet')
        get_data_func = lambda q, params: [(2,'beti@gmail.com', 'Petyo Denev','B HTML and CSS'), (3,'vedi@gmail.com','Stanislav Stanchev','C Emailjet'), (1,'az@gmail.com', 'Ivan Dimitrov','A FastAPI')]
        expected = [mock_student1, mock_student2, mock_student3]

        with patch('api.services.students_service.StudentWithCourse.from_query_result', side_effect=expected) as mock_from_query_result:
            result = students_service.teacher_report_for_subscribed_students(current_user=mock_teacher, sort_by='course', get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_from_query_result.assert_any_call(1, 'az@gmail.com', 'Ivan Dimitrov', 'A FastAPI')
        mock_from_query_result.assert_any_call(2, 'beti@gmail.com', 'Petyo Denev', 'B HTML and CSS')
        mock_from_query_result.assert_any_call(3, 'vedi@gmail.com', 'Stanislav Stanchev', 'C Emailjet')

    def test_subscriptionApproval_returnTrue_when_subscriptionApprovedByTeacher(self):
        update_data_func = lambda q, params: 1
        mock_teacher = td.fake_user(id=2,username='teacher@abv.bg', role='teacher')
        result = students_service.subscription_approval(1, 1, current_user=mock_teacher, update_data_func=update_data_func)
        self.assertTrue(result)

    def test_subscriptionApproval_returnFalse_when_TeacherWhichIsNotTheAuthor(self):
        update_data_func = lambda q, params: 0
        mock_teacher = td.fake_user(id=2,username='teacher@abv.bg', role='teacher')
        result = students_service.subscription_approval(1, 1, current_user=mock_teacher, update_data_func=update_data_func)
        self.assertFalse(result)

    def test_subscriptionApproval_returnTrue_when_subscriptionApprovedByAdmin(self):
        update_data_func = lambda q, params: 1
        mock_teacher = td.fake_user(id=2,username='admin@abv.bg', role='admin')
        result = students_service.subscription_approval(1, 1, current_user=mock_teacher, update_data_func=update_data_func)
        self.assertTrue(result)
    
    def test_removeStudentFromCourse_returnTrue_when_validArgs(self):
        update_data_func = lambda q, params: 1
        mock_teacher = td.fake_user(id=2,username='admin@abv.bg', role='admin')
        result = students_service.remove_student_from_course(1, 1, current_user=mock_teacher, update_data_func=update_data_func)
        self.assertTrue(result)

    def test_removeStudentFromCourse_returnFalse_when_userHadNoAccess(self):
        update_data_func = lambda q, params: 0
        mock_teacher = td.fake_user(id=2,username='admin@abv.bg', role='admin')
        result = students_service.remove_student_from_course(1, 1, current_user=mock_teacher, update_data_func=update_data_func)
        self.assertFalse(result)

    def test_removeStudentFromCourse_returnNone_when_notAdmin(self):
        update_data_func = lambda q, params: 0
        mock_teacher = td.fake_user(id=2,username='teacher@abv.bg', role='teacher')
        result = students_service.remove_student_from_course(1, 1, current_user=mock_teacher, update_data_func=update_data_func)
        self.assertIsNone(result)