import unittest
import test_database as td
from unittest.mock import patch
from api.services import tags_service
from api.data.models import TagWithCoursesResponseModel

class TagsService_Should(unittest.TestCase):

    def test_all_returnsListOfAllTags_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(1, 'Asssss'), (2, 'Bukiiii'), (3, 'Vediiiiii')]
        expected = [tag1, tag2, tag3]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_all_returnsListOfAllTagsSortedInDesc_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(1, 'Asssss'), (2, 'Bukiiii'), (3, 'Vediiiiii')]
        expected = [tag3,tag2, tag1]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func, sort='desc')

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_all_returnsListOfAllTagsSortedByAreaInAscByDefault_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(2, 'Bukiiii') ,(1, 'Asssss'), (3, 'Vediiiiii')]
        expected = [tag1,tag2, tag3]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func, sort_by='area')

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_all_returnsListOfAllTagsSortedByAreaInDesc_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(1, 'Asssss'), (2, 'Bukiiii'), (3, 'Vediiiiii')]
        expected = [tag3,tag2, tag1]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func, sort='desc', sort_by='area')

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_getPublicTags_returnsListOfAllTags_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(1, 'Asssss'), (2, 'Bukiiii'), (3, 'Vediiiiii')]
        expected = [tag1, tag2, tag3]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func)

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_getPublicTags_returnsListOfAllTagsSortedInDesc_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(1, 'Asssss'), (2, 'Bukiiii'), (3, 'Vediiiiii')]
        expected = [tag3,tag2, tag1]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func, sort='desc')

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_getPublicTags_returnsListOfAllTagsSortedByAreaInAscByDefault_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(2, 'Bukiiii') ,(1, 'Asssss'), (3, 'Vediiiiii')]
        expected = [tag1,tag2, tag3]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func, sort_by='area')

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_getPublicTags_returnsListOfAllTagsSortedByAreaInDesc_when_validArgs(self):
        tag1 = td.fake_tag(id=1, area='Asssss')
        tag2 = td.fake_tag(id=2, area='Bukiiii')
        tag3 = td.fake_tag(id=3, area='Vediiiiii')        
        get_data_func = lambda q, params: [(1, 'Asssss'), (2, 'Bukiiii'), (3, 'Vediiiiii')]
        expected = [tag3,tag2, tag1]

        with patch('api.services.tags_service.Tag.from_query_result', side_effect=expected) as mock_tags:
            result = tags_service.all(get_data_func=get_data_func, sort='desc', sort_by='area')

        self.assertEqual(expected, result)
        mock_tags.assert_any_call(1, 'Asssss')
        mock_tags.assert_any_call(2, 'Bukiiii')
        mock_tags.assert_any_call(3, 'Vediiiiii')

    def test_getByID_returnsNone_when_tagNotFound(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        get_data_func = lambda q, params: []
        
        result = tags_service.get_by_id(1,mock_teacher, get_data_func=get_data_func)
       
        self.assertIsNone(result)

    def test_getByID_returnsNotAssigned_when_tagExistsButNotAssignedYet(self):
        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        get_data_func = lambda q, params: [(None,)]
        expected = 'not assigned'

        result = tags_service.get_by_id(1,mock_teacher, get_data_func=get_data_func)
       
        self.assertEqual(expected, result)

    def test_getByID_returnsNotAssigned_when_tagExistsButNotAssignedYet(self):

        mock_teacher = td.fake_user(id=4, username='teacher@abv.bg', role='teacher')
        mock_tag = td.fake_tag()
        mock_course1 = td.fake_course_response_model()
        mock_course2 = td.fake_course_response_model(
            id=2, title='Abcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_tag_courses = td.fake_tag_with_courses_response_model(tag=mock_tag, courses=[mock_course1, mock_course2])
        expected = mock_tag_courses
        get_data_func = lambda q, params: [
            ('1|^Tag area|^1|^Title|^Great Course|^Learn to code|^1|^Ivan Dimitrov|^8.0000',),
            ('1|^Tag area|^2|^Abcdefg|^Another great course|^Learn FastAPI|^0|^Ivan Dimitrov|^7.0000',)]

        with patch('api.services.tags_service.CourseResponseModel.from_query_result', return_value=[mock_course1, mock_course2]) as mock_course_in_func, \
            patch('api.services.tags_service.TagWithCoursesResponseModel.from_query_result', return_value=mock_tag_courses) as mock_tag_with_course_in_func, \
            patch('api.services.tags_service.Tag.from_query_result', return_value=mock_tag) as mock_tag_in_func:

            result = tags_service.get_by_id(1, mock_teacher, get_data_func=get_data_func)

        self.assertEqual(expected.tag.id, result.tag.id)
        self.assertEqual(expected.tag.area, result.tag.area)

        for expected_course, result_course in zip(expected.courses, result.courses):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        mock_course_in_func.assert_called()        
        mock_tag_with_course_in_func.assert_called_once()
        mock_tag_in_func.assert_called_once_with(id=1,area='Tag area')
        self.assertIsInstance(result, TagWithCoursesResponseModel)

    def test_sortTags_returnsSortedTagsByIdAsc_when_noAttributes(self):
        mock_tag1 = td.fake_tag(id=1, area='Backend')
        mock_tag2 = td.fake_tag(id=3, area='Frontend')
        mock_tag3 = td.fake_tag(id=2, area='Database')
        mock_list_of_tags = [mock_tag2, mock_tag1, mock_tag3]
        expected = [mock_tag1, mock_tag3, mock_tag2]
        result = tags_service.sort_tags(mock_list_of_tags)
        self.assertEqual(expected, result)

    def test_sortTags_returnsSortedTagsByIdDesc_when_reverseIsTrue(self):
        mock_tag1 = td.fake_tag(id=1, area='Backend')
        mock_tag2 = td.fake_tag(id=3, area='Frontend')
        mock_tag3 = td.fake_tag(id=2, area='Database')
        mock_list_of_tags = [mock_tag2, mock_tag1, mock_tag3]
        expected = [mock_tag2, mock_tag3, mock_tag1]
        result = tags_service.sort_tags(mock_list_of_tags, reverse=True)
        self.assertEqual(expected, result)

    def test_sortTags_SortsByAreaAsc_when_attributeIsPassed(self):
        mock_tag1 = td.fake_tag(id=1, area='Backend')
        mock_tag2 = td.fake_tag(id=3, area='Frontend')
        mock_tag3 = td.fake_tag(id=2, area='Database')
        mock_list_of_tags = [mock_tag2, mock_tag1, mock_tag3]
        expected = [mock_tag1, mock_tag3, mock_tag2]
        result = tags_service.sort_tags(mock_list_of_tags, attribute='area')
        self.assertEqual(expected, result)

    def test_sortTags_SortsByAreaDesc_when_attributeIsPassedAndReverseIsTrue(self):
        mock_tag1 = td.fake_tag(id=1, area='Backend')
        mock_tag3 = td.fake_tag(id=3, area='Frontend')
        mock_tag2 = td.fake_tag(id=2, area='Database')
        mock_list_of_tags = [mock_tag2, mock_tag1, mock_tag3]
        expected = [mock_tag3,mock_tag2, mock_tag1]
        result = tags_service.sort_tags(mock_list_of_tags, attribute='area', reverse=True)
        self.assertEqual(expected, result)