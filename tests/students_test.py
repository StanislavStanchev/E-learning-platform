import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.routers import students
from api.common.responses import NotFound, Forbidden, NoContent, Created


class Students_Should(TestCase):

    def test_teacherReportForSubscribedStudents_returnForbidden_whenUserIsNotTeacher(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(Forbidden,"Only Teachers can create courses.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=expected) as mock_validate_role, \
                patch('api.services.students_service.teacher_report_for_subscribed_students') as mock_report_service, \
                patch('api.routers.students.paginate') as mock_paginate:
            result = students.teacher_report_for_subscribed_students(current_user=mock_user)

        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="teacher", message="Only Teachers can access the report to subscribed students."
        )
        mock_report_service.assert_not_called()
        mock_paginate.assert_not_called()

        self.assertEqual(result.status_code, expected.status_code)

    def test_approveStudentSubscriptionToCourse_subscriptionApproved_whenUserIsTeacher(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Created,"Subscription approved. Student No. 1 granted access to course 2", status_code=201)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=None) as mock_validate_role, \
                patch('api.services.students_service.subscription_approval', return_value=True) as mock_subscription_approval:
            result = students.approve_student_subscription_to_course(student_id=1,course_id=2,current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Created)
        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="teacher, admin", message="Only Teachers / Admins can approve course subscriptions."
        )
        mock_subscription_approval.assert_called_once_with(1,2, mock_user)

    def test_approveStudentSubscriptionToCourse_subscriptionNotApproved_whenUserIsAdmin(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(NotFound,"Not found request by Student No. 1 to Course No. 2.", status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=None) as mock_validate_role, \
                patch('api.services.students_service.subscription_approval', return_value=False) as mock_subscription_approval:
            result = students.approve_student_subscription_to_course(student_id=1,course_id=2,current_user=mock_user)


        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="teacher, admin", message="Only Teachers / Admins can approve course subscriptions."
        )
        mock_subscription_approval.assert_called_once_with(1,2, mock_user)

    def test_approveStudentSubscriptionToCourse_forbidden_whenUserIsNotTeacherOrAdmin(self):
        mock_user = td.fake_user(role='teacher')
        mock_response = td.fake_response(Forbidden,"Only Teachers / Admins can approve course subscriptions.", status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=mock_response) as mock_validate_role:
            result = students.approve_student_subscription_to_course(student_id=1,course_id=2,current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="teacher, admin", message="Only Teachers / Admins can approve course subscriptions.")

    def test_removeStudentFromCourse_studentRemoved_whenUserIsAdmin(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(NoContent, content='', status_code=204)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=None) as mock_validate_role, \
                patch('api.services.students_service.remove_student_from_course', return_value=True) as mock_remove_student:
            result = students.remove_student_from_course(student_id=1,course_id=2,current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NoContent)
        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="admin", message="Only Admins can remove Students from Course.")
        mock_remove_student.assert_called_once_with(1, 2, mock_user)

    def test_removeStudentFromCourse_notFound_whenUserIsAdmin(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(NotFound, content='Not found Course No. 1 with Student No. 2.', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=None) as mock_validate_role, \
                patch('api.services.students_service.remove_student_from_course', return_value=False) as mock_remove_student:
            result = students.remove_student_from_course(student_id=1,course_id=2,current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, NotFound)
        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="admin", message="Only Admins can remove Students from Course.")
        mock_remove_student.assert_called_once_with(1, 2, mock_user)

    def test_removeStudentFromCourse_forbidden_whenUserIsNotAdmin(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(Forbidden, content='Only Admins can remove Students from Course.', status_code=403)
        expected = mock_response

        with patch('api.services.users_service.validate_role', return_value=mock_response) as mock_validate_role:
            result = students.remove_student_from_course(student_id=1,course_id=2,current_user=mock_user)

        self.assertEqual(result.status_code, expected.status_code)
        self.assertIsInstance(result, Forbidden)
        mock_validate_role.assert_called_once_with(
            mock_user, requested_role="admin", message="Only Admins can remove Students from Course.")