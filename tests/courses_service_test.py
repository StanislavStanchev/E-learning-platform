import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.services import courses_service
from api.data.models import CourseResponseModel, CourseWithSectionsResponseModel, Section

class CoursesService_Should(TestCase):
    
    def test_sortCourses_returnsSortedCategoriesByTitle_when_validArgs(self):
        mock_course1 = td.fake_course_response_model(title='Assssdasd')
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        expected = [mock_course1, mock_course2, mock_course3]

        result = courses_service.sort_courses([mock_course2, mock_course1, mock_course3])
    
        self.assertEqual(expected,result)
   
    def test_sortCourses_returnsSortedCategoriesByTitleInDesc_when_validArgsAndReverseIsTrue(self):
        mock_course1 = td.fake_course_response_model(title='Assssdasd')
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        expected = [mock_course3, mock_course2, mock_course1]

        result = courses_service.sort_courses([mock_course2, mock_course1, mock_course3], reverse=True)
    
        self.assertEqual(expected,result)

    def test_all_returnsListOfAllCourses_when_validArgs(self):
        mock_user = td.fake_user()
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=6.00)

        get_data_func = lambda q, params: [
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)]
        expected = [mock_course1, mock_course2, mock_course3]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', side_effect=expected) as mock_read_query:
            result = courses_service.all(current_user=mock_user, get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)
        mock_read_query.assert_any_call(3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)


    def test_all_returnsListOfCoursesFilteredBySearch_when_searchParamProvided(self):
        mock_user = td.fake_user()
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)

        get_data_func = lambda q, params: [(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),]
        expected = [mock_course1]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', side_effect=expected) as mock_read_query:
            result = courses_service.all(current_user=mock_user, search='Assssdasd', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
     
    def test_all_returnsListOfCoursesFilteredByTag_when_tagParamProvided(self):
        mock_user = td.fake_user()
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)

        get_data_func = lambda q, params: [
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)]
        expected = [mock_course1, mock_course2]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', side_effect=expected) as mock_read_query:
                result = courses_service.all(current_user=mock_user, tag='Tag 2', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)

    def test_all_returnsListOfCoursesSortedInAsc_when_sortParamAscProvided(self):
        mock_user = td.fake_user()
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=6.00)

        get_data_func = lambda q, params: [
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),
            (3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)]
        expected = [mock_course1, mock_course2, mock_course3]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=expected) as mock_read_query, \
            patch('api.services.courses_service.sort_courses', return_value = expected) as mock_sorting:
                result = courses_service.all(current_user=mock_user, sort='asc', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)
        mock_read_query.assert_any_call(3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)
        mock_sorting.assert_called_once()

    def test_all_returnsListOfCoursesSortedInDesc_when_sortParamDescProvided(self):
        mock_user = td.fake_user()
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=6.00)

        get_data_func = lambda q, params: [
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),
            (3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)]
        expected = [mock_course3, mock_course2, mock_course1]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=expected) as mock_read_query, \
            patch('api.services.courses_service.sort_courses', return_value = expected) as mock_sorting:
                result = courses_service.all(current_user=mock_user, sort='desc', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)
        mock_read_query.assert_any_call(3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)
        mock_sorting.assert_called_once()

    def test_getByTitle_returnsCourse_when_TitleExists(self):
        mock_course = td.fake_course_response_model()
        get_data_func = lambda q, params: [(1,'Title','Great Course','Learn to code',1,'Ivan Dimitrov', 8.00),]
        expected = mock_course

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=expected) as mock_from_query_result:
            result = courses_service.get_by_title(title='Title', get_data_func=get_data_func)
        
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.title, result.title)
        self.assertEqual(expected.description, result.description)
        self.assertEqual(expected.objectives, result.objectives)
        self.assertEqual(expected.teacher, result.teacher)
        self.assertEqual(expected.rating, result.rating)
        mock_from_query_result.assert_called_once()

    def test_getByTitle_returnsNone_when_TitleDoesNotExist(self):
        get_data_func = lambda q, params: []
        
        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=None) as mock_from_query_result:
            result = courses_service.get_by_title(title='Title', get_data_func=get_data_func)

        self.assertIsNone(result)
        mock_from_query_result.assert_not_called()

    def test_getById_returnsCourseById_when_validId(self):
        mock_user = td.fake_user()
        mock_course = td.fake_course_response_model()
        mock_section1 = td.fake_section()
        mock_section2 = td.fake_section(id=2, title='Section 2',content='Great content',
                                        description='Great description',ext_link='www.facebook.com')

        get_data_func = lambda q, params: [('1|^Title|^Great Course|^Lern to code|^1|^Ivan Dimitrov|^8.0000|^~^||^1|^Section 1|^Section content|^Great description!|^www.google.com/?search=section1',),
                                             ('1|^Title|^Great Course|^Lern to code|^1|^Ivan Dimitrov|^8.0000|^~^||^2|^Section 2|^Great content|^Great description|^www.facebook.com',)]
   
        expected: CourseWithSectionsResponseModel = td.fake_course_with_sections_response(course=mock_course, sections=[mock_section1,mock_section2])

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=expected) as mock_course_result,\
            patch('api.services.courses_service.Section.from_query_result', return_value=[mock_section1, mock_section2]) as mock_sections_result, \
            patch('api.services.courses_service.CourseWithSectionsResponseModel.from_query_result', return_value=expected) as mock_course_with_sections_result: 
                result = courses_service.get_by_id(id=1, current_user=mock_user, get_data_func=get_data_func)

        self.assertEqual(expected.course.id, result.course.id)
        self.assertEqual(expected.course.title, result.course.title)
        self.assertEqual(expected.course.description, result.course.description)
        self.assertEqual(expected.course.objectives, result.course.objectives)
        self.assertEqual(expected.course.teacher, result.course.teacher)
        self.assertEqual(expected.course.rating, result.course.rating)
        self.assertIsInstance(result.course, CourseResponseModel)

        for expected_section, result_section in zip(expected.sections, result.sections):
            self.assertEqual(expected_section.id, result_section.id)
            self.assertEqual(expected_section.title, result_section.title)  
            self.assertEqual(expected_section.content, result_section.content)  
            self.assertEqual(expected_section.description, result_section.description)
            self.assertEqual(expected_section.ext_link, result_section.ext_link)   
            self.assertIsInstance(result_section, Section)

        mock_course_result.assert_called_once()
        mock_sections_result.assert_any_call('1', 'Section 1', 'Section content', 'Great description!', 'www.google.com/?search=section1')
        mock_sections_result.assert_any_call('2', 'Section 2', 'Great content', 'Great description', 'www.facebook.com')
        mock_course_with_sections_result.assert_called_once()
   
    def test_updateCourse_returnsNotFound_when_courseDoesNotExists(self):
        read_data_fun = lambda q, p: []
        mock_user = td.fake_user()
        mock_edit_course = td.fake_course_edit_model()
        expected = 'Course not found.'

        result = courses_service.update_course(updated_course_data=mock_edit_course, course_id=1, current_user=mock_user, read_data_fun=read_data_fun)

        self.assertEqual(expected, result)

    def test_updateCourse_returnsNotTeacher_when_notAuthorTeacher(self):
        read_data_fun = lambda q, p: [(2,)]
        mock_user = td.fake_user()
        mock_edit_course = td.fake_course_edit_model()
        expected = 'You are not the teacher of this course.'

        result = courses_service.update_course(updated_course_data=mock_edit_course, course_id=1, current_user=mock_user, read_data_fun=read_data_fun)

        self.assertEqual(expected, result)

    def test_updateCourse_returnsNone_when_notAuthorTeacher(self):
        read_data_fun = lambda q, p: [(1,)]
        update_data_func = lambda q, p: 0
        mock_user = td.fake_user()
        mock_edit_course = td.fake_course_edit_model()

        result = courses_service.update_course(updated_course_data=mock_edit_course,
                                                course_id=1, current_user=mock_user,
                                                read_data_fun=read_data_fun,
                                                update_data_func=update_data_func)

        self.assertIsNone(result)
        
    def test_updateCourse_returnsOK_when_validArgs(self):
        read_data_fun = lambda q, p: [(1,)]
        update_data_func = lambda q, p: 1
        mock_user = td.fake_user()
        mock_edit_course = td.fake_course_edit_model()
        expected =  {"updated":"OK"}
        result = courses_service.update_course(updated_course_data=mock_edit_course,
                                                course_id=1, current_user=mock_user,
                                                read_data_fun=read_data_fun,
                                                update_data_func=update_data_func)

        self.assertEqual(expected, result)
         
    @patch('api.services.courses_service._get_connection')
    def test_createSectionAndAssignToCourseSuccess_returnsNone_when_courseDoesNotExist(self, mock_get_connection):
        mock_section = td.fake_section()
        mock_user = td.fake_user()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 0
        mock_cursor.execute.return_value = 0

        result = courses_service.create_section_and_assign_to_course(1, mock_section, mock_user)

        self.assertIsNone(result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    @patch('api.services.courses_service._get_connection')
    def test_createSectionAndAssignToCourseSuccess_success_when_validArgs(self, mock_get_connection):
        mock_section = td.fake_section()
        mock_user = td.fake_user()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1
        expected = {"course_id": 1}, mock_section

        result = courses_service.create_section_and_assign_to_course(1, mock_section, mock_user)

        self.assertEqual(expected, result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    def test_unsubscribeFromCourse_returnsTrue_when_unsubscribedSuccessfully(self):
        update_data_func = lambda q, p: 1
        mock_user = td.fake_user()

        result = courses_service.unsubscribe_from_course(1, mock_user,update_data_func=update_data_func)

        self.assertTrue(result)

    def test_unsubscribeFromCourse_returnsFalse_when_userDoesNotExistOrDoesNotHasAccess(self):
        update_data_func = lambda q, p: 0
        mock_user = td.fake_user()

        result = courses_service.unsubscribe_from_course(1, mock_user,update_data_func=update_data_func)

        self.assertFalse(result)

    @patch('api.services.courses_service._get_connection')
    def test_createTagAndAssignToCourse_returnsNone_when_courseDoesNotExist(self, mock_get_connection):
        mock_tag = td.fake_tag()
        mock_user = td.fake_user()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 0
        mock_cursor.execute.return_value = 0

        result = courses_service.create_tag_and_assign_to_course(1, mock_tag, mock_user)

        self.assertIsNone(result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    @patch('api.services.courses_service._get_connection')
    def test_createTagAndAssignToCourse_success_when_validArgs(self, mock_get_connection):
        mock_tag = td.fake_tag()
        mock_user = td.fake_user()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1
        expected = {"course_id": 1}, mock_tag

        result = courses_service.create_tag_and_assign_to_course(1, mock_tag, mock_user)

        self.assertEqual(expected, result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    def test_removeSectionFromCourse_flag1Executed_when_sectionExistInCourse(self):
        mock_user = td.fake_user()
        update_data_func = lambda q, p: 1
        
        result = courses_service.remove_section_from_course(1,1,mock_user, update_data_func=update_data_func)

        self.assertEqual(1, result)

    def test_removeSectionFromCourse_flag0False_when_sectionDoesNotExistInCourse(self):
        mock_user = td.fake_user()
        update_data_func = lambda q, p: 0
        
        result = courses_service.remove_section_from_course(1,1,mock_user, update_data_func=update_data_func)

        self.assertEqual(0, result)

    def test_removeTagFromCourse_returnsPermissionDenied_when_noAccess(self):
        mock_user = td.fake_user()
        read_data_func = lambda q, p: [('NO',)]
        expected = 'Permission denied.'

        result = courses_service.remove_tag_from_course(1,1,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_removeTagFromCourse_returnsCourseNotFound_when_courseNotFound(self):
        mock_user = td.fake_user()
        read_data_func = lambda q, p: [(1,'NO')]
        expected = 'Course not found.'

        result = courses_service.remove_tag_from_course(1,1,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_removeTagFromCourse_returnsTagNotFound_when_tagNotFound(self):
        mock_user = td.fake_user()
        read_data_func = lambda q, p: [(1,1,'NO')]
        expected = 'Tag not found'

        result = courses_service.remove_tag_from_course(1,1,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_removeTagFromCourse_returnsRemovedTagFromCourse_when_success(self):
        mock_user = td.fake_user()
        read_data_func = lambda q, p: [(1,1,1)]
        update_data_func = lambda q, p: 1
        expected = {"course_id": 1, "tag_id": 1}

        result = courses_service.remove_tag_from_course(1,1,mock_user,read_data_func=read_data_func, update_data_func=update_data_func)

        self.assertEqual(expected, result)

    def test_patchSection_returnDeniedPermission_when_userIsDifferentThanAuthorOftheCOurse(self):
        mock_user = td.fake_user()
        mock_section = td.fake_section()
        read_data_func = lambda q, p: [('NO',)]
        expected = "Only owner can edit sections."

        result = courses_service.patch_section(1,mock_section,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_patchSection_deniesPermission_when_userIsDifferentThanAuthorOftheCOurse(self):
        mock_user = td.fake_user()
        mock_section = td.fake_section()
        read_data_func = lambda q, p: [('NO',)]
        expected = "Only owner can edit sections."

        result = courses_service.patch_section(1,mock_section,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_patchSection_returnsNoCourseSections_when_courseDoesNotHaveAssignedSectionsYet(self):
        mock_user = td.fake_user()
        mock_section = td.fake_section()
        read_data_func = lambda q, p: [(1,'NO')]
        expected = "Course dont have sections."

        result = courses_service.patch_section(1,mock_section,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_patchSection_returnsSectionNotFound_when_noSuchSection(self):
        mock_user = td.fake_user()
        mock_section = td.fake_section()
        read_data_func = lambda q, p: [(1,1,'NO')]
        expected = "Section not found."

        result = courses_service.patch_section(1,mock_section,mock_user,read_data_func=read_data_func)

        self.assertEqual(expected, result)

    def test_patchSection_returnsCourseIdAndSectionID_when_successfullUpdate(self):
        mock_user = td.fake_user()
        mock_section = td.fake_section()
        read_data_func = lambda q, p: [(1,1,1)]
        update_data_func = lambda q, p: 1
        expected = {"course_id":1,"section":mock_section}

        result = courses_service.patch_section(1,mock_section,mock_user,read_data_func=read_data_func, update_data_func=update_data_func)

        self.assertEqual(expected, result)

    @patch('api.services.courses_service._get_connection')
    def test_addPutHomePageToCourse_returnsNoExistingCourse_when_courseNotFound(self, mock_get_connection):
        mock_user = td.fake_user()
        mock_home_page = td.fake_home_page()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = None
        mock_cursor.fetchone.return_value = None
        expected = "There is no course with id:1!"

        result = courses_service.add_put_home_page_to_course(1,mock_home_page,mock_user)

        self.assertEqual(expected, result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()
    
    @patch('api.services.courses_service._get_connection')
    def test_addPutHomePageToCourse_returnsNoPermission_when_courseNotFound(self, mock_get_connection):
        mock_user = td.fake_user()
        mock_home_page = td.fake_home_page()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1
        mock_cursor.fetchone.return_value = [1, 'NO']
        expected = 'You dont have permission for this course!'

        result = courses_service.add_put_home_page_to_course(1,mock_home_page,mock_user)

        self.assertEqual(expected, result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    @patch('api.services.courses_service._get_connection')
    def test_addPutHomePageToCourse_insertsHomePage_when_validArgsAndNoHomePageExistedPreviously(self, mock_get_connection):
        mock_user = td.fake_user()
        mock_home_page = td.fake_home_page()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1
        mock_cursor.fetchone.return_value = [1, 1, None]
        expected = {"course_id": 1, "home_page_id": 1}

        result = courses_service.add_put_home_page_to_course(1,mock_home_page,mock_user)

        self.assertEqual(expected, result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    @patch('api.services.courses_service._get_connection')
    def test_addPutHomePageToCourse_updatesHomePage_when_validArgsAndNoHomePageExisted(self, mock_get_connection):
        mock_user = td.fake_user()
        mock_home_page = td.fake_home_page()
        mock_conn = mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1
        mock_cursor.fetchone.return_value = [1, 1, 1]
        expected = {"course_id": 1, "home_page_id": 1}

        result = courses_service.add_put_home_page_to_course(1,mock_home_page,mock_user)

        self.assertEqual(expected, result)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()

    def test_requestSubscription_returnsCourseDoesNotExists_when_courseNotFound(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [(None, 1, None)]
        expected = "Course does not exist."
        
        result = courses_service.request_subscription(1,mock_user,get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_requestSubscription_returnsRequestExist_when_alreadyExist(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [(1, 1, None)]
        expected = "Request already exist."
        
        result = courses_service.request_subscription(1,mock_user,get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_requestSubscription_returnsMaxNumberOfSubscriptions_when_userReachedFivePremiumSubscriptions(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [(1, 0, 0)]
        update_data_func = lambda q, p: 0
        expected = "Maximum number of premium courses subscribed."
        
        result = courses_service.request_subscription(1,mock_user,get_data_func=get_data_func, update_data_func=update_data_func)

        self.assertEqual(expected, result)

    def test_requestSubscription_returnsTrueWithFlagTrue_when_Success_requestAccepted(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [(1, 0, 1)]
        update_data_func = lambda q, p: 1
        expected = (True,True)
        
        result = courses_service.request_subscription(1,mock_user,get_data_func=get_data_func, update_data_func=update_data_func)

        self.assertEqual(expected, result)

    def test_requestSubscription_returnsTrueWithFlagFalse_when_Success_SubscriptionAccepted(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [(1, 0, 0)]
        update_data_func = lambda q, p: 1
        expected = (True,False)
        
        result = courses_service.request_subscription(1,mock_user,get_data_func=get_data_func, update_data_func=update_data_func)

        self.assertEqual(expected, result)

    def test_addRateToCourse_returnCourseDoesNotExist_when_courseNotFound(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: []
        expected = ("Course does not exist!")

        result = courses_service.add_rate_to_course(1, 6, mock_user, get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_addRateToCourse_returnConflictOneRatePerCouse_when_courseNotFound(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [('You can only give 1 rate per course.',)]
        expected = 'You can only give 1 rate per course.'

        result = courses_service.add_rate_to_course(1, 6, mock_user, get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_addRateToCourse_returnUserHasNoAccess_when_courseNotFound(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [('User does not have access to the course.',)]
        expected = 'User does not have access to the course.'

        result = courses_service.add_rate_to_course(1, 6, mock_user, get_data_func=get_data_func)

        self.assertEqual(expected, result)

    def test_addRateToCourse_returnRateUserAndCourse_when_successValidArgs(self):
        mock_user = td.fake_user()
        get_data_func = lambda q, p: [('OK',)]
        insert_data_func = lambda q, p: 1
        expected = {"rate":6,"user_id":mock_user.id,"course_id":1}

        result = courses_service.add_rate_to_course(1, 6, mock_user, get_data_func=get_data_func, insert_data_func=insert_data_func)

        self.assertEqual(expected, result)

    def test_getPublicCoursesForQuests_returnsListOfAllCourses_when_validArgs(self):
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=6.00)

        get_data_func = lambda q, params: [
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)]
        expected = [mock_course1, mock_course2, mock_course3]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', side_effect=expected) as mock_read_query:
            result = courses_service.get_public_courses_for_guests(get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)
        mock_read_query.assert_any_call(3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)


    def test_getPublicCoursesForQuests_returnsListOfCoursesFilteredByRating_when_ratingParamProvided(self):
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)

        get_data_func = lambda q, params: [(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),]
        expected = [mock_course1]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', side_effect=expected) as mock_read_query:
            result = courses_service.get_public_courses_for_guests(rating=8, get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
     
    def test_getPublicCoursesForQuests_returnsListOfCoursesFilteredByTag_when_tagParamProvided(self):
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        get_data_func = lambda q, params: [
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)]
        expected = [mock_course1, mock_course2]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', side_effect=expected) as mock_read_query:
                result = courses_service.get_public_courses_for_guests(tag='Tag 2', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)

    def test_getPublicCoursesForQuests_returnsListOfCoursesSortedInAsc_when_sortParamAscProvided(self):
        mock_user = td.fake_user()
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=6.00)

        get_data_func = lambda q, params: [
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),
            (3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)]
        expected = [mock_course1, mock_course2, mock_course3]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=expected) as mock_read_query, \
            patch('api.services.courses_service.sort_courses', return_value = expected) as mock_sorting:
                result = courses_service.get_public_courses_for_guests(sort='asc', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)
        mock_read_query.assert_any_call(3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)
        mock_sorting.assert_called_once()

    def test_getPublicCoursesForQuests_returnsListOfCoursesSortedInDesc_when_sortParamDescProvided(self):
        mock_course1 = td.fake_course_response_model(
            id=1, title='Assssdasd', description='Great Course',
            objectives='Learn to code', is_public = 'Public',
            teacher='Ivan Dimitrov', rating= 8.00)
        mock_course2 = td.fake_course_response_model(
            id=2, title='BBbsbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=7.00)
        mock_course3 = td.fake_course_response_model(
            id=3, title='Ccsadbcdefg', description='Another great course',
            objectives='Learn FastAPI', is_public='Premium',
            teacher='Ivan Dimitrov', rating=6.00)

        get_data_func = lambda q, params: [
            (2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00),
            (1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00),
            (3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)]
        expected = [mock_course3, mock_course2, mock_course1]

        with patch('api.services.courses_service.CourseResponseModel.from_query_result', return_value=expected) as mock_read_query, \
            patch('api.services.courses_service.sort_courses', return_value = expected) as mock_sorting:
                result = courses_service.get_public_courses_for_guests(sort='desc', get_data_func=get_data_func)

        for expected_course, result_course in zip(expected, result):
            self.assertEqual(expected_course.id, result_course.id)
            self.assertEqual(expected_course.title, result_course.title)
            self.assertEqual(expected_course.description, result_course.description)
            self.assertEqual(expected_course.objectives, result_course.objectives)
            self.assertEqual(expected_course.teacher, result_course.teacher)
            self.assertEqual(expected_course.rating, result_course.rating)

        for row in result:
            self.assertIsInstance(row, CourseResponseModel)

        mock_read_query.assert_any_call(1, 'Assssdasd', 'Great Course', 'Learn to code', 1, 'Ivan Dimitrov', 8.00)
        mock_read_query.assert_any_call(2, 'BBbsbcdefg', 'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 7.00)
        mock_read_query.assert_any_call(3, 'Ccsadbcdefg',  'Another great course', 'Learn FastAPI', 0, 'Ivan Dimitrov', 6.00)
        mock_sorting.assert_called_once()

    def test_viewStudentsThatRatedCourse_returnNone_when_noStudentsRatedACourse(self):
        get_data_func = lambda q, params: []
            
        result = courses_service.view_students_that_rated_course(1,get_data_func=get_data_func)

        self.assertIsNone(result)

    def test_viewStudentsThatRatedCourse_returnStudentsRatedCourse_when_validArgs(self):
        mock_student1 = td.fake_user_rated_course()
        mock_student2 = td.fake_user_rated_course(rate=7, id=2, first_name='Petyo', last_name='Denev')
        
        get_data_func = lambda q, params: [(5,1,'Ivan', 'Dimitrov'), (7, 2, 'Petyo', 'Denev')]
    
        expected = [mock_student1, mock_student2] 

        result = courses_service.view_students_that_rated_course(1,get_data_func=get_data_func)

        for expected_student, result_student in zip(expected, result):
            self.assertEqual(expected_student.rate, result_student.rate)
            self.assertEqual(expected_student.id, result_student.id)
            self.assertEqual(expected_student.first_name, result_student.first_name)
            self.assertEqual(expected_student.last_name, result_student.last_name)

    def test_updateCourseActiveStatus_returnsNotFound_when_CourseDoesNotExist(self):
        mock_user = td.fake_user(username='teacher@gmail.com', role='teacher')
        get_data_func = lambda q, params: [(None,)]
        result = courses_service.update_course_active_status(1, course_status=td.fake_course_status(), current_user=mock_user, get_data_func=get_data_func)
        expected = 'Not found'
        self.assertEqual(expected, result)

    def test_updateCourseActiveStatus_returnsNotTeacher_when_LoggedTeacherIsNotTheAuthor(self):
        mock_user = td.fake_user(username='teacher@gmail.com', role='teacher')
        get_data_func = lambda q, params: [(1,None)]
        result = courses_service.update_course_active_status(1, course_status=td.fake_course_status(), current_user=mock_user, get_data_func=get_data_func)
        expected = 'Not teacher'
        self.assertEqual(expected, result)

    def test_updateCourseActiveStatus_returnsTrue_when_StatusChangedSuccessfully(self):
        mock_user = td.fake_user(username='teacher@gmail.com', role='teacher')
        get_data_func = lambda q, params: [(1,1)]
        update_data_func = lambda q, params: 1
        result = courses_service.update_course_active_status(1, course_status=td.fake_course_status(), current_user=mock_user, get_data_func=get_data_func, update_data_func=update_data_func)
        self.assertTrue(result)

    def test_updateCourseActiveStatus_returnsFalse_when_StatusChangedSuccessfully(self):
        mock_user = td.fake_user(username='teacher@gmail.com', role='teacher')
        get_data_func = lambda q, params: [(1,1)]
        update_data_func = lambda q, params: 0
        result = courses_service.update_course_active_status(1, course_status=td.fake_course_status(), current_user=mock_user, get_data_func=get_data_func, update_data_func=update_data_func)
        self.assertFalse(result)

    def test_getTeacherEmailByCourseId_returnsValidData_when_teacherExsits(self):
        get_data_func = lambda q, params: [('techer@gmail.com', 'FastAPI', 'Ivan Dimitrov', 'Hristo Hristov')]
        expected =('techer@gmail.com', 'FastAPI', 'Ivan Dimitrov', 'Hristo Hristov')
        result = courses_service.get_teacher_email_by_course_id(1,2, get_data_func=get_data_func)
        self.assertEqual(expected, result)

    def test_selectCoursesByTeacherOrStudentHandler_returnsCourseOfTeacherNotFound_when_noSuchCourse(self):
        get_data_func = lambda q, p: None
        expected = f"Teacher with id:1 does not have a course with student with id 1."

        result = courses_service.select_courses_by_teacher_or_student_handler(teacher_id=1,student_id=1,get_data_func=get_data_func)

        self.assertEqual(expected,result)

    def test_selectCoursesByTeacherOrStudentHandler_returnsTeacherHasNoCourses_when_noSuchCourse(self):
        get_data_func = lambda q, p: None
        expected = "Teacher with id:1 doest not have courses."

        result = courses_service.select_courses_by_teacher_or_student_handler(teacher_id = 1,get_data_func=get_data_func)

        self.assertEqual(expected,result)

    def test_selectCoursesByTeacherOrStudentHandler_returnsStudentHasNoCourses_when_noSuchCourse(self):
        get_data_func = lambda q, p: None
        expected = "Student with id:1 doest not have courses."

        result = courses_service.select_courses_by_teacher_or_student_handler(student_id= 1,get_data_func=get_data_func)

        self.assertEqual(expected,result)

    def test_selectCoursesByTeacherOrStudentHandler_returnsCourseTeacherResponse_when_validArgs(self):
        get_data_func = lambda q, p: [(1,'Title','Great description',1),]
        mock_course_teacher_response = td.fake_course_teacher_response()
        expected = [mock_course_teacher_response]

        with patch('api.services.courses_service.CourseTeacherResponseModel.from_query_result', return_value = mock_course_teacher_response) as mock_response:
            result = courses_service.select_courses_by_teacher_or_student_handler(student_id= 1,get_data_func=get_data_func)

        self.assertEqual(expected,result)
        mock_response.assert_called_once()

    def test_courseProgress_returnsCourseDoesNotExist_when_noCourse(self):
        get_data_func = lambda q, p: [('Course does not exist',)]
        expected = {'result':'Course does not exist'}
        mock_user = td.fake_user()
        
        result = courses_service.course_progress(1, mock_user,get_data_func=get_data_func)

        self.assertEqual(expected, result) 

    def test_courseProgress_returnsStudentNotSubscribed_when_studentNotSubscribed(self):
        get_data_func = lambda q, p: [('Student is not subscribed',)]
        expected = {'result':'Student is not subscribed'}
        mock_user = td.fake_user()
        
        result = courses_service.course_progress(1, mock_user,get_data_func=get_data_func)

        self.assertEqual(expected, result) 

    def test_courseProgress_returnsStudentNotSubscribed_when_studentNotSubscribed(self):
        get_data_func = lambda q, p: [('Course has no sections',)]
        expected = {'result':'Course has no sections'}
        mock_user = td.fake_user()
        
        result = courses_service.course_progress(1, mock_user,get_data_func=get_data_func)

        self.assertEqual(expected, result) 

    def test_courseProgress_returnsCourseProgress_when_validArgs(self):
        get_data_func = lambda q, p: [('Course progress is 20 %',)]
        expected = {'result':'Course progress is 20 %'}
        mock_user = td.fake_user()
        
        result = courses_service.course_progress(1, mock_user,get_data_func=get_data_func)

        self.assertEqual(expected, result) 