import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.routers import guests
from api.common.responses import NotFound


class Guests_Should(TestCase):

    def test_createUser_callsCreate_when_executedWithUser(self):
        mock_user = td.fake_user_create()

        with patch('api.services.users_service.create', return_value=True) as mock_create:
            result = guests.create_user(user=mock_user)

        mock_create.assert_called_once_with(mock_user)
        self.assertTrue(result)

    def test_login_validCredentials_when_validArgs(self):
        mock_user = td.fake_OAuth2()

        with patch('api.services.users_service.get_password', return_value=True), \
                patch('api.services.users_service.compare_passwords', return_value=True), \
                patch('api.services.users_service.create_access_token', return_value='mock_token') as mock_create_token:
            response = guests.login(user=mock_user)

        self.assertEqual(response, {'access_token': 'mock_token', 'token_type': 'bearer'})
        mock_create_token.assert_called_once_with(data={'email': mock_user.username})

    def test_login_returnsNotFOund_when_passwordNotFound(self):
        mock_user = td.fake_OAuth2()
        mock_response = td.fake_response(NotFound, 'Invalid credentials!', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.get_password', return_value=False) as mock_password:
            result = guests.login(user=mock_user)

        self.assertEqual(expected.status_code, result.status_code)
        self.assertIsInstance(result, NotFound)
        mock_password.assert_called_once_with(mock_user.username)

    def test_login_returnsNotFOund_when_passwordNotMatching(self):
        mock_user = td.fake_OAuth2()
        mock_response = td.fake_response(NotFound, 'Invalid credentials!', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.get_password', return_value=True) as mock_password,\
               patch('api.services.users_service.compare_passwords', return_value=False) as mock_comparison:
            result = guests.login(user=mock_user)

        self.assertEqual(expected.status_code, result.status_code)
        self.assertIsInstance(result, NotFound)
        mock_password.assert_called_once_with(mock_user.username)
        mock_comparison.assert_called_once()