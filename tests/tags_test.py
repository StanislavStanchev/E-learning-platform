import test_database as td
from unittest import TestCase
from unittest.mock import patch
from api.routers import tags
from api.common.responses import NotFound, Conflict


class Tags_Should(TestCase):

    def test_getTag_tagWithCoursesReturned_whenUserIsLoggedIn(self):
        mock_user = td.fake_user()
        mock_tag_with_courses = td.fake_tag()
        expected = mock_tag_with_courses

        with patch('api.services.users_service.get_current_user', return_value=mock_user), \
                patch('api.services.tags_service.get_by_id', return_value=mock_tag_with_courses) as mock_get_by_id:
                result = tags.get_tag(id=1, current_user=mock_user)

        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.area, result.area)
        mock_get_by_id.assert_called_once()

    def test_getTag_conflict_whenTagHasNoCoursesAssigned(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(Conflict, content='The tag exists, but no courses are assigned to it.', status_code=409)
        expected = mock_response

        with patch('api.services.users_service.get_current_user', return_value=mock_user), \
                patch('api.services.tags_service.get_by_id', return_value="not assigned") as mock_get_by_id:
            result = tags.get_tag(id=1, current_user=mock_user)

        self.assertEqual(expected.status_code, result.status_code)
        self.assertIsInstance(result, Conflict)
        mock_get_by_id.assert_called_once()

    def test_getTag_notFound_whenTagNotFound(self):
        mock_user = td.fake_user()
        mock_response = td.fake_response(NotFound, content='Tag with Id No.1 not found.', status_code=404)
        expected = mock_response

        with patch('api.services.users_service.get_current_user', return_value=mock_user), \
                patch('api.services.tags_service.get_by_id', return_value=False) as mock_get_by_id:
            result = tags.get_tag(id=1, current_user=mock_user)

        self.assertEqual(expected.status_code, result.status_code)
        self.assertIsInstance(result, NotFound)
        mock_get_by_id.assert_called_once()