import unittest
import test_database as td
from api.services import users_service
from unittest.mock import Mock, patch, call, MagicMock, ANY
from fastapi.responses import JSONResponse
from api.data.models import User, UserOut
from passlib.context import CryptContext
import mariadb
from jose import jwt, ExpiredSignatureError, JWTError
from fastapi import HTTPException
from pydantic import ValidationError

class UsersService_Should(unittest.TestCase):

    @patch('api.services.users_service._get_connection')
    @patch('api.common.mailjet.verify_email')
    @patch.object(CryptContext, 'hash', return_value='hashedpassword')
    def test_create_CreatesUser_when_RoleIsStudent(self, mock_hash, mock_verify_email, mock_get_connection):
        mock_user_create = td.fake_user_create(role='student')
        mock_conn= mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1

        result = users_service.create(mock_user_create)
        message = f"You have successfully created a user with username: {mock_user_create.username}."
        expected = message
        self.assertEqual(result, expected)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()
        mock_verify_email.assert_not_called()
        mock_hash.assert_called_once_with(mock_user_create.password)

    @patch('api.services.users_service._get_connection')
    @patch('api.common.mailjet.verify_email')
    @patch.object(CryptContext, 'hash', return_value='hashedpassword')
    def test_create_CreatesUser_when_RoleIsTeacher(self, mock_hash, mock_verify_email, mock_get_connection):
        mock_user_create = td.fake_user_create()
        mock_conn= mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.lastrowid = 1
        mock_cursor.execute.return_value = 1
        result = users_service.create(mock_user_create)
        actual_message = result.body.decode().strip('"').replace('\\n', '\n')
        expected_message ="""We have sent you an email to confirm your account.
                Please check your inbox and follow the instructions in the email to activate your account."""
        self.assertEqual(actual_message, expected_message)
        mock_get_connection.assert_called_once()
        mock_conn.cursor.assert_called_once()
        mock_verify_email.assert_called_once_with(mock_user_create.username, mock_user_create.first_name + " " + mock_user_create.last_name)
        mock_hash.assert_called_once_with(mock_user_create.password)

    @patch('api.services.users_service._get_connection')
    @patch('api.common.mailjet.verify_email')
    @patch.object(CryptContext, 'hash', return_value='hashedpassword')
    def test_create_ReturnsError_when_EmailAlreadyRegistered(self, mock_hash, mock_verify_email, mock_get_connection):
        mock_user_create = td.fake_user_create()
        mock_conn= mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.execute.side_effect = mariadb.IntegrityError("Duplicate entry 'test@test.com' for key 'email_UNIQUE'")
        
        result = users_service.create(mock_user_create)
        
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.body.decode().strip('"'), "This email is already registered.")
        mock_hash.assert_called_once_with(mock_user_create.password)
        mock_verify_email.assert_not_called()

    @patch('api.services.users_service._get_connection')
    @patch('api.common.mailjet.verify_email')
    @patch.object(CryptContext, 'hash', return_value='hashedpassword')
    def test_create_ReturnsError_when_PhoneNumberAlreadyRegistered(self, mock_hash, mock_verify_email, mock_get_connection):
        mock_user_create = td.fake_user_create()
        mock_conn= mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.execute.side_effect = mariadb.IntegrityError("Duplicate entry '123456789' for key 'phone_number_UNIQUE'")
        
        result = users_service.create(mock_user_create)
        
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.body.decode().strip('"'), "This phone number is already registered.")
        mock_hash.assert_called_once_with(mock_user_create.password)
        mock_verify_email.assert_not_called()

    @patch('api.services.users_service._get_connection')
    @patch('api.common.mailjet.verify_email')
    @patch.object(CryptContext, 'hash', return_value='hashedpassword')
    def test_create_ReturnsError_when_LinkedInAccountAlreadyRegistered(self, mock_hash, mock_verify_email, mock_get_connection):
        mock_user_create = td.fake_user_create()
        mock_conn= mock_get_connection.return_value.__enter__.return_value
        mock_cursor = mock_conn.cursor.return_value
        mock_cursor.execute.side_effect = mariadb.IntegrityError("Duplicate entry 'linkedin_account' for key 'linkedin_account_UNIQUE'")
        
        result = users_service.create(mock_user_create)
        
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.body.decode().strip('"'), "This linkedin_account is already registered.")
        mock_hash.assert_called_once_with(mock_user_create.password)
        mock_verify_email.assert_not_called()
    
    def test_get_password_returnsPassword_whenExists(self):
        mock_user_create = td.fake_user_create()
        expected_password = mock_user_create.password
        get_data_func = lambda q,params: [(expected_password,)]

        actual_password = users_service.get_password(mock_user_create.username, get_data_func)

        self.assertEqual(expected_password,actual_password)
    
    def test_get_password_returnsPassword_whenExists(self):
        mock_user_create = td.fake_user_create(password=False)
        expected_password = mock_user_create.password
        get_data_func = lambda q,params: [(expected_password,)]

        actual_password = users_service.get_password(mock_user_create.username, get_data_func)

        self.assertEqual(expected_password,actual_password)

    @patch.object(CryptContext, 'verify', return_value=True)
    def test_compare_passwords_ReturnsTrue_when_passwordsAreEqual(self, mock_verify):
        mock_user_create = td.fake_user_create()
        password = mock_user_create.password
        real_password = mock_user_create.password

        result = users_service.compare_passwords(password,real_password)
        
        self.assertEqual(True, result)
        mock_verify.assert_called_once()
    
    @patch.object(CryptContext, 'verify', return_value=False)
    def test_compare_passwords_ReturnsTrue_when_passwordsAreEqual(self, mock_verify):
        mock_user_create = td.fake_user_create()
        password = mock_user_create.password
        real_password = 'aaaa'

        result = users_service.compare_passwords(password,real_password)
        
        self.assertEqual(False, result)
        mock_verify.assert_called_once()

    @patch.object(jwt, 'encode', return_value='mock_token')
    @patch('api.services.users_service.settings_token')
    def test_create_access_token_ReturnsExpectedToken(self, mock_settings, mock_encode):
        mock_settings.access_token_expire_minites = 30
        mock_settings.secret_key = 'test_secret'
        mock_settings.algorithm = 'HS256'
        
        mock_user_create = td.fake_user_create()
        mock_data = {'user_email': mock_user_create.username}
        expected_token = 'mock_token'

        result = users_service.create_access_token(mock_data)

        self.assertEqual(result, expected_token)
        mock_encode.assert_called_once_with({'user_email': mock_user_create.username, 'exp': ANY}, mock_settings.secret_key, mock_settings.algorithm)

    def test_get_by_email_returnsNone_whenUserDoesNotExist(self):
        mock_user_create = td.fake_user_create()
        get_data_func = lambda q,params: []

        user = users_service.get_by_email(mock_user_create.username, get_data_func)

        self.assertIsNone(user)

    def test_get_by_email_returnsError_whenUserIsNotVerified(self):
        mock_user_create = td.fake_user_create()
        get_data_func = lambda q,params: [(1, mock_user_create.username, mock_user_create.role, 0, 0)]

        with self.assertRaises(HTTPException) as context:
            users_service.get_by_email(mock_user_create.username, get_data_func)

        self.assertEqual(context.exception.detail, "User is not verified")

    def test_get_by_email_returnsError_whenUserIsNotVerified(self):
        mock_user_create = td.fake_user_create()
        get_data_func = lambda q,params: [(1, mock_user_create.username, mock_user_create.role, 1, 0)]

        with self.assertRaises(HTTPException) as context:
            users_service.get_by_email(mock_user_create.username, get_data_func)
            
        self.assertEqual(context.exception.detail, "User is deactivated")

    def test_get_by_email_returnsUser_whenUserIsVerifiedAndHasAccess(self):
        mock_user = td.fake_user()
        get_data_func = lambda q,params: [(mock_user.id, mock_user.username, 0, 1, 1)]
        user = users_service.get_by_email(mock_user.username, get_data_func)
            
        self.assertEqual(mock_user.id, user.id)
        self.assertEqual(mock_user.username, user.username)
        self.assertEqual(mock_user.role, user.role)
    
    def test_returns_correct_http_exception(self):
        headers = {"WWW-Authenticate": "Bearer"}
        status_code = 401
        detail = "Could not validate credentials"
        
        exception = users_service.credentials_exception(headers, status_code, detail)
        
        assert isinstance(exception, HTTPException)
        assert exception.status_code == status_code
        assert exception.detail == detail
        assert exception.headers == headers
    
    @patch.object(jwt, 'decode')
    def test_valid_token_returns_token_data(self, mock_decode):
        mock_decode.return_value = {"email": "test@example.com"}
        token = jwt.encode({"email": "test@example.com"}, users_service.settings_token.secret_key, algorithm=users_service.settings_token.algorithm)

        token_data = users_service.verify_access_token(token)

        assert token_data.email == "test@example.com"
        mock_decode.assert_called_once_with(token, users_service.settings_token.secret_key, users_service.settings_token.algorithm)

    @patch.object(jwt, 'decode', return_value={})
    def test_no_email_raises_exception(self, mock_decode):
        self.assertRaises(HTTPException, users_service.verify_access_token, 'invalid_token')
        exception_context = None
        try:
            users_service.verify_access_token('invalid_token')
        except HTTPException as e:
            exception_context = e
        self.assertEqual(exception_context.status_code, 401)

    @patch.object(jwt, 'decode', side_effect=ExpiredSignatureError())
    def test_expired_token_raises_exception(self, mock_decode):
        self.assertRaises(HTTPException, users_service.verify_access_token, 'expired_token')
        exception_context = None
        try:
            users_service.verify_access_token('expired_token')
        except HTTPException as e:
            exception_context = e
        self.assertEqual(exception_context.status_code, 403)

   

    @patch.object(jwt, 'decode', side_effect=JWTError())
    def test_invalid_token_raises_exception_jwtError(self, mock_decode):
        
        self.assertRaises(HTTPException, users_service.verify_access_token, 'invalid_token')
        exception_context = None
        try:
            users_service.verify_access_token('invalid_token')
        except HTTPException as e:
            exception_context = e
        self.assertEqual(exception_context.status_code, 401)
    
    @patch.object(jwt, 'decode', side_effect=ValidationError(errors=[], model="MockModel"))
    def test_invalid_token_raises_exception_ValidationError(self, mock_decode):
        
        self.assertRaises(HTTPException, users_service.verify_access_token, 'invalid_token')
        exception_context = None
        try:
            users_service.verify_access_token('invalid_token')
        except HTTPException as e:
            exception_context = e
        self.assertEqual(exception_context.status_code, 401)

