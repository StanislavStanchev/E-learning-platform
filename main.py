from fastapi import FastAPI
from fastapi_pagination import add_pagination
from api.api_router import api_router
from front_end.front_router import front_router


app = FastAPI()

app.include_router(api_router)

app.include_router(front_router)

add_pagination(app)
