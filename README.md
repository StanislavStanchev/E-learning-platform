![Alt text](images/header.PNG)
***
**Poodle is a Python-based E-learning platform, which provides RESTful API that incorporates functionalities for students and teachers in an online learning environment.**
***
![release](https://img.shields.io/badge/release-v1.0-blue) ![released_at](https://img.shields.io/badge/released%20at-15%2F06%2F2023-lightgrey)
![pythvers](https://img.shields.io/badge/python-v3.11-blue) ![endpoints](https://img.shields.io/badge/API%20endpoints-35-orange) 
![tests](https://img.shields.io/badge/total%20tests-216-blue) ![passing](https://img.shields.io/badge/tests%20passing-100%20%25-brightgreen) 
***
##### Developers: [Ivan Dimitrov](https://gitlab.com/idi-bg), [Petyo Denev](https://gitlab.com/denev_petyo), [Stanislav Stanchev](https://gitlab.com/StanislavStanchev)
***
## Table of contents

- [Table of contents](#table-of-contents)
- [Summary ](#summary-)
- [Tech stack ](#tech-stack-)
- [API demo](#api-demo)
- [Functionality ](#functionality-)
  - [Public Part](#public-part)
  - [Users](#users)
  - [Course Management](#course-management)
  - [Course Sections](#course-sections)
  - [Student Functionality](#student-functionality)
  - [Teacher Functionality](#teacher-functionality)
  - [Admin Functionality](#admin-functionality)
  - [Security / Authentication](#security--authentication)
    - [1. Security](#1-security)
    - [2. Authentication](#2-authentication)
- [Endpoints Summary ](#endpoints-summary-)
  - [Guests](#guests)
  - [Courses](#courses)
  - [Sections](#sections)
  - [Tags](#tags)
  - [Users](#users-1)
  - [Students](#students)
  - [Admins](#admins)
- [Unit tests ](#unit-tests-)
- [Database ](#database-)
- [Setup and Run project Instructions ](#setup-and-run-project-instructions-)
  - [1. Setting-up virtual environment (optional)](#1-setting-up-virtual-environment-optional)
    - [1.1. Installing virtual environment](#11-installing-virtual-environment)
    - [1.2. Choosing the venv interpreter](#12-choosing-the-venv-interpreter)
    - [1.3. Activate the venv](#13-activate-the-venv)
  - [2. Install project dependencies](#2-install-project-dependencies)
  - [3. Database server](#3-database-server)
    - [3.1. Offline testing mode](#31-offline-testing-mode)
    - [3.2. Connecting to your own DB server](#32-connecting-to-your-own-db-server)
  - [4. Run the web server](#4-run-the-web-server)
  - [5. FastAPI automatic documentation (Swagger)](#5-fastapi-automatic-documentation-swagger)
  - [6. Shutting down the web server](#6-shutting-down-the-web-server)
- [Postman collection](#postman-collection)
- [Acknowledgments](#acknowledgments)


## Summary <a id="summary"></a>

The [Poodle API](http://13.51.163.104/docs) revolves around the concepts of Users (students and teachers), Courses (public and premium), and Course Sections. Students can search for courses, enroll in them, and access course content based on their subscription status and course type (public or premium). Teachers have the ability to create and update courses, manage course sections, and publish content. 

Administrators have additional privileges, including authorizing teacher registrations, managing student access, and controlling course visibility. The project aims to create a user-friendly and secure platform for online learning, empowering students and teachers in their educational journey.

The API is complemented with a [basic web client](http://13.51.163.104/poodle/), build with Jinja2, Bootstrap and HTML.

## Tech stack <a id="teck stack"></a>

![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue)![FastAPI](https://img.shields.io/badge/FastAPI-009688.svg?style=for-the-badge&logo=FastAPI&logoColor=white)![JWT](https://img.shields.io/badge/JSON%20Web%20Tokens-000000.svg?style=for-the-badge&logo=JSON-Web-Tokens&logoColor=white)![AWS_RDS](https://img.shields.io/badge/Amazon%20RDS-527FFF.svg?style=for-the-badge&logo=Amazon-RDS&logoColor=white)![MariaDB](https://img.shields.io/badge/MariaDB-003545.svg?style=for-the-badge&logo=MariaDB&logoColor=white)![sqlite](https://img.shields.io/badge/SQLite-07405E?style=for-the-badge&logo=sqlite&logoColor=white)![Jinja2](https://img.shields.io/badge/Jinja-B41717.svg?style=for-the-badge&logo=Jinja&logoColor=white)![HTML](https://img.shields.io/badge/HTML5-E34F26.svg?style=for-the-badge&logo=HTML5&logoColor=white)![Bootstrap](https://img.shields.io/badge/Bootstrap-7952B3.svg?style=for-the-badge&logo=Bootstrap&logoColor=white)![Postman](https://img.shields.io/badge/Postman-FF6C37.svg?style=for-the-badge&logo=Postman&logoColor=white)![Git](https://img.shields.io/badge/Git-F05032.svg?style=for-the-badge&logo=Git&logoColor=white)

***

## API demo

The API is deployed on an AWS EC2 instance running Ubuntu server. It can be tested using the automatic FastAPI (Swagger) documentation: http://13.51.163.104/docs . 

The basic web client could be accessed through: http://13.51.163.104/poodle/ .

## Functionality <a id="functionality"></a>

### Public Part
- The public part of the platform is accessible by anonymous users without authentication.
- Guest users can view the titles, descriptions, and tags of available public courses.
- Guest users can search for courses by tag and/or rating.
- Guest users can register and create an account.

### Users
- Anonymous users can create accounts with their email, password and first and last name.
- Teachers have additional information such as a phone number and linked-in account.
- User roles include regular users, teachers, and administrators.

### Course Management
- Courses have a title (unique), description, objectives, owner (teacher), and relevant expertise tags.
- Courses can be either public or premium.
- Courses can have an optional home page picture.
- Courses can be rated by students, and the overall rating is calculated based on the provided scores.

### Course Sections
- Each course consists of multiple sections.
- Sections have a title, content, description (optional), and an optional link to external resources.
- Sections within a course can be sorted by name.

### Student Functionality
- Students can view and edit their account information.
- Students can track their progress in courses based on visited sections.
- Students can view the courses they are enrolled in (both public and premium).
- Students can search for existing public and premium courses by name and tag.
- Students can unsubscribe from premium courses.
- Students can rate a course if they are enrolled in it.
- Students can subscribe to a maximum of 5 premium courses at a time and an unlimited number of public courses.

### Teacher Functionality
- Teachers can view and edit their account information.
- Teachers can create courses and manage their own courses.
- Teachers can receive email notifications for enrollment requests in their courses.
- Teachers can approve enrollment requests from students.
- Teachers can deactivate (hide) their courses when no students are subscribed.
- Teachers can run reports on past and current students who have subscribed to their courses.

### Admin Functionality
- Administrators can approve teacher registrations.
- Administrators can view a list of all public and premium courses, along with the number of students and ratings.
- Administrators can deactivate/reactivate students.
- Administrators can delete or hide courses, with notifications sent to enrolled students.
- Administrators can remove students from courses.
- Administrators can search for courses filtered by teacher and/or student.
- Administrators can trace back ratings for courses to identify students who scored the course.

### Security / Authentication

#### 1. Security

During user registration, the app does not store plain text passwords in the database. The passwords are hashed for security purposes before being stored.

#### 2. Authentication

User authentication is implemented using OAuth 2, an industry-standard protocol for authorization, ensuring secure information exchange. This maintains the requirement of a stateless REST client-server communication, where the token is stored by the web client.

Upon each login, a JSON Web Token (JWT) is generated. For each subsequent user request, the token should be included in the request header. Our API then verifies the token's validity using a security algorithm and secret key. If the token is valid, the API grants access to the requested resource.

The following picture summarizes the authentication process:

![Alt text](images/oauth2.png)


## Endpoints Summary <a id="endpoints"></a>

Anonymous users are able to access the endpoints listed in Guests table.

### Guests

|` Method `|` Endpoint                                                                                           `|` About                      `|
|--------|---------------------------------------------------|------------------------------|
|`POST`|`/api/guests/signup`| Students and Teachers can create a Poodle profile.            |
|`POST`|`/api/guests/login` | Login for registered users.                                   |
|`GET`|`/api/guests/courses`| List title and description of available Public (only) Courses.|
|`GET`|`/api/guests/tags`   | Returns Tags of all Public (only) Courses.                    |


The rest of the endpoints (listed below) require user authentication.

### Courses

|` Method `|` Endpoint                                                                                         `|` About             `|
|--------|---------------------------------------------------|------------------------------------------------------------------------|
|`GET`   |`/api/courses`                                    | Users can view a list of all active Courses.                            |
|`GET`   | `/api/courses/{id}`                              | Users can access an active Course along with its sections depending on its role and subscription status.  |
|`POST`  |`/api/courses`| Teachers can create new courses. Course title should be unique.|
|`PUT`   |`/api/courses/{id}/sections` | Allows Teachers to create and assign Section to their own courses with one action. |
|`PUT`   |`/api/courses/{id}/tags` | Allows Teachers to create and assign tag to specific course with one action. |
|`PUT`   |`/api/courses/{id}/home-pages` | Teachers can set and update a Course Home Page. |
|`PUT`   |`/api/courses/{id}/status`                       |  Teachers can change the status (deactivate or activate) their own courses only when there are no students subscribed. A submitted but not approved request for subscription is not considered as student subscription.|
|`PATCH` |`/api/courses/{id}/sections` | Teachers are able to update section information. |
|`PATCH` |`/api/courses/{id}` | Teachers can update the main course information. |
|`DELETE`|`/api/courses/{course_id}/sections/{section_id}` |  allows Teachers to remove specific section from their course.  |
|`DELETE`|`/api/courses/{course_id}/tags/{tag_id}` | Allows Teachers to remove specific section from their own course. |


### Sections
|` Method `|` Endpoint                                                                                          `|` About             `|
|--------|---------------------------------------------------|-------------------------|
|`GET`|`/api/sections` | Returns a list of all sections (if admin/teacher), else: only to public or premium active courses where user has subscription. |
|`GET`|`/api/sections/{id}` | Returns a Section to which user has access. Can be accessed by all users (students/teachers/admins). |

### Tags
|` Method `|` Endpoint                                                                                          `|` About             `|
|--------|---------------------------------------------------|-------------------------|
|`GET` | `/api/tags`                  | Returns Tags of all courses (including Premium) |
|`GET` | `/api/tags/{id}`             | Returns a Tag along with the related courses. |

### Users
|` Method `|` Endpoint                                                                                          `|` About             `|
|--------|---------------------------------------------------|-------------------------|
|`GET` | `/api/users`                 | Users can view its account information.|
|`PUT` | `/api/users`                 | Update User Info |
|`PATCH` | `/api/users/password`      | User can update its passowrd. |

### Students 
|` Method `|` Endpoint                                                                                          `|` About             `|
|--------|---------------------------------------------------|-------------------------|
|`PUT` | `/api/courses/{id}` | Students can subscribe to public courses or sent subscription requests for premium courses. Constraint for premium courses: 5 at a time). |
|`DELETE` | `/api/courses/{id}` | Students are able to unsubscribe from premium courses. |
|`PUT` | `/api/courses/{id}/rates` |Students are able to rate a specific Course within the range from 1 (Min) to 10 (Max). |
|`GET` | `/api/courses/{id}/progress` | Students can track their progress in a specific course. |
|`DELETE` | `/api/students` | Admins could remove students from courses. |
|`PUT` | `/api/students/subscriptions` | Teachers / Admins can approve course subscription requests submitted by students. |
|`GET` | `/api/students` | Each Teacher can access a report with the subscribed students in their courses.  |

### Admins 
|` Method `|` Endpoint                                                                                          `|` Description             `|
|--------|---------------------------------------------------|-------------------------|
|`GET` | `/api/admins/reports` | Returns a list with all public and premium courses, the number of students in them and their rating. |
|`GET` | `/api/courses/students/report` | Admins can search for spefic Course, filtered by Teacher and / or Student. |
|`GET` | `/api/courses/{id}/rates` | Admins can view report on Students that rated specific Course. |
|`PUT` | `/api/admins/courses/{id}` | Admins can deactivate/activate courses. If the course has been deactivated, the enrolled students will receive an email notifications about the change. |
|`PUT` | `/api/admins/users/{id}` | Admin can approve or revoke User access. |

## Unit tests <a id="unit tests"></a>

We have diligently implemented over **210** comprehensive unit tests for both routers and services within the project. These tests   verify the functionality, behavior, and integrity of the implemented code, ensuring robustness and reliability throughout the application. By extensively testing the routers and services, we aim to deliver a highly stable and dependable system.

## Database <a id="database"></a>

The MariaDB table schema is structured as follows:
<details><summary>Click to expand</summary>
![Alt text](images/diagram.PNG)
</details>

## Setup and Run project Instructions <a id="api setup"></a>

In case the API could not be reached throught the AWS web server: [http://13.51.163.104/docs](#http://13.51.163.104/docs), you could still test the API by setting it up on your local machine.

### 1. Setting-up virtual environment (optional)

We recommend setting up a virtual environment (venv) before installing the project dependencies. This allows you to keep the dependencies isolated from your machine's global environment. To create a virtual environment, you can use tools like Python's venv module. Once the virtual environment is activated, you can install the project dependencies without affecting your system's libraries. This ensures a clean and controlled environment for running the API.

#### 1.1. Installing virtual environment

So the terminal command for creating new venv with name 'venv' is as follows:

```
py -3 -m venv venv
```

#### 1.2. Choosing the venv interpreter

Further, we should specify the venv interpreter to be used within the environment. This should be the python interpretator located in the venv folder (relative path in the project workspace: `.\venv\Scripts\python.exe`). 

More details could be found here:

- [VS Code venv instructions](https://code.visualstudio.com/docs/python/environments)

- [PyCharm venv instructions](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html#env-requirements)

#### 1.3. Activate the venv

The next step is to activate the venv that we just created to ensure that the dependencies are installed within the isolated environment. To activate the venv, use the appropriate command based on your operating system:

For Windows: 
```
venv\Scripts\activate
```

For macOS/Linux:
```
source venv/bin/activate
```

Once the venv is activated, you can proceed with installing the project dependencies using the command provided in the installation instructions.

### 2. Install project dependencies

You will need to install all dependencies that are necessary to run the app on your local machine. 
The requirements.txt file contain all the dependencies required.

To install the virtualenv packages on the venv please run:
```
pip install -r requirements.txt
```

### 3. Database server

The database server is hosted by AWS (used service: RDS) and it's constatly running.
Thus, no actions needed on your side to run the server. 

#### 3.1. Offline testing mode

If you have no Internet connection or the public (AWS) IP address is not accessible for some reason, you could still use and test the API on your local machine.

This could be done by pulling from the repository and using the branch called `"offline_api_branch"`. Based on the implemented logic, once the uvicorn server (described below) is started, the app will automatically create and load database information, thanks to the build in SQLite database engine. *In this case, the point 3.2. below could be skipped.*

#### 3.2. Connecting to your own DB server

The API can be configured using environment variables or .env file. The following variables need to be set:
Database Variables:

- DATABASE_NAME - the DB name
- DATABASE_HOSTNAME - the host name of the DB
- DATABASE_PORT - the DB port
- DATABASE_USERNAME - the DB username
- DATABASE_PASSWORD - the password to access the DB
- SECRET_KEY - the secret key for applying the hashing algorithm
- ALGORITHM - the algorithm for encrypting the JWT
- ACCESS_TOKEN_EXPIRE_MINITES - minutes for expiration of the JWT
- API_KEY - the API key for using the Mailjet platform for sending email to student / teacher users
- API_SECRET_KEY - the API key for using the Mailjet platform for sending email to student / teacher users

### 4. Run the web server

In order to run the uvicorn web server, execute the following command on the terminal:

```
python -m uvicorn main:app --reload
```

Once the command is executed, the uvicorn server will start, and you should see output indicating the server's status and any logged information or errors. The API will then be accessible at the default host and port (e.g., http://127.0.0.1:8000).

Remember to keep the venv activated while the server is running to ensure that it uses the correct dependencies and environment settings.

### 5. FastAPI automatic documentation (Swagger)

FastAPI provides automatic documentation generation, making it easier to present and communicate the API functionality. 
The documentation could be accessed while the web server is running to the default URL: http://127.0.0.1:8000/docs . 

### 6. Shutting down the web server

When funished using the app, web server could be shutted down by pressing `CTRL+C` in the terminal.

## Postman collection

A pre-populated Postman collection is also uploaded in order to make the API testing easier.

Before start using the Postman collection, a simple environment variable setup is needed, depending on whether the AWS Public IP address [http://13.51.163.104/docs](#http://13.51.163.104/docs) could be reacher or not. 

Thus, set an "IP" environment variable in the following way and choose the public or the local IP (*only one*) depending on your setup case:

![Alt text](images/env_variable.PNG)

## Acknowledgments

We would like to thank [Alexander Nedelev](https://gitlab.com/anedelev) for the support during the project! 
